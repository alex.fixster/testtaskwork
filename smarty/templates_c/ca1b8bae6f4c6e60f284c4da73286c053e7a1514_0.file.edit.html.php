<?php
/* Smarty version 3.1.39, created on 2022-01-13 11:25:28
  from '/Users/alex/www/coffee-store/templates/admin/income/edit.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61dff008b630e2_60620062',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ca1b8bae6f4c6e60f284c4da73286c053e7a1514' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/income/edit.html',
      1 => 1642023406,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61dff008b630e2_60620062 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript" src="/library/chosen/1.1.0/chosen.jquery.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" src="/library/datepicker/js/moment.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/library/datepicker/js/daterangepicker.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/library/datepicker/js/initialization.js" charset="UTF-8"><?php echo '</script'; ?>
>
<link href="/library/datepicker/css/daterangepicker.css" type="text/css" rel="stylesheet">

<?php echo '<script'; ?>
 type="text/javascript" language="javascript" src="/admin/js/income.js"><?php echo '</script'; ?>
>


<div class="page-header">
	<h1>
		<?php if ($_smarty_tpl->tpl_vars['incomeInfo']->value['id']) {?>Редагування<?php } else { ?>Добавлення<?php }?> приходу продуктів
        <span style="float:right;"><a href="/admin/income/add/" class="btn btn-success btn-sm no-border"><span class="fa fa-gift"></span>&nbsp;&nbsp;&nbsp;Добавити прихід продуктів</a></span>
    </h1>
</div>

<div style="height:10px;"></div>

<form class="form-horizontal" role="form" action="/admin/income/update/" method="post" enctype="multipart/form-data">
<input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['incomeInfo']->value['id'];?>
">

<div class="row">
<div class="col-xs-6">

	<div class="form-group">
		<label for="inputDate" class="col-xs-3 normal">Дата</label>
		<div class="col-xs-4">
        	<div class="input-group">
                <input name="cdate" id="inputDate" class="form-control" type="text" placeholder="Вкажіть дату" value="<?php echo $_smarty_tpl->tpl_vars['incomeInfo']->value['cdate'];?>
">
                <span class="input-group-addon" style="cursor:pointer;"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
		</div>
	</div>
    
	<div class="form-group">
		<label for="inputBrand" class="col-xs-3 normal">Бренд</label>
		<div class="col-xs-4">
			<select id="inputBrand" name="brand_id" class="form-control chosen-select" onChange="changeBrand()">
            <option value="">-- Виберіть бренд --</option>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['brandList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>           
            <option value="<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['incomeInfo']->value['brand_id'] == $_smarty_tpl->tpl_vars['local']->value['id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
</option>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </select>
		</div>
	</div>
    
	<div class="form-group">
		<label for="inputProduct" class="col-xs-3 normal">Продукт</label>
		<div class="col-xs-6">
			<select id="inputProduct" name="product_id" class="form-control chosen-select" onChange="changeProduct()">
            <option value="">-- Виберіть продукт --</option>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['productList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>           
            <option value="<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['incomeInfo']->value['product_id'] == $_smarty_tpl->tpl_vars['local']->value['id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['local']->value['art_no'];?>
 : <?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
, <?php echo $_smarty_tpl->tpl_vars['local']->value['weight'];?>
</option>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </select>
            
		</div>
	</div>

	 <div class="form-group">
		<label for="inputPrice" class="col-xs-3 normal">Країна поставки</label>
		<div class="col-xs-2">
			<label>
				<input name="income_country" type="radio" class="ace" value="UA" <?php if ($_smarty_tpl->tpl_vars['incomeInfo']->value['income_country'] == 'UA') {?>checked<?php }?>>
				<span class="lbl"> Україна</span>
			</label>

			<label>
				<input name="income_country" type="radio" class="ace" value="PL" <?php if ($_smarty_tpl->tpl_vars['incomeInfo']->value['income_country'] == 'PL') {?>checked<?php }?>>
				<span class="lbl"> Польща</span>
			</label>
		</div>
	</div>
    
    <div class="form-group">
		<label for="inputQty" class="col-xs-3 normal">Кількість</label>
		<div class="col-xs-6">
			<input type="text" id="inputQty" name="qty" class="form-control" placeholder="К-сть" value="<?php echo $_smarty_tpl->tpl_vars['incomeInfo']->value['qty'];?>
" style="width: 100px; float:left; margin-right: 10px;">
			<input type="text" id="inputIncomePrice" name="incomePrice" class="form-control" placeholder="Ціна" value="<?php echo $_smarty_tpl->tpl_vars['incomeInfo']->value['incomePrice'];?>
" style="width: 100px; float:left; margin-right: 10px;">
			<input type="text" id="inputIncomeDelivery" name="incomeDelivery" class="form-control" placeholder="Доставка" value="<?php echo $_smarty_tpl->tpl_vars['incomeInfo']->value['incomeDelivery'];?>
" style="width: 100px; float:left; margin-right: 10px;">
			<div style="padding-top: 4px;"><a href="javascript: reCalcPrice();"><i class="fa fa-calculator blue" style="font-size: 25px; font-weight:bold;"></i></a></div>
		</div>
	</div>
    
    <div class="form-group">
		<label for="inputPrice" class="col-xs-3 normal">Ціна</label>
		<div class="col-xs-2">
			<input type="text" id="inputPrice" name="price" class="form-control" placeholder="Введіть ціну" value="<?php echo $_smarty_tpl->tpl_vars['incomeInfo']->value['price'];?>
">
		</div>
	</div>
	
	<div class="form-group">
		<label for="inputDeliveryPrice" class="col-xs-3 normal">Доставка</label>
		<div class="col-xs-2">
			<input type="text" id="inputDeliveryPrice" name="delivery_price" class="form-control" placeholder="Введіть вартість доставки" value="<?php echo $_smarty_tpl->tpl_vars['incomeInfo']->value['delivery_price'];?>
">
		</div>
	</div>
	
	<div class="form-group">
		<label for="inputNote" class="col-xs-3 normal">Нотатки</label>
		<div class="col-xs-6">
			<textarea id="inputNote" name="note" class="form-control" rows="6" placeholder="Введіть нотатки"><?php echo $_smarty_tpl->tpl_vars['incomeInfo']->value['note'];?>
</textarea>
		</div>
	</div>

</div>

<div class="col-xs-6" id="divProductData">
	
</div>


<div class="col-xs-12">
	
	<div class="form-group">
		<div class="col-sm-6 text-center">
			<button type="submit" class="btn btn-warning no-border" style="width:120px;">Зберегти</button>
		</div>
	</div>
	

</div>

</div>
</form>


<?php echo '<script'; ?>
 type="text/javascript" language="javascript">
daterangepicker_initialize({
	'inputDate' : 'single'
});

$(".chosen-select").chosen({
	width:"100%",
	no_results_text: 'Нічого не знайдено'
}); 
$('.chosen-select').data('chosen').allow_single_deselect = true;
$("#inputFilterBrand_chosen input").css("width", "100%");
$("#inputFilterProduct_chosen input").css("width", "100%");
changeProduct();
<?php echo '</script'; ?>
>

<?php }
}
