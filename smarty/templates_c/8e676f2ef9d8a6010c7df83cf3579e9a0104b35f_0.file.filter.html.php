<?php
/* Smarty version 3.1.39, created on 2022-01-13 10:53:37
  from '/Users/alex/www/coffee-store/templates/admin/income/filter.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61dfe89152dce4_29552851',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8e676f2ef9d8a6010c7df83cf3579e9a0104b35f' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/income/filter.html',
      1 => 1641899089,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61dfe89152dce4_29552851 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript" src="/library/chosen/1.1.0/chosen.jquery.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" src="/library/typeahead/0.10.5/typeahead.bundle.js"><?php echo '</script'; ?>
>
<link href="/library/typeahead/0.10.5/typeahead.css" type="text/css" rel="stylesheet">

<?php echo '<script'; ?>
 type="text/javascript" src="/library/datepicker/js/moment.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/library/datepicker/js/daterangepicker.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/library/datepicker/js/initialization.js" charset="UTF-8"><?php echo '</script'; ?>
>
<link href="/library/datepicker/css/daterangepicker.css" type="text/css" rel="stylesheet">

<form class="form-horizontal" role="form" action="/admin/income/save_filter/" method="post">
	<div class="row">
    	<div class="col-xs-3">
			
			<div class="form-group">
				<label for="inputBrand" class="col-xs-12">Бренди</label>
				<div class="col-xs-12">
					<select class="form-control chosen-select" multiple name="brand_id[]" id="inputFilterBrand" data-placeholder="Введіть бренд">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['brandList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['searchInfo']->value['brand_id'][$_smarty_tpl->tpl_vars['local']->value['id']]) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
</option>
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </select>
				</div>
			</div>
            
      </div>
        
      <div class="col-xs-3">
			
			<div class="form-group">
				<label for="inputCategory" class="col-xs-12">Категорія</label>
				<div class="col-xs-12">
					<select class="form-control chosen-select" multiple name="category_id[]" id="inputFilterCategory" data-placeholder="Введіть категорію">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categoryList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
					<optgroup label="<?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
">
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['local']->value['items'], 'local_i', false, 'key_i');
$_smarty_tpl->tpl_vars['local_i']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key_i']->value => $_smarty_tpl->tpl_vars['local_i']->value) {
$_smarty_tpl->tpl_vars['local_i']->do_else = false;
?>
                    	<option value="<?php echo $_smarty_tpl->tpl_vars['local_i']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['searchInfo']->value['category_id'][$_smarty_tpl->tpl_vars['local_i']->value['id']]) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['local_i']->value['header'];?>
</option>
						<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					</optgroup>
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </select>
				</div>
			</div>
            
      </div>
   

		<div class="col-xs-3">
			
			<div class="form-group">
				<label for="inputHeader" class="col-xs-12">Назва</label>
				<div class="col-xs-12">
					<input type="text" class="form-control" name="header" id="inputFilterHeader" value="<?php echo $_smarty_tpl->tpl_vars['searchInfo']->value['header'];?>
" placeholder="Введіть назву товару">
				</div>
			</div>
            
      </div>
      <div class="col-xs-3">
            
            <div class="form-group">
				<label for="inputDatePeriod" class="col-xs-12">Період</label>
				<div class="col-xs-12">
                	<div class="input-group">
						<input name="period" id="inputDatePeriod" class="form-control" type="text" placeholder="Вкажіть період" value="<?php echo $_smarty_tpl->tpl_vars['searchInfo']->value['period'];?>
">
						 <span class="input-group-addon" style="cursor:pointer;"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>
			</div>
            
         </div>
         
		
		<div class="col-xs-12 text-center">
			<button type="submit" name="search" value="Search" class="btn btn-warning btn-sm no-border btn-mobile" style="width:100px;">
                Пошук
            </button> 
            <button type="button" name="clear" value="Clear" class="btn btn-default btn-sm no-border btn-mobile" onclick="clearFilter()" style="width:100px;">
                Очистити
            </button>
		</div>
	</div>
</form>

<hr>

<?php echo '<script'; ?>
 type="text/javascript" language="javascript">
initTypeahead();

daterangepicker_initialize({
	'inputDatePeriod' : 'double'
});
<?php echo '</script'; ?>
><?php }
}
