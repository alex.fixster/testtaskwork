<?php
/* Smarty version 3.1.39, created on 2022-01-13 20:50:07
  from '/Users/alex/www/coffee-store/templates/home/main.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e0745f5dc568_65689550',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '12485c733fa1c63836f1470c9d71eae825007b73' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/home/main.html',
      1 => 1633717597,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61e0745f5dc568_65689550 (Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="home-product-block">
	<div class="block short illy-grani">
		<h1><?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['TOP_PRODUCT_BLOCK_GRANI_HEADER'];?>
</h1>
		<p>
		<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['TOP_PRODUCT_BLOCK_GRANI_CONTENT'];?>

		</p>
		<div class="btn"><a href="/coffee/illy/in-grani/" title="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['TOP_PRODUCT_BLOCK_GRANI_HEADER'];?>
"><img src="/images/home-block/btn-red-<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
.png" width="194" height="40" alt="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['TOP_PRODUCT_BLOCK_GRANI_HEADER'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['TOP_PRODUCT_BLOCK_GRANI_HEADER'];?>
"></a></div>
	</div>
	<div class="block short illy-monoarabica">
		<h1><?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['TOP_PRODUCT_BLOCK_MONO_HEADER'];?>
</h1>
		<p>
		<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['TOP_PRODUCT_BLOCK_MONO_CONTENT'];?>

		</p>
		<div class="btn"><a href="/coffee/illy/monoarabica/" title="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['TOP_PRODUCT_BLOCK_MONO_HEADER'];?>
"><img src="/images/home-block/btn-green-<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
.png" width="194" height="40" alt="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['TOP_PRODUCT_BLOCK_MONO_HEADER'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['TOP_PRODUCT_BLOCK_MONO_HEADER'];?>
"></a></div>
	</div>
	<div class="block long illy-iperespresso">
		<h1><?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['TOP_PRODUCT_BLOCK_IPER_HEADER'];?>
</h1>
		<p>
		<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['TOP_PRODUCT_BLOCK_IPER_CONTENT'];?>

		</p>
		<div class="btn"><a href="/coffee/illy/iperespresso/" title="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['TOP_PRODUCT_BLOCK_IPER_HEADER'];?>
"><img src="/images/home-block/btn-black-<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
.png" width="194" height="40" alt="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['TOP_PRODUCT_BLOCK_IPER_HEADER'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['TOP_PRODUCT_BLOCK_IPER_HEADER'];?>
"></a></div>
	</div>
</div>

<div class="line"></div>


<div class="home-block-text">
	<div class="block short">
		<div class="text-left">
			<h1><?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['BLOCK_TEXT_1_HEADER'];?>
</h1>
			<p>
			<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['BLOCK_TEXT_1_TEXT'];?>

			</p>
			<div class="btn"><a href="/blog/populyarni_vidi_kavi/" title="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['BLOCK_TEXT_1_HEADER'];?>
"><?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['BLOCK_TEXT_LINK'];?>
</a></div>
		</div>
		<div class="image-right"><img src="/images/home-text/block-1-img.jpg" width="555" height="260" alt="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['BLOCK_TEXT_1_HEADER'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['BLOCK_TEXT_1_HEADER'];?>
"></div>
	</div>
	
	<div class="block short">
		<div class="text-right">
			<h1 style="padding-top:40px;"><?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['BLOCK_TEXT_2_HEADER'];?>
</h1>
			<p>
			<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['BLOCK_TEXT_2_TEXT'];?>

			</p>
			<div class="btn"><a href="/blog/osobennosti_kofe_iz_raznyh_stran_mira/" title="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['BLOCK_TEXT_2_HEADER'];?>
"><?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['BLOCK_TEXT_LINK'];?>
</a></div>
		</div>
		<div class="image-left"><img src="/images/home-text/block-2-img.jpg" width="555" height="260" alt="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['BLOCK_TEXT_2_HEADER'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['BLOCK_TEXT_2_HEADER'];?>
"></div>
	</div>
	
	<div class="block short">
		<div class="text-left">
			<h1 style="padding-top:30px;"><?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['BLOCK_TEXT_3_HEADER'];?>
</h1>
			<p>
			<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['BLOCK_TEXT_3_TEXT'];?>

			</p>
			<div class="btn"><a href="/blog/tehnologia_vyrobnyctva_kavy/" title="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['BLOCK_TEXT_3_HEADER'];?>
"><?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['BLOCK_TEXT_LINK'];?>
</a></div>
		</div>
		<div class="image-right"><img src="/images/home-text/block-3-img.jpg" width="555" height="260" alt="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['BLOCK_TEXT_3_HEADER'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['HOME']['BLOCK_TEXT_3_HEADER'];?>
"></div>
	</div>
</div>

<div class="clear"></div>
<div style="height:40px;"></div>

<?php }
}
