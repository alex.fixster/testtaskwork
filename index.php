<?php

include "inc/config.inc.php";


$rqData = clearQuotes($_REQUEST);
	
$urlValues = explodeUrl($rqData['url']);

$errorMessageArr = $_SESSION['messages'];
unset_messages();

// dump($errorMessageArr);

// $smarty->assign("host", ($_SERVER['HTTP_HOST'] == "localhost") ? $_SERVER['HTTP_HOST']."/coffee-stor" : $_SERVER['HTTP_HOST']);

if ($urlValues[0] and file_exists(BASE_PATH."/modules/".$urlValues[0].".php")) include BASE_PATH."/modules/".$urlValues[0].".php";	
else include BASE_PATH."/modules/home.php";

$smarty->assign("errorMessageArr", $errorMessageArr);

unset($_SESSION['form_data']);

$smarty->assign("include_tpl", $include_tpl);

$smarty->registerFilter("output", "rewrite_url");

$smarty->display("index.html");

$db->close();

?>