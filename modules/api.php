<?php


switch ($urlValues[1]) {
	
	default:

		header("location: ".BASE_URL."/");
		exit;

	break;
	
	case "product":

		switch ($urlValues[2]) {

			default:
			case "list":

				$resultArray = $api->getAllProduct($rqData['category_id'], $rqData['subcategory']);

			break;

			case "info":

				$resultArray = $api->getProductInfo($rqData['id']);

			break;

			case "count":

				$resultArray = $api->getProductCount($rqData['category_id']);

			break;

		}

	break;

	case "category":

		switch ($urlValues[2]) {

			default:
			case "list":

				$resultArray = $api->getAllCategory($rqData['id'], $rqData['product']);

			break;

			case "info":

				$resultArray = $api->getCategoryOne($rqData['id'], $rqData['product'], $rqData['subcategory']);

			break;

			case "breadcrumb":

				$resultArray = $api->getCategoryBreadcrumb($rqData['id']);

			break;

		}

	break;
	
}



header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json");

// код ответа - 200 OK
http_response_code(200);

// вывод в формате json
echo json_encode($resultArray);

exit;

?>