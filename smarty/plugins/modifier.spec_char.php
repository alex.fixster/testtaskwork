<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty spec_char modifier plugin
 *
 * Type:     modifier<br>
 * Name:     spec_char<br>
 * Purpose:  Replace special chars
 * @link http://smarty.php.net/manual/en/language.modifier.strip.tags.php
 *          spec_char (Smarty online manual)
 * @param string
 * @param boolean
 * @return string
 */
function smarty_modifier_spec_char($string, $slash = false)
{
	
	$charArr['&'] = "&amp;";
	$charArr['<'] = "&lt;";
	$charArr['>'] = "&gt;";
	if ($slash) {
		$charArr['"'] = '\"';
		$charArr["'"] = "\'";
	}
	else {
		$charArr['"'] = "&quot;";
		$charArr["'"] = "&apos;";
	}
	$charArr["«"] = "&laquo;";
	$charArr["»"] = "&raquo;";

	
	
	foreach ($charArr as $key => $val) {
		
		$string = str_replace($key, $val, $string);
		
	}
	
	return $string;
	
}

/* vim: set expandtab: */

?>
