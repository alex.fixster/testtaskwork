<?php
/* Smarty version 3.1.39, created on 2022-01-13 20:50:07
  from '/Users/alex/www/coffee-store/templates/home/slider.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e0745f5ce116_58044212',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5c67df1898e031c937f125e11a10fd97f21be832' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/home/slider.html',
      1 => 1543345649,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61e0745f5ce116_58044212 (Smarty_Internal_Template $_smarty_tpl) {
?><link rel="stylesheet" type="text/css" href="/css/home.css" media="all"/>
<link rel="stylesheet" type="text/css" href="/css/home-tablet.css" media="all"/>
<link rel="stylesheet" type="text/css" href="/css/home-mobile.css" media="all"/>
<link rel="stylesheet" type="text/css" href="/css/home-mobile-small.css" media="all"/>
<?php echo '<script'; ?>
 src="/library/slick/jquery.bxslider.min.js"><?php echo '</script'; ?>
>

<ul class="slider">
	<li class="slide-1" data-slide-index="1">
		<div class="slide-text-block">
			<h1><?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['SLIDE_1_HEADER'];?>
</h1>
			
			<div class="slide-text"><?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['SLIDE_1_CONTENT'];?>
</div>
			
			<a href="/coffee/illy/" title="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['GO_TO_TEXT'];?>
">
			<div class="slide-link"><?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['GO_TO_TEXT'];?>
</div>
			</a>
		</div>
		<div class="slide-pic-block"><img src="/images/slider/illy-coffee-slide-pic.jpg" width="484" height="335" title="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['SLIDE_1_HEADER'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['SLIDE_1_HEADER'];?>
"></div>
	</li>
	
	<li class="slide-2" data-slide-index="2">
		<div class="slide-text-block">
			<h1><?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['SLIDE_2_HEADER'];?>
</h1>
			
			<div class="slide-text"><?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['SLIDE_2_CONTENT'];?>
</div>
			
			<a href="/tea/dammann/" title="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['GO_TO_TEXT'];?>
">
			<div class="slide-link"><?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['GO_TO_TEXT'];?>
</div>
			</a>
		</div>
		<div class="slide-pic-block"><img src="/images/slider/bg-dammann-tea.jpg" width="583" height="375" title="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['SLIDE_2_HEADER'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['SLIDE_2_HEADER'];?>
"></div>
	</li>
	
	<li class="slide-3" data-slide-index="3">
		<div class="slide-text-block">
			<h1><?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['SLIDE_3_HEADER'];?>
</h1>
			
			<div class="slide-text"><?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['SLIDE_3_CONTENT'];?>
</div>
			
			<a href="/accessories/illy/" title="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['GO_TO_TEXT'];?>
">
			<div class="slide-link"><?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['GO_TO_TEXT'];?>
</div>
			</a>
		</div>
		<div class="slide-pic-block"><img src="/images/slider/bg-illy-accessory.jpg" width="425" height="347" title="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['SLIDE_3_HEADER'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['SLIDE_3_HEADER'];?>
"></div>
	</li>
	
	
	<li class="slide-4" data-slide-index="4">
		<div class="slide-text-block">
			<h1><?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['SLIDE_4_HEADER'];?>
</h1>
			
			<div class="slide-text"><?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['SLIDE_4_CONTENT'];?>
</div>
			
			<a href="/accessories/dammann/" title="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['GO_TO_TEXT'];?>
">
			<div class="slide-link"><?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['GO_TO_TEXT'];?>
</div>
			</a>
		</div>
		<div class="slide-pic-block"><img src="/images/slider/bg-eva-solo.jpg" width="500" height="331" title="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['SLIDE_4_HEADER'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['homeStatText']->value['SLIDER']['SLIDE_4_HEADER'];?>
"></div>
	</li>
	
</ul>

<?php echo '<script'; ?>
 type="text/javascript" language="javascript">

var slider = $('.slider').bxSlider({
	controls:false,
	auto:true,
	speed:2000,
	pause:6000
});


<?php echo '</script'; ?>
><?php }
}
