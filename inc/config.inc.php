<?


require_once(realpath(dirname(__FILE__))."/settings.inc.php");

if ($_SERVER["HTTP_HOST"]) session_start();



define("CATEGORY_PER_PAGE", 5);
define("PRODUCT_PER_PAGE", 5);



set_include_path(BASE_PATH.'/inc/class/library');
include_once (BASE_PATH."/inc/functions.php");

# Zend library
include_once (BASE_PATH."/inc/class/library/Zend/Loader.php");
include_once (BASE_PATH."/inc/class/library/Zend/Db.php");
# END Zend Library


# include class

$handleInc = opendir(BASE_PATH."/inc/class/");

while (false !== ($file = readdir($handleInc))) {
	
	if (preg_match("/\.php/", $file)) {

		include_once (BASE_PATH."/inc/class/".$file);

	}
		
}


function autoload($class_name) {

	global $startData;

	$class_nameArr = explode("_", $class_name);

	$ignoreClass = array("Zend", "Smarty");

	
	if (!in_array($class_nameArr[0], $ignoreClass)) {
		
		$class_name = implode("_", $class_nameArr);

		$prPath = BASE_PATH."/inc/class/".$class_name.".class.php";

		
	    include_once $prPath;

    }
}

	

# end include class


$zend_db = Zend_Db::factory('Pdo_Mysql', array('host' => $dbhost, 'username' => $dbuser, 'password' => $dbpassword, 'dbname' => $dbname, 'charset'  => 'utf8'));
$db = new Zend_Db_Adapter_db();


# class
$category = new category();
$message = new message();
$product = new product();
$api = new api();
# end class



# include Smarty
include (BASE_PATH.'/smarty/Smarty.class.php');

$smarty = new Smarty;
$smarty->template_dir = BASE_PATH.'/templates/';
$smarty->compile_dir  = BASE_PATH.'/smarty/templates_c/';
$smarty->config_dir   = BASE_PATH.'/smarty/configs/';
$smarty->cache_dir	  = BASE_PATH.'/smarty/cache/';

spl_autoload_register('autoload');

require (BASE_PATH.'/smarty/smarty.functions.php');

$smarty->registerPlugin("function", "categoryTree", "smarty_function_category_tree");
$smarty->registerPlugin("function", "productCategoryTree", "smarty_function_product_category_tree");
# end include Smarty

