<?





function dump($var) {
	
	global $dumpCounterRND;
	echo '<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">';
	echo "<hr><div style='background-color:white; padding:10px;'><pre>".(int)$dumpCounterRND++.". ";

	if (is_array($var)) {
		print_r($var);
	} else {
		var_dump($var);
	}
	
	echo "</pre></div><hr>";
	
}



function clearQuotes($data) {
	
	if (is_array($data)) {
		
		foreach ($data as $id => $value) if (!is_array($value)) $data[$id] = stripslashes($value);
			
	}
	
	else $data = stripslashes($data);
	
	return $data;
}






function explodeUrl($url) {

	if (substr($url, 0, 1) == "/") $url = substr($url, 1);
	
	if (substr($url, 0, 6) == "admin/") $url = substr($url, 6);

	if (substr($url, 0, 3) == "uk/" or substr($url, 0, 3) == "ru/") $url = substr($url, 3);

	
	
	$returnVal = explode('/', str_replace('.html', '', $url));
	
	if ($profile and $returnVal[0] != "js" and $returnVal[0] != "story") {
		
		$tempArr = $returnVal;
		
		unset($returnVal);
		
		$returnVal[0] = "profile";
		
		$returnVal[1] = $profile;
		
		if (is_array($tempArr)) {
			
			foreach ($tempArr as $key => $val) $returnVal[] = $val;
			
		}
		
	}
	
	end($returnVal);


	if (!$returnVal[key($returnVal)]) unset($returnVal[key($returnVal)]);
	
	if (count($returnVal) == 1 and $returnVal[0] == "") return false;
	
	else return $returnVal;

}



function rewrite_url($tpl) {
	
	$Domain ="";

	$requestUrl = str_replace(BASE_URL, "", $_SERVER['REQUEST_URI']);

	// dump($requestUrl);
	// dump($_SERVER['REQUEST_URI']);exit;

	if (strpos(BASE_URL, "admin")) $admin_path = "/".ADMIN_PATH;
	if (stripos($requestUrl, "/ru/") === 0) $lang_path = "/ru";
	// if (stripos($requestUrl, "/ru/") === 0) $lang_path = "/ru";

	// dump($lang_path);
	// dump(BASE_URL);exit;
	
	$tpl = preg_replace("/(<link[[:space:]a-zA-Z0-9_=\.\/\'\"]{1,})href=(\"|')([a-zA-Z0-9\-_\.\/]{0,}\/)([a-zA-Z0-9\-_\.]{1,}\.css)(\"|')([[:space:]a-zA-Z0-9_=\.\/\'\"]{0,}>)/i", "\\1href=\\2"."/------\\3\\4\\5\\6", $tpl);
	
	$tpl = preg_replace("/href=(\"|')([a-zA-Z0-9\-_\.\/]{1,})(jpg|jpeg|gif|png|swf)(\"|')/i", "href=\\1--------\\2\\3\\4", $tpl);
	$tpl = preg_replace("/href=(\"|')([a-zA-Z0-9\=\&\?\-_\.\/]{1,})(\"|'|\?|\&|\#)/i", "href=\\1".BASE_URL.$lang_path."\\2\\3", $tpl);
	$tpl = preg_replace("/href=(\"|')([a-zA-Z0-9\-_\.\/]{0,})(--------)([a-zA-Z0-9\-_\.\/]{0,})(\"|'|\?|\&)/i", "href=\\1".BASE_URL.STATIC_SERVER.$admin_path."\\4\\5", $tpl);
	
	// $tpl = preg_replace("/href=(\"|')([a-zA-Z0-9\-_\.\/]{1,})(jpg|jpeg|gif|png|swf)(\"|')/i", "href=\\1--------\\2\\3\\4", $tpl);
	// $tpl = preg_replace("/href=(\"|')([a-zA-Z0-9\=\&\?\-_\.\/\#]{1,})(\"|'|\?|\&|\#)/i", "href=\\1".BASE_URL."\\2\\3", $tpl);
	// $tpl = preg_replace("/href=(\"|')([a-zA-Z0-9\-_\.\/]{0,})(--------)([a-zA-Z0-9\-_\.\/]{0,})(\"|'|\?|\&)/i", "href=\\1".BASE_URL.STATIC_SERVER."/p.".CURRENT_PROJECT.$admin_path."\\4\\5", $tpl);

	$tpl = preg_replace("/(<link[[:space:]a-zA-Z0-9_=\.\/\'\"]{1,}href=)(\"|')([a-zA-Z0-9\-_\.\/]{0,})(\/------)([a-zA-Z0-9\-_\.\/]{1,})\/([a-zA-Z0-9\-_\.]{1,}\.css)(\"|')([[:space:]a-zA-Z0-9_=\.\/\'\"]{0,}>)/i", "\\1\\2".BASE_URL.STATIC_SERVER.$admin_path."\\5/\\6\\7\\8", $tpl);
	$tpl = preg_replace("/(<link[[:space:]a-zA-Z0-9_=\.\/\'\"]{1,}href=)(\"|')([a-zA-Z0-9\-_\.\/]{0,})(\/library)([a-zA-Z0-9\-_\.\/]{1,})\/([a-zA-Z0-9\-_\.]{1,}\.css)(\"|')([[:space:]a-zA-Z0-9_=\.\/\'\"]{0,}>)/i", "\\1\\2".BASE_URL.STATIC_SERVER."\\4\\5/\\6\\7\\8", $tpl);
	
	//$tpl = preg_replace("/href=(\"|')([a-zA-Z0-9_\.\/]{1,})(#####)([a-zA-Z0-9\-_\.\/]{1,})(\"|')/", "href=\\1\\4\\5", $tpl);
	

	
	//$tpl = preg_replace("/src=(\"|')(\/[a-zA-Z0-9\-_\.\/]{1,}\.(jpg|js|jpeg|gif|png|html|htm))(\"|')/i", "src=\\1".BASE_URL.STATIC_SERVER."/p.".CURRENT_PROJECT.$admin_path."\\2\\4", $tpl);
	
	$tpl = preg_replace("/src=(\"|')(\/[a-zA-Z0-9\-_\.\/]{1,}\.(jpg|js|jpeg|gif|png|html|htm))(\?urn=[0-9]+)?(\"|')/i", "src=\\1".BASE_URL.STATIC_SERVER.$admin_path."\\2\\4\\5", $tpl);
	
	$tpl = preg_replace("/src=(\"|')(\/[a-zA-Z0-9_\.\/]{1,}(\/library\/[a-zA-Z0-9\-_\.\/]{0,}\.(jpg|js|jpeg|gif|png|html|htm)))(\?urn=[0-9]+)?(\"|')/i", "src=\\1".BASE_URL.STATIC_SERVER."\\3\\5\\6", $tpl);

	$tpl = preg_replace("/data-zoom-image=(\"|')(\/[a-zA-Z0-9\-_\.\/]{1,}\.(jpg|jpeg|gif|png))(\?urn=[0-9]+)?(\"|')/i", "data-zoom-image=\\1".BASE_URL.STATIC_SERVER.$admin_path."\\2\\4\\5", $tpl);
	
	//$tpl = preg_replace("/src=(\"|')([a-zA-Z0-9_\.\/]{1,})(#####)([a-zA-Z0-9\-_\.\/]{1,})(\"|')/", "src=\\1\\4\\5", $tpl);
	
	
	$tpl = str_replace("ACTION=\"/", "ACTION=\"".BASE_URL.$lang_path."/", $tpl);
	
	$tpl = str_replace("action=\"/", "ACTION=\"".BASE_URL.$lang_path."/", $tpl);
	
	
	$tpl = str_replace("confirmdel('/", "confirmdel('".BASE_URL."/", $tpl);
	
	$tpl = str_replace("openWindow('/", "openWindow('".BASE_URL."/", $tpl);
	
	
	$tpl = preg_replace("/(background-image:[[:space:]]{0,}url\(['|\"]{0,1})/", "\\1".BASE_URL.STATIC_SERVER.$admin_path, $tpl);
	$tpl = preg_replace("/(background-image:[[:space:]]{0,}url\(['|\"]{0,1})([a-zA-Z0-9_\.\/]{1,})(#{5})([a-zA-Z0-9\-_\.\/]{1,})(\))(\"|'|;)/", "\\1\\4\\5\\6", $tpl);
	
	
	$tpl = preg_replace("/(src=|href=)(\"|')([a-zA-Z0-9_\.\/]{1,})(#{5})([a-zA-Z0-9\-_\.\/]{1,})(\"|'|\&|\?)/", "\\1\\2\\5\\6", $tpl);


	return $tpl;
	
}





function checkEmail($email) {

	$res = filter_var($email, FILTER_VALIDATE_EMAIL);

	return $res;
	
}



function _checkDate($date) {

	$dataInfo = explode("-", $date);

 	if (count($dataInfo) == 3 and checkdate($dataInfo[1], $dataInfo[2], $dataInfo[0])) return true;
 	else {

 		$dataInfo = explode("/", $date);

 		if (count($dataInfo) == 3 and checkdate($dataInfo[1], $dataInfo[0], $dataInfo[2])) return true;

 		else return false;

 	}
	
}



function _checkTime($time, $sec = false) {
	
	if ($sec) {

		if (preg_match("/^(0[0-9]|1[0-9]|2[0123])\:([0-5][0-9])\:([0-5][0-9])$/i",$time)) return true;
		else return false;

	}

	else {

		if (preg_match("/^(0[0-9]|1[0-9]|2[0123])\:([0-5][0-9])$/i",$time)) return true;
		else return false;

	}
 	
 	
	
}



function save_message($id) {
	
	global $message;
	
	$messageInfo = $message->getOne($id);

	// if ($replace_param) $messageInfo['content'] = str_replace(array_keys($replace_param), $replace_param, $messageInfo['content']);
	
	$_SESSION['messages'][] = $messageInfo['content'];
	
	return true;
	
}



function unset_messages() {
	
	unset($_SESSION['messages']);
	// unset($_SESSION['admin_messages']);
	
}




function generateNameImg ($image, $new_name = false) {
	
	if ($new_name) $str = $new_name;
	
	else {
	
		$str = getmicrotime();
	
		$str .= "_";
		
		for ($i = 0; $i < 4; $i++) $str .= rand(0, 9);
	
	}
	
	return $str . strrchr($image, '.');
	
}





function deleteUploadedPic($picArr, $image, $path = "") {
	
	foreach ($picArr as $key => $val) {
		
		@unlink($val[0].$path."/".$image);
		
	}
	
}





function getFileExtension($file) {

	$arr = explode(".", $file);

	if (count($arr) <= 1) return false;
	
	$extension = array_pop($arr);
	
	return $extension;
	
}






function downloadFile($path, $file, $file_name) {
	
	$path = $path."/".$file;

	header("Content-Type: application/octet-stream"); 
	header("Content-Length: ".filesize($path)); 
	header('Content-Disposition: attachment; filename="'.$file_name.'"'); 
	 
	header("Cache-Control: no-store, no-cache, must-revalidate");   // HTTP/1.1 
	header("Cache-Control: post-check=0, pre-check=0", false); 
	if (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on") 
	    header("Pragma: public"); 
	else 
	    header("Pragma: no-cache"); 
	header("Cache-Control: private"); 
	header("Expires: 0"); 
	
	ob_end_clean(); 
	readfile($path); 
	ob_end_flush(); 
	die;

}





function downloadString($str, $file_name) {

	//$path = $path."/".$file;

	header("Content-Type: application/octet-stream");
	header("Content-Length: ".strlen($str));
	header('Content-Disposition: attachment; filename="'.$file_name.'"');

	header("Cache-Control: no-store, no-cache, must-revalidate");   // HTTP/1.1
	header("Cache-Control: post-check=0, pre-check=0", false);
	if (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on")
		header("Pragma: public");
	else
		header("Pragma: no-cache");
	header("Cache-Control: private");
	header("Expires: 0");

	ob_end_clean();
	echo $str;
	ob_end_flush();
	die;

}




function isValidURL($url) {
	
	if (preg_match("/^(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/", $url)) return true;
	else return false;
	
}
	



function uploadFile($fileInfo, $path, $old_file = "", $new_name = "") {
	
	if ($fileInfo['name']) {
		
		if (!$new_name) $new_name = generateNameImg($fileInfo['name']);
		
		@copy($fileInfo['tmp_name'], $path."/".$new_name);
		
		if ($old_file) @unlink($path."/".$old_file);
		
		$resArr['original_name'] = $fileInfo['name'];
		$resArr['file'] = $new_name;
		
	}
	
	return $resArr;
	
}




function clearUrl($post) {

	unset($post['url']);
	unset($post['x']);
	unset($post['y']);
	// unset($post['nomir_sid']);
	unset($post['__utma']);
	unset($post['__utmz']);
	unset($post['__utmc']);
	unset($post['__utmb']);
	// unset($post['nomir_user_sid']);
	unset($post['PHPSESSID']);

	return $post;

}



function dateTransform($date, $with_time = true, $monthGroup = 1, $am_pm = false) {

	global $db, $common;

	$monthList = $common->getMonths($monthGroup);

	// dump($monthList);

	
	$dateTimeArr = explode(" ", $date);
	
	$dateArr = explode("-", $dateTimeArr[0]);

	$dateArr[2] = (!(int)substr($dateArr[2], 0, 1)) ? substr($dateArr[2], 1, 1) : $dateArr[2];
	
	$ret_str = $dateArr[2]." ".$monthList[$dateArr[1]]['month'].", ".$dateArr[0];
	
	if ($with_time and $dateTimeArr[1]) {
		
		$timeArr = explode(":", $dateTimeArr[1]);

		if ($am_pm) $ret_str .= " ".date("g:i a", strtotime($date));
		
		else $ret_str .= " ".$timeArr[0].":".$timeArr[1];
		
	}
	
	return $ret_str;
	
}




function _stopScript($urlValues) {

	$stop = false;

	if ($urlValues[0] == "favicon.ico") $stop = true;

	if ($stop) exit;

}



function transliterate($input){
	$gost = array(
		"Є"=>"YE","І"=>"I","Ѓ"=>"G","і"=>"i","№"=>"-","є"=>"ye","ѓ"=>"g",
		"А"=>"A","Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D",
		"Е"=>"E","Ё"=>"YO","Ж"=>"ZH",
		"З"=>"Z","И"=>"I","Й"=>"J","К"=>"K","Л"=>"L",
		"М"=>"M","Н"=>"N","О"=>"O","П"=>"P","Р"=>"R",
		"С"=>"S","Т"=>"T","У"=>"U","Ф"=>"F","Х"=>"X",
		"Ц"=>"C","Ч"=>"CH","Ш"=>"SH","Щ"=>"SHH","Ъ"=>"'",
		"Ы"=>"Y","Ь"=>"","Э"=>"E","Ю"=>"YU","Я"=>"YA",
		"а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d",
		"е"=>"e","ё"=>"yo","ж"=>"zh",
		"з"=>"z","и"=>"i","й"=>"j","к"=>"k","л"=>"l",
		"м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
		"с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"x",
		"ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"shh","ъ"=>"",
		"ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
		" "=>"_","—"=>"_",","=>"_","!"=>"_","@"=>"_",
		"#"=>"-","$"=>"","%"=>"","^"=>"","&"=>"","*"=>"",
		"("=>"",")"=>"","+"=>"","="=>"",";"=>"",":"=>"",
		"'"=>"",'"'=>"","~"=>"","`"=>"","?"=>"","/"=>"",
		"["=>"","]"=>"","{"=>"","}"=>"","|"=>""," " => "_"
	);

	return strtr($input, $gost);
}

?>