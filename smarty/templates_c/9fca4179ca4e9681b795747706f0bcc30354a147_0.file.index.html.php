<?php
/* Smarty version 3.1.39, created on 2022-01-13 10:53:28
  from '/Users/alex/www/coffee-store/templates/admin/index.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61dfe888b75255_27333268',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9fca4179ca4e9681b795747706f0bcc30354a147' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/index.html',
      1 => 1641303594,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:admin/common/message.html' => 1,
  ),
),false)) {
function content_61dfe888b75255_27333268 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="en">
<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>
			Coffee-Store Адмін
		</title>

		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		
		

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="/library/ace/1.3/css/bootstrap.min.css" />
		<!--<link rel="stylesheet" href="/library/ace/1.3/css/font-awesome.min.css" />-->
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">

		<link rel="stylesheet" href="/library/chosen/1.1.0/chosen.css" />
		<link rel="stylesheet" href="/library/ui/jquery-ui.custom.min.css" />
		<!-- page specific plugin styles -->

		<!-- text fonts -->
		<link rel="stylesheet" href="/library/ace/1.3/css/ace-fonts.css" />

		
		<!-- ace styles -->
		<link rel="stylesheet" href="/library/ace/1.3/css/ace.min.css" />

		
		<link rel="stylesheet" href="/library/ace/1.3/css/ace-skins.min.css" />
		<link rel="stylesheet" href="/library/ace/1.3/css/ace-rtl.min.css" />
		
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic,700italic&subset=latin,cyrillic' rel='stylesheet' type='text/css' />
		
		<link href="/admin/css/main.css" type="text/css" rel="stylesheet">
		<?php echo '<script'; ?>
 type="text/javascript" language="javascript"  src="/admin/js/main.js"><?php echo '</script'; ?>
>
		<!-- ace settings handler -->
		<?php echo '<script'; ?>
 src="/library/ace/1.3/js/ace-extra.min.js"><?php echo '</script'; ?>
>
		
		<?php echo '<script'; ?>
 src="/library/jquery/jquery-2.1.1.min.js"><?php echo '</script'; ?>
>
		
		
		
		<?php echo '<script'; ?>
 src="/library/ace/1.3/js/bootstrap.min.js"><?php echo '</script'; ?>
>
		
		<?php echo '<script'; ?>
 type="text/javascript" src="/library/bootstrap/3.1.1/js/modal.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 type="text/javascript" src="/library/bootstrap/3.1.1/js/tooltip.js"><?php echo '</script'; ?>
>
		
	
		
		<?php echo '<script'; ?>
 type="text/javascript" language="javascript">
		var browser_type = '<?php echo $_smarty_tpl->tpl_vars['browser_type']->value;?>
';
		var BASE_URL = '<?php echo (defined('BASE_URL') ? constant('BASE_URL') : null);?>
';
		var configDateFormat = 'YYYY-MM-DD'
		<?php echo '</script'; ?>
>

	</head>

	<body class="no-skin">
		
		<?php $_smarty_tpl->_subTemplateRender("file:admin/common/message.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
		
		<!-- #section:basics/navbar.layout -->
		<div id="navbar" class="navbar navbar-default">
			

			<div class="navbar-container" id="navbar-container">
				<!-- #section:basics/sidebar.mobile.toggle -->
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<!-- /section:basics/sidebar.mobile.toggle -->
				<div class="navbar-header pull-left">
					<!-- #section:basics/navbar.layout.brand -->
					<a href="/" class="navbar-brand">
						<small>
							<i class="fa fa-leaf"></i> Coffee-Store Адмін
						</small>
					</a>

					<!-- /section:basics/navbar.layout.brand -->

					<!-- #section:basics/navbar.toggle -->

					<!-- /section:basics/navbar.toggle -->
				</div>

				<!-- #section:basics/navbar.dropdown -->
				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						
						<!-- #notice -->
						<!--
						<li class="purple">
							<a class="dropdown-toggle" href="#" data-toggle="dropdown">
								<i class="ace-icon fa fa-bell <?php if ($_smarty_tpl->tpl_vars['mainNoticeList']->value['count']) {?>icon-animated-bell<?php }?>"></i><?php if ($_smarty_tpl->tpl_vars['mainNoticeList']->value['count']) {?> <span class="badge badge-important"><?php echo $_smarty_tpl->tpl_vars['mainNoticeList']->value['count'];?>
</span><?php }?>
							</a>
							<ul class="dropdown-menu-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="ace-icon fa fa-exclamation-triangle"></i> <?php echo (($tmp = @$_smarty_tpl->tpl_vars['mainNoticeList']->value['count'])===null||$tmp==='' ? '0' : $tmp);?>
 New Notification(s)<?php if ($_smarty_tpl->tpl_vars['mainNoticeList']->value['count'] > 1) {?>s<?php }?>
								</li>
								<?php if ($_smarty_tpl->tpl_vars['mainNoticeList']->value['count']) {?>
								<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['mainNoticeList']->value['list'], 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
								<li>
									<a href="/account/notice/#tr<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
">
									<i class="btn btn-xs no-hover btn-<?php echo $_smarty_tpl->tpl_vars['local']->value['fa_color'];?>
 fa fa-<?php echo $_smarty_tpl->tpl_vars['local']->value['fa_icon'];?>
 ace-icon" style="height:23px;"></i> <?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>

									</a>
								</li>
								<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
								<?php }?>
								
						
								<li class="dropdown-footer">
									<a href="/account/notice/">
									See all notifications <i class="ace-icon fa fa-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>
						-->
						<!-- end-- #notice -->
						
						<!-- #section:basics/navbar.user_menu -->
						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<i class="fa fa-user" style="font-size:26px; margin-top:8px; margin-right:5px;"></i>
								<span class="user-info" style="margin-top:8px; max-width:150px;">
									<?php echo $_SESSION['admin']['name'];?>

								</span>
								<i class="ace-icon fa fa-caret-down"></i>
							</a>
							
							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li><a href="/admin/signout/"><i class="ace-icon fa fa-power-off"></i> Вийти</a></li>
							</ul>

						<!-- /section:basics/navbar.user_menu -->
					</ul>
				</div>

				<!-- /section:basics/navbar.dropdown -->
			</div><!-- /.navbar-container -->
		</div>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			

			<!-- #section:basics/sidebar -->
			<div id="sidebar" class="sidebar responsive <?php if (($_smarty_tpl->tpl_vars['activeSubMenu']->value == 'del_notes' || $_smarty_tpl->tpl_vars['activeSubMenu']->value == 'reports_overview') && $_SESSION['login']['container'] == 'Y' && $_smarty_tpl->tpl_vars['os_type']->value != 'tab') {?>menu-min<?php }?>">
				
				<ul class="nav nav-list">
				
					<li class="highlight <?php if ($_smarty_tpl->tpl_vars['activeMenu']->value == 'dashboard') {?>active<?php }?>">
						<a href="/admin/"><i class="menu-icon fa fa-tachometer"></i><span class="menu-text"> Дашборд </span></a>
						<b class="arrow"></b>
					</li>
					
					<li class="highlight <?php if ($_smarty_tpl->tpl_vars['activeMenu']->value == 'config') {?>active<?php }?>">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-cogs"></i><span class="menu-text"> Конфігурація </span><b class="arrow fa fa-angle-down"></b>
						</a>
						
						<b class="arrow"></b>
					
						<ul class="submenu">
							<li <?php if ($_smarty_tpl->tpl_vars['activeSubMenu']->value == 'conf_delivery') {?>class="active"<?php }?>>
								<a href="/admin/config/delivery/"><i class="menu-icon fa fa-caret-right"></i>Доставка</a>
							</li>
                            <li <?php if ($_smarty_tpl->tpl_vars['activeSubMenu']->value == 'conf_discount') {?>class="active"<?php }?>>
								<a href="/admin/config/discount/"><i class="menu-icon fa fa-caret-right"></i>Знижки</a>
							</li>
							<li <?php if ($_smarty_tpl->tpl_vars['activeSubMenu']->value == 'packing_amount') {?>class="active"<?php }?>>
								<a href="/admin/config/packing_amount/"><i class="menu-icon fa fa-caret-right"></i>Вартість упакування</a>
							</li>
							<li <?php if ($_smarty_tpl->tpl_vars['activeSubMenu']->value == 'sms') {?>class="active"<?php }?>>
								<a href="/admin/config/sms/"><i class="menu-icon fa fa-caret-right"></i>SMS шаблони</a>
							</li>
                            <li <?php if ($_smarty_tpl->tpl_vars['activeSubMenu']->value == 'admin') {?>class="active"<?php }?>>
								<a href="/admin/config/admin/"><i class="menu-icon fa fa-caret-right"></i>Адмін</a>
							</li>
							<li <?php if ($_smarty_tpl->tpl_vars['activeSubMenu']->value == 'meta') {?>class="active"<?php }?>>
								<a href="/admin/meta/list/"><i class="menu-icon fa fa-caret-right"></i>Мета-теги</a>
							</li>
						</ul>
					</li>
					
					<li class="highlight <?php if ($_smarty_tpl->tpl_vars['activeMenu']->value == 'content') {?>active<?php }?>">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-align-left "></i><span class="menu-text"> Контенти </span><b class="arrow fa fa-angle-down"></b>
						</a>
						
						<b class="arrow"></b>
					
						<ul class="submenu">
							<li <?php if ($_smarty_tpl->tpl_vars['activeSubMenu']->value == 'text_list') {?>class="active"<?php }?>>
								<a href="/admin/content/text_list/"><i class="menu-icon fa fa-caret-right"></i>Тексти</a>
							</li>
							<li <?php if ($_smarty_tpl->tpl_vars['activeSubMenu']->value == 'home_list' || $_smarty_tpl->tpl_vars['activeSubMenu']->value == 'block_list' || $_smarty_tpl->tpl_vars['activeSubMenu']->value == 'slider_list') {?>class="active"<?php }?>>
								<a href="#" class="dropdown-toggle">Головна <i class="menu-icon fa fa-caret-right"></i><i class="menu-icon fa fa-caret-right"></i><b class="arrow fa fa-angle-down"></b></a>
								<ul class="submenu nav-show">
									<li <?php if ($_smarty_tpl->tpl_vars['activeSubMenu']->value == 'slider_list') {?>class="active"<?php }?>>
										<a href="/admin/content/slider_list/">
											<i class="menu-icon glyphicon glyphicon-picture blue"></i>
											Слайдер
										</a>

										<b class="arrow"></b>
									</li>

									<li <?php if ($_smarty_tpl->tpl_vars['activeSubMenu']->value == 'block_list') {?>class="active"<?php }?>>
										<a href="/admin/content/block_list/">
											<i class="menu-icon glyphicon glyphicon-th blue"></i>
											Блоки
										</a>

										<b class="arrow"></b>
									</li>

									<li <?php if ($_smarty_tpl->tpl_vars['activeSubMenu']->value == 'home_list') {?>class="active"<?php }?>>
										<a href="/admin/content/home_list/">
											<i class="menu-icon glyphicon glyphicon-align-left blue"></i>
											Контенти
										</a>

										<b class="arrow"></b>
									</li>
								</ul>

							</li>
						</ul>
					</li>
					
					<li class="highlight <?php if ($_smarty_tpl->tpl_vars['activeMenu']->value == 'news') {?>active<?php }?>">
						<a href="/admin/news/list/" ><i class="menu-icon fa fa-newspaper-o"></i><span class="menu-text"> Новини </span></a>
						<b class="arrow"></b>
					</li>
					
					<li class="highlight <?php if ($_smarty_tpl->tpl_vars['activeMenu']->value == 'action') {?>active<?php }?>">
						<a href="/admin/action/list/"><i class="menu-icon fa fa-gift"></i><span class="menu-text"> Акції </span></a>
						<b class="arrow"></b>
					</li>
					
					<li class="highlight <?php if ($_smarty_tpl->tpl_vars['activeMenu']->value == 'client') {?>active<?php }?>">
						<a href="/admin/client/list/"><i class="menu-icon fa fa-user"></i><span class="menu-text"> Клієнти </span></a>
						<b class="arrow"></b>
					</li>
					
					<li class="highlight <?php if ($_smarty_tpl->tpl_vars['activeMenu']->value == 'coupon') {?>active<?php }?>">
						<a href="/admin/coupon/list/"><i class="menu-icon fa fa-percent"></i><span class="menu-text"> Купони знижок </span></a>
						<b class="arrow"></b>
					</li>
					
					<!--
										-->
					
					<li class="highlight <?php if ($_smarty_tpl->tpl_vars['activeMenu']->value == 'essense') {?>active<?php }?>">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-database"></i><span class="menu-text"> Сутності </span><b class="arrow fa fa-angle-down"></b>
						</a>
						
						<b class="arrow"></b>
					
						<ul class="submenu">
							<li <?php if ($_smarty_tpl->tpl_vars['activeSubMenu']->value == 'box_list') {?>class="active"<?php }?>>
								<a href="/admin/essense/box_list/"><i class="menu-icon fa fa-caret-right"></i>Коробки</a>
							</li>
                            <li <?php if ($_smarty_tpl->tpl_vars['activeSubMenu']->value == 'sample_list') {?>class="active"<?php }?>>
								<a href="/admin/essense/sample_list/"><i class="menu-icon fa fa-caret-right"></i>Пробніки</a>
							</li>
						</ul>
					</li>
					
					<li class="highlight <?php if ($_smarty_tpl->tpl_vars['activeMenu']->value == 'products') {?>active<?php }?>">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-ticket"></i><span class="menu-text"> Продукти </span><b class="arrow fa fa-angle-down"></b>
						</a>
						
						<b class="arrow"></b>
					
						<ul class="submenu">
							<li <?php if ($_smarty_tpl->tpl_vars['activeSubMenu']->value == 'product') {?>class="active"<?php }?>>
								<a href="/admin/products/list/"><i class="menu-icon fa fa-caret-right"></i>Товари</a>
							</li>
							<li <?php if ($_smarty_tpl->tpl_vars['activeSubMenu']->value == 'income') {?>class="active"<?php }?>>
								<a href="/admin/income/list/"><i class="menu-icon fa fa-caret-right"></i>Прихід</a>
							</li>
						</ul>
					</li>
					
					<li class="highlight <?php if ($_smarty_tpl->tpl_vars['activeMenu']->value == 'order') {?>active<?php }?>">
						<a href="/admin/order/list/"><i class="menu-icon fa fa-shopping-cart"></i><span class="menu-text"> Замовлення </span></a>
						<b class="arrow"></b>
					</li>
					
				</ul><!-- /.nav-list -->

				<!-- #section:basics/sidebar.layout.minimize -->
				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>

				<!-- /section:basics/sidebar.layout.minimize -->
				
			</div>

			<!-- /section:basics/sidebar -->
			<div class="main-content">
				

				<!-- /section:basics/content.breadcrumbs -->
				<div class="page-content">
					

					<!-- /section:settings.box -->
					<div class="row">
						<div class="col-xs-12">
							<!-- PAGE CONTENT BEGINS -->
							<?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['include_tpl']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
							<!-- PAGE CONTENT ENDS -->
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.page-content -->
			</div><!-- /.main-content -->
			<div style="height:30px;"></div>
			

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		
		<?php echo '<script'; ?>
 src="/library/ui/jquery-ui.min.js"><?php echo '</script'; ?>
>
		
		
		<!-- ace scripts -->
		
		
		<?php echo '<script'; ?>
 src="/library/ace/1.3/js/ace.min.js"><?php echo '</script'; ?>
>
		
		


		<?php if ($_smarty_tpl->tpl_vars['adminMessageArr']->value) {?>
		<?php echo '<script'; ?>
 type="text/javascript" language="javascript">
		
		$('#modalClientMessage').modal({show:true});
		$('#modalClientMessage').on('hidden.bs.modal', function (e) {
			
			<?php echo $_smarty_tpl->tpl_vars['messageCloseFunct']->value;?>

			
		});
		
		
		<?php echo '</script'; ?>
>
		<?php }?>
		
		
	</body>
</html>
<?php }
}
