<?php
/* Smarty version 3.1.39, created on 2022-01-13 20:50:07
  from '/Users/alex/www/coffee-store/templates/index.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e0745f522cb9_68610674',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c9c29881d35c67609fd19ba106a9d7dcd812b824' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/index.html',
      1 => 1641303504,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:home/slider.html' => 1,
    'file:common/message.html' => 1,
  ),
),false)) {
function content_61e0745f522cb9_68610674 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/Users/alex/www/coffee-store/smarty/plugins/modifier.spec_char.php','function'=>'smarty_modifier_spec_char',),));
?>
<!DOCTYPE html>
<html>
<head>
	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><?php echo $_smarty_tpl->tpl_vars['metaData']->value['title'];?>
</title>
	<meta name="description" content="<?php echo smarty_modifier_spec_char($_smarty_tpl->tpl_vars['metaData']->value['description']);?>
"/>
	<meta name="keywords" content="<?php echo smarty_modifier_spec_char($_smarty_tpl->tpl_vars['metaData']->value['keywords']);?>
"/>
	<meta name="viewport" content="width=device-width,minimum-scale=0.6">
	
	<meta name="viewport" content="width=device-width,maximum-scale=1">
	<link rel="stylesheet" type="text/css" href="/css/main.css" media="all"/>
	<link rel="stylesheet" type="text/css" href="/css/main-tablet.css" media="all"/>
	<link rel="stylesheet" type="text/css" href="/css/main-mobile.css" media="all"/>
	<link rel="stylesheet" type="text/css" href="/css/main-mobile-small.css" media="all"/>
	
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;subset=cyrillic" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700&amp;subset=cyrillic-ext" rel="stylesheet"> 
	<?php echo '<script'; ?>
 src="http://code.jquery.com/jquery-1.12.0.min.js" type="text/javascript" language="javascript"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="/library/ui/jquery-ui.min.js" type="text/javascript" language="javascript"><?php echo '</script'; ?>
>
	
	<?php echo '<script'; ?>
 type="text/javascript" language="javascript">
	var BASE_URL = '<?php echo (defined('BASE_URL') ? constant('BASE_URL') : null);
echo $_smarty_tpl->tpl_vars['langURL']->value;?>
';
	var AJAX_URL = '<?php echo (defined('BASE_URL') ? constant('BASE_URL') : null);?>
';
	var STATIC_URL = '<?php echo (defined('BASE_URL') ? constant('BASE_URL') : null);
echo (defined('STATIC_SERVER') ? constant('STATIC_SERVER') : null);?>
';
	var LANG = '<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
';
	
	var statText = new Array();
	
	statText['GLOBAL'] = new Array();
	statText['GLOBAL']['POPUP_CLOSE_BTN_ALT'] = '<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['POPUP_CLOSE_BTN_ALT'];?>
';
	statText['GLOBAL']['PRODUCT_NAME_QTY_1'] = '<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['PRODUCT_NAME_QTY_1'];?>
';
	statText['GLOBAL']['PRODUCT_NAME_QTY_2'] = '<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['PRODUCT_NAME_QTY_2'];?>
';
	statText['GLOBAL']['PRODUCT_NAME_QTY_3'] = '<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['PRODUCT_NAME_QTY_3'];?>
';
	<?php echo '</script'; ?>
>
	
	<?php echo '<script'; ?>
 type="text/javascript" language="javascript" src="/js/main.js"><?php echo '</script'; ?>
>
</head>

<body>

<div class="main-section">


<div class="main-header">
	<div class="menu-icon-mobile"><a href="javascript: showMobileMenu()" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ICO_ALT'];?>
"><img src="/images/menu-icon-mobile.gif" width="31" height="25" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ICO_ALT'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ICO_ALT'];?>
"></a></div>
	
	
	
	<?php if ($_smarty_tpl->tpl_vars['homeSlider']->value) {?>
	<div class="logo-top"><a href="/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['LOGO_TITLE'];?>
"><img src="/images/logo-top.jpg" width="96" height="142" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['LOGO_TITLE'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['LOGO_TITLE'];?>
"></a></div>
	<div class="logo-bottom"><a href="/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['LOGO_TITLE'];?>
"><img src="/images/logo-bottom.png" width="98" height="31" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['LOGO_TITLE'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['LOGO_TITLE'];?>
"></a></div>
	<?php } else { ?>
	<div class="logo"><a href="/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['LOGO_TITLE'];?>
"><img src="/images/logo.jpg" width="94" height="173" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['LOGO_TITLE'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['LOGO_TITLE'];?>
"></a></div>
	<?php }?>
	<div class="logo-mobile-normal"><a href="/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['LOGO_TITLE'];?>
"><img src="/images/logo/logo-mobile-top-normal.jpg" width="66" height="100" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['LOGO_TITLE'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['LOGO_TITLE'];?>
"></a></div>
	<div class="logo-bottom-mobile-normal"><a href="/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['LOGO_TITLE'];?>
"><img src="/images/logo/logo-mobile-bottom-normal.png" width="66" height="21" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['LOGO_TITLE'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['LOGO_TITLE'];?>
"></a></div>


	<div class="menu-mobile">
		<div class="menu-mobile-header">
			<div><a href="javascript: hideMobileMenu()" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ICO_HIDE_ALT'];?>
"><img src="/images/menu-mobile/menu-icon.gif" width="31" height="25" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ICO_HIDE_ALT'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ICO_HIDE_ALT'];?>
"></a></div>
			<div class="search">
				<form method="post" action="/search-save/">
					<input type="text" name="q" placeholder="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['SEARCH_PLACEHOLDER'];?>
" value="<?php echo $_GET['q'];?>
"><input type="submit" value="">
				</form>
			</div>
			<div class="lang-mobile lang-mobile-RU"><a href="http://<?php echo $_smarty_tpl->tpl_vars['domen']->value;?>
/ru<?php echo $_smarty_tpl->tpl_vars['urlChangeLang']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['HEADER_MENU_LANG_RU_TITLE'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['HEADER_MENU_LANG_RU'];?>
</a></div>
			<div class="lang-mobile lang-mobile-UA"><a href="http://<?php echo $_smarty_tpl->tpl_vars['domen']->value;
echo $_smarty_tpl->tpl_vars['urlChangeLang']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['HEADER_MENU_LANG_UA_TITLE'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['HEADER_MENU_LANG_UA'];?>
</a></div>
		</div>
		<div class="menu-mobile-items">
		
			<ul>
				<li>
					<a href="/coffee/illy/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_ILLY'];?>
">
					<div><img src="/images/menu-mobile/menu-illy.gif" width="83" height="41" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_ILLY'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_ILLY'];?>
"></div>
					<div><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_ILLY'];?>
</div>
					</a>
				</li>
				<li>
					<a href="/tea/dammann/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_DAMMANN'];?>
">
					<div><img src="/images/menu-mobile/menu-dammann.gif" width="83" height="41" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_DAMMANN'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_DAMMANN'];?>
"></div>
					<div><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_DAMMANN'];?>
</div>
					</a>
				</li>
				<li>
					<a href="/accessories/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_ACCESSORIES'];?>
">
					<div><img src="/images/menu-mobile/menu-accessories.gif" width="83" height="41" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_ACCESSORIES'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_ACCESSORIES'];?>
"></div>
					<div><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_ACCESSORIES'];?>
</div>
					</a>
				</li>
				<li>
					<a href="/espresso-machines/francis-francis/illy/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_ESPRESSO_MACHINES_ALT'];?>
">
					<div><img src="/images/menu-mobile/menu-coffeemaker.gif" width="83" height="41" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_ESPRESSO_MACHINES_ALT'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_ESPRESSO_MACHINES_ALT'];?>
"></div>
					<div><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_ESPRESSO_MACHINES'];?>
</div>
					</a>
				</li>
				<li>
					<a href="/tea-makers/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_TEA_MAKERS_ALT'];?>
">
					<div><img src="/images/menu-mobile/menu-teapots.gif" width="83" height="41" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_TEA_MAKERS_ALT'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_TEA_MAKERS_ALT'];?>
"></div>
					<div><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_TEA_MAKERS'];?>
</div>
					</a>
				</li>
				<li>
					<a href="/delivery/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_PAYMENT'];?>
">
					<div><img src="/images/menu-mobile/menu-payment.gif" width="83" height="41" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_PAYMENT'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_PAYMENT'];?>
"></div>
					<div><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_PAYMENT'];?>
</div>
					</a>
				</li>
				<li>
					<a href="/actions/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_ACTIONS_ALT'];?>
">
					<div><img src="/images/menu-mobile/menu-actions.gif" width="83" height="41" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_ACTIONS_ALT'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_ACTIONS_ALT'];?>
"></div>
					<div><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_ACTIONS'];?>
</div>
					</a>
				</li>
				<li>
					<a href="/blog/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_BLOG_ALT'];?>
">
					<div><img src="/images/menu-mobile/menu-blog.gif" width="83" height="41" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_BLOG_ALT'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_BLOG_ALT'];?>
"></div>
					<div><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_BLOG'];?>
</div>
					</a>
				</li>
				<li>
					<a href="/contacts/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_CONTACTS'];?>
">
					<div><img src="/images/menu-mobile/menu-contacts.gif" width="83" height="41" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_CONTACTS'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_CONTACTS'];?>
"></div>
					<div><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MOBILE_CONTACTS'];?>
</div>
					</a>
				</li>
			</ul>
		
		</div>
	</div>


	<div class="main-header-content">
		<div class="top-content">
			<div class="slogan"><a href="/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['SLOGAN'];?>
"><img src="/images/slogan-mobile_<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
.gif" width="263" height="36" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['SLOGAN'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['SLOGAN'];?>
"></a></div>
			<div class="lang-shopico">
				<div class="lang">
					<a href="javascript: showLangs();" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['HEADER_MENU_LANG_ALT'];?>
"><?php if ($_smarty_tpl->tpl_vars['lang']->value == 'UK') {
echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['HEADER_MENU_LANG_UA'];
} else {
echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['HEADER_MENU_LANG_RU'];
}?></a>
					<ul class="hide">
						<li><a href="http://<?php echo $_smarty_tpl->tpl_vars['domen']->value;
echo $_smarty_tpl->tpl_vars['urlChangeLang']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['HEADER_MENU_LANG_UA_TITLE'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['HEADER_MENU_LANG_UA'];?>
</a></li>
						<li><a href="http://<?php echo $_smarty_tpl->tpl_vars['domen']->value;?>
/ru<?php echo $_smarty_tpl->tpl_vars['urlChangeLang']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['HEADER_MENU_LANG_RU_TITLE'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['HEADER_MENU_LANG_RU'];?>
</a></li>
					</ul>
				</div>
				<div class="shopico">
					<a href="/shopping_cart/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['SHOPPING_CART_ICON'];?>
">
					<div>
						<div id="shopping-header-counter"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['cartProductList']->value['total']['qty'])===null||$tmp==='' ? 0 : $tmp);?>
 <?php if ($_smarty_tpl->tpl_vars['cartProductList']->value['total']['qty'] >= 11 && $_smarty_tpl->tpl_vars['cartProductList']->value['total']['qty'] <= 20) {
echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['PRODUCT_NAME_QTY_3'];
} elseif (substr($_smarty_tpl->tpl_vars['cartProductList']->value['total']['qty'],-1,1) == 1) {
echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['PRODUCT_NAME_QTY_1'];
} elseif (substr($_smarty_tpl->tpl_vars['cartProductList']->value['total']['qty'],-1,1) >= 2 && substr($_smarty_tpl->tpl_vars['cartProductList']->value['total']['qty'],-1,1) <= 4) {
echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['PRODUCT_NAME_QTY_2'];
} else {
echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['PRODUCT_NAME_QTY_3'];
}?></div>
						<div><span id="shopping-header-amount"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['cartProductList']->value['total']['amount_sum_arr'][0])===null||$tmp==='' ? 0 : $tmp);?>
</span> <?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['CURRENCY_TEXT'];?>
</div>
					</div>
					<div class="shopico-icon"><img src="/images/shopico-top-mobile.png" width="28" height="28" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['SHOPPING_CART_ICON'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['SHOPPING_CART_ICON'];?>
"></div>
					</a>
				</div>
			</div>
			<div class="contact-search">
				<div class="contact">
					<div>+38 <?php echo $_smarty_tpl->tpl_vars['adminParamData']->value['ADMIN_PHONE_2']['pvalue'];?>
</div>
					<div>+38 <?php echo $_smarty_tpl->tpl_vars['adminParamData']->value['ADMIN_PHONE_1']['pvalue'];?>
</div>
				</div>
				<div class="search">
					<form method="post" action="/search-save/" onSubmit="return searchBtn();">
						<input type="text" name="q" placeholder="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['SEARCH_PLACEHOLDER'];?>
" value="<?php echo $_GET['q'];?>
"><input type="submit" value="">
					</form>
				</div>
			</div>
		</div>
		<div class="menu">
			<ul>
				<li <?php if ($_smarty_tpl->tpl_vars['activeMenu']->value == 'about') {?>class="active"<?php }?>><a href="/about/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ABOUT'];?>
"><span><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ABOUT'];?>
</span></a></li>
				<li class="menu-illy <?php if ($_smarty_tpl->tpl_vars['activeMenu']->value == 'COFFEE') {?>active<?php }?>" onMouseOver="showSubMenu('menu-illy')" onMouseOut="hideSubMenu('menu-illy')">
					<a title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY'];?>
"><span><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY'];?>
</span></a>
					<ul class="submenu hide">
						<li><a href="/coffee/illy/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_ALL_COFFEE'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_ALL_COFFEE'];?>
</a></li>
						<li><a href="/coffee/illy/in-grani/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_IN_GRANI'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_IN_GRANI'];?>
</a></li>
						<li><a href="/coffee/illy/ground/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_GROUND'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_GROUND'];?>
</a></li>
						<li><a href="/coffee/illy/moka/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_MOKA'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_MOKA'];?>
</a></li>
						<li><a href="/coffee/illy/ese/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_ESE'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_ESE'];?>
</a></li>
						<li><a href="/coffee/illy/iperespresso/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_IPERESPRESSO'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_IPERESPRESSO'];?>
</a></li>
					</ul>
				</li>
				<li class="menu-dammann <?php if ($_smarty_tpl->tpl_vars['activeMenu']->value == 'TEA') {?>active<?php }?>" onMouseOver="showSubMenu('menu-dammann')" onMouseOut="hideSubMenu('menu-dammann')">
					<a title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN'];?>
"><span><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN'];?>
</span></a>
					<ul class="submenu hide">
						<li><a href="/tea/dammann/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_ALL_TEA'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_ALL_TEA'];?>
</a></li>
						<li><a href="/tea/dammann/tea-tins/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_TINS'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_TINS'];?>
</a></li>
						<li><a href="/tea/dammann/tea-bags/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_BAGS'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_BAGS'];?>
</a></li>
						<li><a href="/tea/dammann/black/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_BLACK'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_BLACK'];?>
</a></li>
						<li><a href="/tea/dammann/green/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_GREEN'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_GREEN'];?>
</a></li>
						<li><a href="/tea/dammann/herbal/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_HERBA'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_HERBA'];?>
</a></li>
						<li><a href="/tea/dammann/gift-sets/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_GIFT'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_GIFT'];?>
</a></li>
					</ul>
				</li>
				<li class="menu-coffee-tea-maker <?php if ($_smarty_tpl->tpl_vars['activeMenu']->value == 'TEA-MAKERS' || $_smarty_tpl->tpl_vars['activeMenu']->value == 'ESPRESSO-MACHINES') {?>active<?php }?>" onMouseOver="showSubMenu('menu-coffee-tea-maker')" onMouseOut="hideSubMenu('menu-coffee-tea-maker')">
					<a title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MACHINES_MAKERS'];?>
"><span><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MACHINES_MAKERS'];?>
</span></a>
					<ul class="submenu hide">
						<li><a href="/espresso-machines/francis-francis/illy/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ESPRESSO_MACHINES'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ESPRESSO_MACHINES'];?>
</a></li>
						<li><a href="/tea-makers/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_MAKERS'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_MAKERS'];?>
</a></li>
					</ul>
				</li>
				<li class="menu-accessories <?php if ($_smarty_tpl->tpl_vars['activeMenu']->value == 'ACCESSORIES') {?>active<?php }?>" onMouseOver="showSubMenu('menu-accessories')" onMouseOut="hideSubMenu('menu-accessories')">
					<a title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACCESSORIES'];?>
"><span><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACCESSORIES'];?>
</span></a>
					<ul class="submenu hide">
						<li><a href="/accessories/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACCESSORIES_ALL'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACCESSORIES_ALL'];?>
</a></li>
						<li><a href="/accessories/illy/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACCESSORIES_ILLY'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACCESSORIES_ILLY'];?>
</a></li>
						<li><a href="/accessories/dammann/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACCESSORIES_DAMMANN'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACCESSORIES_DAMMANN'];?>
</a></li>
						<!--<li><a href="/accessories/eva-solo/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACCESSORIES_EVA_SOLO'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACCESSORIES_EVA_SOLO'];?>
</a></li>-->
					</ul>
				</li>
				<li <?php if ($_smarty_tpl->tpl_vars['activeMenu']->value == 'delivery') {?>class="active"<?php }?>><a href="/delivery/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_PAYMENT_DELIVERY'];?>
"><span><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_PAYMENT_DELIVERY'];?>
</span></a></li>
				<li <?php if ($_smarty_tpl->tpl_vars['activeMenu']->value == 'actions') {?>class="active"<?php }?>><a href="/actions/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACTIONS'];?>
"><span><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACTIONS'];?>
</span></a></li>
				<li <?php if ($_smarty_tpl->tpl_vars['activeMenu']->value == 'blog') {?>class="active"<?php }?>><a href="/blog/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_BLOG'];?>
"><span><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_BLOG'];?>
</span></a></li>
				<li <?php if ($_smarty_tpl->tpl_vars['activeMenu']->value == 'contacts') {?>class="active"<?php }?>><a href="/contacts/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_CONTACTS'];?>
"><span><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_CONTACTS'];?>
</span></a></li>
			</ul>
		</div>
		
	</div>
</div>

<div class="sBody">
	
	<?php if ($_smarty_tpl->tpl_vars['homeSlider']->value) {?>
	
	<?php $_smarty_tpl->_subTemplateRender("file:home/slider.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
	<?php }?>
	
	
	<div class="top-category">
		<ul>
			<li>
				<a href="/coffee/illy/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_COFFEE_ILLY'];?>
">
				<div><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_COFFEE_ILLY'];?>
</div>
				<img src="/images/main-category/illy.gif" width="40" height="44" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_COFFEE_ILLY'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_COFFEE_ILLY'];?>
">
				</a>
			</li>
			<li class="monoarabica">
				<a href="/coffee/illy/monoarabica/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_ILLY_MONOARABICA'];?>
">
				<div><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_ILLY_MONOARABICA'];?>
</div>
				<img src="/images/main-category/monoarabica.jpg" width="89" height="44" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_ILLY_MONOARABICA'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_ILLY_MONOARABICA'];?>
">
				</a>
			</li>
			<li class="iperespresso">
				<a href="/coffee/illy/iperespresso/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_ILLY_IPERESPRESSO'];?>
">
				<div><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_ILLY_IPERESPRESSO'];?>
</div>
				<img src="/images/main-category/iperespresso.jpg" width="129" height="44" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_ILLY_IPERESPRESSO'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_ILLY_IPERESPRESSO'];?>
">
				</a>
			</li>
			<li>
				<a href="/tea/dammann/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_TEA_DAMMANN'];?>
">
				<div><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_TEA_DAMMANN'];?>
</div>
				<img src="/images/main-category/dammann.jpg" width="86" height="44" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_TEA_DAMMANN'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_TEA_DAMMANN'];?>
">
				</a>
			</li>
			<li>
				<a href="/accessories/dammann/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_ACCESSORIES_DAMMANN'];?>
">
				<div><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_ACCESSORIES_DAMMANN'];?>
</div>
				<img src="/images/main-category/evasolo.gif" width="80" height="44" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_ACCESSORIES_DAMMANN'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_ACCESSORIES_DAMMANN'];?>
">
				</a>
			</li>
			<li class="illy-access">
				<a href="/accessories/illy/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_ACCESSORIES_ILLY'];?>
">
				<div><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_ACCESSORIES_ILLY'];?>
</div>
				<img src="/images/main-category/illy-access.jpg" width="100" height="44" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_ACCESSORIES_ILLY'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['TOP_CATEGORY_ACCESSORIES_ILLY'];?>
">
				</a>
			</li>
		</ul>
	</div>
	
	
	
	<div class="line"></div>
	
	<?php if (!$_smarty_tpl->tpl_vars['homeSlider']->value) {?>
	<div class="breadcrumbs">
		<a href="/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['BREADCRUMBS_MAIN'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['BREADCRUMBS_MAIN'];?>
</a>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['breadcrumbsList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
		/ <a href="<?php echo $_smarty_tpl->tpl_vars['local']->value['link'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
"><?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
</a>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	</div>
	<?php }?>
	
	<?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['include_tpl']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
	
	<div class="clear"></div>
	
</div>


<div class="footer">
	<div class="link-part">
		
		<div class="menu">
			
			<ul class="menu-category">
				<li class="categories">
					<h4><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_CATALOGS'];?>
</h4>
					<ul>
						<li><a href="/coffee/illy/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY'];?>
</a></li>
						<li><a href="/tea/dammann/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN'];?>
</a></li>
						<li><a href="/espresso-machines/francis-francis/illy/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ESPRESSO_MACHINES'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ESPRESSO_MACHINES'];?>
</a></li>
						<li><a href="/tea-makers/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_MAKERS'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_MAKERS'];?>
</a></li>
						<li><a href="/accessories/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACCESSORIES'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACCESSORIES'];?>
</a></li>
					</ul>
				</li>
				<li class="illy">
					<h4><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY'];?>
</h4>
					<ul>
						<li><a href="/coffee/illy/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_ALL_COFFEE'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_ALL_COFFEE'];?>
</a></li>
						<li><a href="/coffee/illy/in-grani/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_IN_GRANI'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_IN_GRANI'];?>
</a></li>
						<li><a href="/coffee/illy/ground/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_GROUND'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_GROUND'];?>
</a></li>
						<li><a href="/coffee/illy/moka/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_MOKA'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_MOKA'];?>
</a></li>
						<li><a href="/coffee/illy/ese/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_ESE'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_ESE'];?>
</a></li>
						<li><a href="/coffee/illy/iperespresso/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_IPERESPRESSO'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_IPERESPRESSO'];?>
</a></li>
						<li><a href="/coffee/illy/monoarabica/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_MONOARABICA'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_COFFEE_ILLY_MONOARABICA'];?>
</a></li>
					</ul>
				</li>
				<li class="dammann">
					<h4><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN'];?>
</h4>
					<ul>
						<li><a href="/tea/dammann/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_ALL_TEA'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_ALL_TEA'];?>
</a></li>
						<li><a href="/tea/dammann/tea-tins/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_TINS'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_TINS'];?>
</a></li>
						<li><a href="/tea/dammann/tea-bags/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_BAGS'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_BAGS'];?>
</a></li>
						<li><a href="/tea/dammann/black/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_BLACK'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_BLACK'];?>
</a></li>
						<li><a href="/tea/dammann/green/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_GREEN'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_GREEN'];?>
</a></li>
						<li><a href="/tea/dammann/herbal/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_HERBA'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_HERBA'];?>
</a></li>
						<li><a href="/tea/dammann/gift-sets/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_GIFT'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_DAMMANN_GIFT'];?>
</a></li>
					</ul>
				</li>
				<li class="accessories">
					<h4><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_MAKERS_ACCESSORIES'];?>
</h4>
					<ul>
						<li><a href="/espresso-machines/francis-francis/illy/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ESPRESSO_MACHINES'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ESPRESSO_MACHINES'];?>
</a></li>
						<li><a href="/tea-makers/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_MAKERS'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TEA_MAKERS'];?>
</a></li>
						<li><a href="/accessories/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACCESSORIES_ALL'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACCESSORIES_ALL'];?>
</a></li>
						<li><a href="/accessories/illy/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACCESSORIES_ILLY'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACCESSORIES_ILLY'];?>
</a></li>
						<li><a href="/accessories/dammann/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACCESSORIES_DAMMANN'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACCESSORIES_DAMMANN'];?>
</a></li>
						<!--<li><a href="/accessories/eva-solo/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACCESSORIES_EVA_SOLO'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACCESSORIES_EVA_SOLO'];?>
</a></li>-->
						
					</ul>
				</li>
				<li class="information">
					<h4><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_INFO'];?>
</h4>
					<ul>
						<li><a href="/about/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ABOUT'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ABOUT'];?>
</a></li>
						<li><a href="/delivery/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_PAYMENT_DELIVERY'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_PAYMENT_DELIVERY'];?>
</a></li>
						<li><a href="/actions/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACTIONS'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_ACTIONS'];?>
</a></li>
						<li><a href="/blog/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_BLOG'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_BLOG'];?>
</a></li>
						<li><a href="/contacts/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_CONTACTS'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_CONTACTS'];?>
</a></li>
						<li><a href="/map/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MAPS'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_MAPS'];?>
</a></li>
						<li><a href="/terms/" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TERMS'];?>
"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['MENU_TERMS'];?>
</a></li>
					</ul>
				</li>
			</ul>
			
			<div class="clear"></div>
			
		</div>
		
		<div class="copyright"><?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['COPYRIGHT'];?>
</div>
		
	</div>
	
	<div class="contact-part">
		<div class="contact">
			<div><span>+38 <?php echo $_smarty_tpl->tpl_vars['adminParamData']->value['ADMIN_PHONE_2']['footer'][0];?>
</span> <?php echo $_smarty_tpl->tpl_vars['adminParamData']->value['ADMIN_PHONE_2']['footer'][1];?>
</div>
			<div><span>+38 <?php echo $_smarty_tpl->tpl_vars['adminParamData']->value['ADMIN_PHONE_1']['footer'][0];?>
</span> <?php echo $_smarty_tpl->tpl_vars['adminParamData']->value['ADMIN_PHONE_1']['footer'][1];?>
</div>
			<div><a href="mailto: <?php echo $_smarty_tpl->tpl_vars['adminParamData']->value['ADMIN_MAIL']['pvalue'];?>
" title=""><?php echo $_smarty_tpl->tpl_vars['adminParamData']->value['ADMIN_MAIL']['pvalue'];?>
</a></div>
		</div>
		<div class="facebook">
			<a href="https://www.facebook.com/coffeestoreonline/" target="_blank" class="facebook-desktop" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['FOOTER_FACEBOOK'];?>
"><img src="/images/footer-fb.gif" width="183" height="48" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['FOOTER_FACEBOOK'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['FOOTER_FACEBOOK'];?>
"></a>
			<a href="https://www.facebook.com/coffeestoreonline/" target="_blank" class="facebook-mobile" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['FOOTER_FACEBOOK'];?>
"><img src="/images/footer-fb-mobile.gif" width="31" height="48" alt="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['FOOTER_FACEBOOK'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['FOOTER_FACEBOOK'];?>
"></a>
		</div>
	</div>
	
</div>

</div>

<?php $_smarty_tpl->_subTemplateRender("file:common/message.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php echo '<script'; ?>
 type="text/javascript" language="javascript">

$( window ).resize(function() {
	disabledMobileMenu();
});


initDialogMessage();

<?php if ($_smarty_tpl->tpl_vars['messageArr']->value) {?>
showMessage();
<?php }
echo '</script'; ?>
>


</body>
</html>
<?php }
}
