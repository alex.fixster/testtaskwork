<?php
/* Smarty version 3.1.39, created on 2022-01-17 15:28:58
  from '/Users/alex/www/coffee-store/templates/admin/products/_edit_related.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e56f1ace9b27_25946441',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b13d1cbde5ea6c4302c2a0acf95f4da82070c52d' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/products/_edit_related.html',
      1 => 1642426137,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61e56f1ace9b27_25946441 (Smarty_Internal_Template $_smarty_tpl) {
?>	<div class="col-xs-3" id="divRelated_<?php echo $_smarty_tpl->tpl_vars['local']->value['related_product_id'];?>
">
    	<input type="hidden" name="related[<?php echo $_smarty_tpl->tpl_vars['local']->value['related_product_id'];?>
]" value="<?php echo $_smarty_tpl->tpl_vars['local']->value['related_product_id'];?>
">
		<div style="float:left; margin-right:5px; margin-bottom:7px;"><img width="100" height="100" src="/upl_files/product/size.150/<?php echo (($tmp = @$_smarty_tpl->tpl_vars['local']->value['image'])===null||$tmp==='' ? 'logo.jpg' : $tmp);?>
" alt="" style="border:solid 1px #A4A3A3;"></div>
        <div><em><?php echo $_smarty_tpl->tpl_vars['local']->value['brand'];?>
</em></div>
		<div style="font-size:15px; margin-bottom:5px;"><?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
</div>
        <div class="action-buttons"><a href="javascript: deleteRelatedProduct(<?php echo $_smarty_tpl->tpl_vars['local']->value['related_product_id'];?>
)" class="text-danger"><i class="fa fa-trash"></i> Видалити</a></div>
	</div><?php }
}
