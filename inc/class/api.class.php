<?
class api {

	
	
	function getAllProduct($category_id = array(), $subcategoryOpt = false) {
		
		global $product, $category;

		if ($subcategoryOpt == "Y" and $category_id and is_array($category_id)) {

			foreach ($category_id as $key => $val) {

				$val = (int)$val;

				if ($val) {

					$categoryTreeList = $category->getCategoryTree($val, true);

					$category_id = array_merge($category_id, array_keys($categoryTreeList));

				}

			}

			

		}
		
		$productList = $product->getApiAllProduct($category_id);

		return $productList;
		
	}



	function getProductInfo($id) {

		global $product;

		$id = (int)$id;

		if ($id) {

			$productInfo = $product->getApiProductInfo($id);

			return $productInfo;

		}

		else return false;

	}



	function getAllCategory($id = 0, $productOpt = false) {

		global $category;

		$id = (int)$id;

		$categorylist = $category->getCategoryTree($id, false, $productOpt);

		// if ($productOpt == "Y") {

		// 	$categorylist = $this->_getProductToCategoryTree($categorylist);

		// }

		$returnData = $this->_normalizeDataCategoryTree($categorylist);

		return $returnData;

	}



	// private function _getProductToCategoryTree($categorylist) {

	// 	global $product;

	// 	foreach ($categorylist as $key => $val) {

	// 		$productList = $product->getAllProduct(array("category_id" => array($val['id'])));

	// 		if ($productList) {

	// 			foreach ($productList as $k => $v) {

	// 				$categorylist[$key]['product_list'][$v['id']]['id'] = $v['id'];
	// 				$categorylist[$key]['product_list'][$v['id']]['title'] = $v['header'];
	// 				$categorylist[$key]['product_list'][$v['id']]['price'] = $v['price'];

	// 			}

	// 		}

	// 		if ($val['parent_list']) $this->_getProductToCategoryTree($val['parent_list']);

	// 	}

	// 	return $categorylist;

	// }




	private function _normalizeDataCategoryTree($categorylist) {

		foreach ($categorylist as $key => $val) {

			$returnData[$val['id']]['id'] = $val['id'];
			$returnData[$val['id']]['title'] = $val['header'];
			$returnData[$val['id']]['product_qty'] = $val['product_qty'];
			$returnData[$val['id']]['subcategory_qty'] = $val['subcategory_qty'];

			if ($val['product_list']) $returnData[$val['id']]['product_list'] = $val['product_list'];

			if ($val['parent_list']) $returnData[$val['id']]['parent_list'] = $this->_normalizeDataCategoryTree($val['parent_list']);

		}

		return $returnData;

	}



	function getCategoryOne($id, $productOpt = false, $subcategoryOpt = false) {

		if ($id and is_array($id)) {

			foreach ($id as $key => $val) {

				$val = (int)$val;

				if ($val) {

					$returnData[$val] = $this->_getCategoryItem($id, $productOpt, $subcategoryOpt);

				}

			}

			return $returnData;

		}

		else return false;

	}



	private function _getCategoryItem($id, $productOpt = false, $subcategoryOpt = false) {

		global $category, $product;

		$categoryInfo = $category->getCategoryInfo($id);

		$returnData['id'] = $categoryInfo['id'];
		$returnData['title'] = $categoryInfo['header'];
		$returnData['product_qty'] = $categoryInfo['product_qty'];
		$returnData['subcategory_qty'] = $categoryInfo['subcategory_qty'];

		if ($productOpt == "Y") {

			$productList = $product->getAllProduct(array("category_id" => array($categoryInfo['id'])));

			if ($productList) {

				foreach ($productList as $key => $val) {

					$returnData['product_list'][$val['id']]['id'] = $val['id'];
					$returnData['product_list'][$val['id']]['title'] = $val['header'];
					$returnData['product_list'][$val['id']]['price'] = $val['price'];

				}

			}

		}


		if ($subcategoryOpt == "Y") {

			$returnData['parent_list'] = $this->getAllCategory($id, $productOpt);

		}

		return $returnData;

	}




	function getCategoryBreadcrumb($category_id) {

		global $category;

		if ($category_id and is_array($category_id)) {

			foreach($category_id as $key => $val) {

				$val = (int)$val;

				if ($val) {

					$breadcrumbCategoryList = $category->getBreadcrumb($val);
					
					if ($breadcrumbCategoryList) {

						foreach ($breadcrumbCategoryList as $k => $v) {

							$returnData[$val][$v['id']]['id'] = $v['id'];
							$returnData[$val][$v['id']]['title'] = $v['header'];

						}

					}

				}

			}

			return $returnData;

		}

		else return false;

	}



	function getProductCount($category_id = array()) {

		global $db, $zend_db;

		// SELECT COUNT(DISTINCT product_id) FROM testtask.product_category WHERE category_id IN (2,3);

		$select = $zend_db->select()
							->from("product_category", array("qty" => "COUNT(DISTINCT product_id)"));

		if ($category_id and is_array($category_id)) {

			foreach ($category_id as $key => $val) {

				if ((int)$val) $idArr[] = (int)$val;

			}

			if ($idArr) $select->where("category_id IN (".implode(",", $idArr).")");

		}

		// dump($select->__toString());exit;

		$returnData = $db->getOne($select->__toString());

		return $returnData;

	}
	
	
}
?>