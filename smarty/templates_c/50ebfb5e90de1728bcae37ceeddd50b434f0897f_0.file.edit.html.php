<?php
/* Smarty version 3.1.39, created on 2022-01-26 22:53:45
  from '/Users/alex/www/coffee-store/templates/admin/order/edit.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61f1b4d989d2f9_30084918',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '50ebfb5e90de1728bcae37ceeddd50b434f0897f' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/order/edit.html',
      1 => 1643230419,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:admin/order/_product_popup.html' => 1,
    'file:admin/order/_sample_product_popup.html' => 1,
    'file:admin/order/_user_comment.html' => 1,
    'file:admin/order/_bank_data.html' => 1,
    'file:admin/order/product.html' => 1,
  ),
),false)) {
function content_61f1b4d989d2f9_30084918 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/Users/alex/www/coffee-store/smarty/plugins/function.counter.php','function'=>'smarty_function_counter',),));
echo '<script'; ?>
 type="text/javascript" src="/library/chosen/1.1.0/chosen.jquery.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" src="/library/bootstrap/3.1.1/js/popover.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" language="javascript" src="/admin/js/order_edit.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" src="/library/typeahead/0.10.5/typeahead.bundle.js"><?php echo '</script'; ?>
>
<link href="/library/typeahead/0.10.5/typeahead.css" type="text/css" rel="stylesheet">
<?php echo '<script'; ?>
 type="text/javascript" src="/library/handlebars/handlebars-v4.0.4.js"><?php echo '</script'; ?>
>

<style type="text/css">
#scrollable-dropdown-menu .tt-dropdown-menu {
  max-height: 300px;
  overflow-y: auto;
}

</style>

<?php echo '<script'; ?>
 type="text/javascript" language="javascript">
var basePackingCost = '<?php echo $_smarty_tpl->tpl_vars['basePackingCost']->value;?>
';
var envelopePriceData = new Array();
envelopePriceData['white'] = '<?php echo $_smarty_tpl->tpl_vars['packingParam']->value['ORDER_COST_ENV_WHITE']['pvalue'];?>
';
envelopePriceData['black'] = '<?php echo $_smarty_tpl->tpl_vars['packingParam']->value['ORDER_COST_ENV_BLACK']['pvalue'];?>
';

var categoryGroupList = new Array();
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categoryGroups']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
categoryGroupList['<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
'] = '<?php echo $_smarty_tpl->tpl_vars['local']->value;?>
';
<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
echo '</script'; ?>
>




<?php $_smarty_tpl->_subTemplateRender("file:admin/order/_product_popup.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender("file:admin/order/_sample_product_popup.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender("file:admin/order/_user_comment.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>




<div class="page-header">
	<h1>
		<?php if ($_smarty_tpl->tpl_vars['orderInfo']->value['id']) {?>Редагування<?php } else { ?>Створення<?php }?> замовлення
        <span style="float:right;"><a href="/admin/order/add/" class="btn btn-success btn-sm no-border"><span class="fa fa-ticket"></span>&nbsp;&nbsp;&nbsp;Добавити замовлення</a></span>
    </h1>
</div>

<div style="height:10px;"></div>

<form class="form-horizontal" role="form" action="/admin/order/update/" method="post">
<input type="hidden" name="id" id="inputID" value="<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['id'];?>
">

<?php $_smarty_tpl->_subTemplateRender("file:admin/order/_bank_data.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<div class="row">
<div class="col-xs-7">

	<div class="form-group">
		<label for="inputClientPhone" class="col-xs-4 normal">Телефон*</label>
		<div class="col-xs-8">
			<div class="input-group">
				<span class="input-group-addon">
					+38
				</span>
				<input type="hidden" id="inputClientID" name="user_id" value="<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['user_id'];?>
">
				<input type="text" id="inputClientPhone" name="client_phone" class="form-control" style="height:37px;" placeholder="Введіть телефон клієнта" value="<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['phone'];?>
" onKeyUp="getUserID()">
				<span class="input-group-addon" id="clientCheck" style="border:0; background-color:#FFF; padding-bottom:10px;"><i class="fa <?php if ($_smarty_tpl->tpl_vars['orderInfo']->value['user_id']) {?>fa-check text-success<?php } else { ?>fa-close text-danger<?php }?> bigger-150"></i></span>
			</div>
		</div>
	</div>

	<div class="form-group" id="formMail">
		<label for="inputClientName" class="col-xs-4 normal">Ім'я клієнта*</label>
		<div class="col-xs-8">
			<div class="input-group">
				<input type="text" id="inputClientName" name="client" class="form-control" placeholder="Імя, телефон, e-mail" value="<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['name'];?>
">
				<span class="input-group-addon"  style="border:0; background-color:#FFF; padding-bottom:10px;"><a href="javascript: loadToEditUserComment(<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['user_id'];?>
)" class="<?php if (!$_smarty_tpl->tpl_vars['orderInfo']->value['user_id']) {
}?>" id="divUserNoteIcon"><i class="fa fa-comment bigger-150"></i></a></span>
			</div>
			<div class="alert alert-danger <?php if (!$_smarty_tpl->tpl_vars['orderInfo']->value['user_note']) {?>hide<?php }?>" id="divUserNote" style="padding:5px 7px 5px 7px; margin-top:3px; font-size:12px; line-height:15px;"><?php echo nl2br($_smarty_tpl->tpl_vars['orderInfo']->value['user_note']);?>
</div>
		</div>
	</div>
    
    <div class="form-group">
		<label for="inputClientMail" class="col-xs-4 normal">E-mail</label>
		<div class="col-xs-8">
			<input type="text" id="inputClientMail" name="client_mail" class="form-control" placeholder="Введіть e-mail клієнта" value="<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['mail'];?>
">
		</div>
	</div>
    
    <div class="form-group">
		<label for="inputOrderNum" class="col-xs-4 normal"># замовлення*</label>
		<div class="col-xs-3">
			<input type="text" id="inputOrderNum" name="order_num" readonly class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['order_num'];?>
">
		</div>
	</div>
    
	<div class="form-group">
		<label for="inputStatus" class="col-xs-4 normal">Статус*</label>
		<div class="col-xs-8">
			<select id="inputStatus" name="status_id" class="form-control chosen-select">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['statusList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>           
            <option data-brand="<?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['orderInfo']->value['status_id'] == $_smarty_tpl->tpl_vars['local']->value['id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
</option>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </select>
		</div>
	</div>
    
   	<div class="form-group" id="formCoupon">
		<label for="inputCoupon" class="col-xs-4 normal">Купон</label>
		<div class="col-xs-8">
        	<div class="input-group">
                <input id="inputCoupon" class="form-control input-mask-product" name="coupon" placeholder="Введіть купон" type="text" value="<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['coupon_code'];?>
" onBlur="checkCoupon();">
                <span class="input-group-addon" id="checkCoupon" style="border-left:0;">
               		<i class="ace-icon fa fa-check text-success"></i>
                </span>
				
				<span class="input-group-addon" id="spanSearchCoupon" style="border:0; background:none;">
               		<a href="javascript: searchCoupon();"><i class="ace-icon fa fa-search bigger-150" style="margin-top:-4px;"></i></a>
                </span>
				
            </div>
			<div class="text-danger" id="couponMessage" style="font-size:11px; padding-top:2px;"></div>
			<div class="alert-success hide" style="padding:3px 5px 3px 5px;" id="coupon-percent"></div>
		</div>
	</div>
	
	<div class="form-group" id="formMail">
		<label for="inputIndDiscount" class="col-xs-4 normal">Інд. знижка (%):</label>
		<div class="col-xs-2">
			<input type="text" id="inputIndDiscount" name="ind_discount" class="form-control" placeholder="0.00" value="<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['ind_discount'];?>
">
		</div>
	</div>

</div>
<div class="col-xs-1"></div>


<div class="col-xs-4">
	<?php if ($_smarty_tpl->tpl_vars['orderInfo']->value['id']) {?>
	<table class="table table-bordered">
		<tbody>
			<tr>
				<td colspan="4" class="text-right" style="vertical-align:middle; font-size:14px;">ВАРТІСТЬ</td>
				<td class="text-center" style="background-color:#fcfcfc;">
					<strong><div style="font-size:18px; float:left;"><?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['total']['PRODUCT_AMOUNT']['pvalue_arr'][0];?>
</div><div style="float:left;">&nbsp;<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['total']['PRODUCT_AMOUNT']['pvalue_arr'][1];?>
</div></strong>
				</td>
			</tr>
			<tr <?php if (!$_smarty_tpl->tpl_vars['orderInfo']->value['total']['DISCOUNT_AMOUNT']['pvalue']) {?>class="hide"<?php }?>>
				<td colspan="4" class="text-right" style="vertical-align:middle; font-size:14px;">ЗНИЖКА</td>
				<td class="text-center" style="background-color:#fcfcfc;">
					<strong><div style="font-size:18px; float:left;">-<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['total']['DISCOUNT_AMOUNT']['pvalue_arr'][0];?>
</div><div style="float:left;">&nbsp;<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['total']['DISCOUNT_AMOUNT']['pvalue_arr'][1];?>
</div></strong>
				</td>
			</tr>
			<tr <?php if (!$_smarty_tpl->tpl_vars['orderInfo']->value['total']['COUPON_DISCOUNT_AMOUNT']['pvalue']) {?>class="hide"<?php }?>>
				<td colspan="4" class="text-right" style="vertical-align:middle; font-size:14px;">ЗНИЖКА КУПОНА</td>
				<td class="text-center" style="background-color:#fcfcfc;">
					<strong><div style="font-size:18px; float:left;">-<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['total']['COUPON_DISCOUNT_AMOUNT']['pvalue_arr'][0];?>
</div><div style="float:left;">&nbsp;<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['total']['COUPON_DISCOUNT_AMOUNT']['pvalue_arr'][1];?>
</div></strong>
				</td>
			</tr>
			<tr>
				<td colspan="4" class="text-right" style="vertical-align:middle; font-size:14px;">ДОСТАВКА</td>
				<td style="background-color:#fcfcfc;">
					<?php if ($_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['free_delivery'] == 'Y') {?>
					<div class="label label-danger label-xlg" style="text-transform:uppercase; padding-top:7px; height:30px;">Безкоштовна</div>
					<?php } else { ?>
					<div class="label label-xlg" style="text-transform:uppercase; padding-top:7px; height:30px;">Клієнт</div>
					<?php }?>
				</td>
			</tr>
			<tr style="background-color:#ebeaea;">
				<td colspan="4" class="text-right" style="vertical-align:middle; font-size:14px;"><strong>ДО ОПЛАТИ</strong></td>
				<td class="text-center">
					<strong><div style="font-size:20px; float:left;"><?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['total']['TOTAL_AMOUNT']['pvalue_arr'][0];?>
</div><div style="float:left;">&nbsp;<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['total']['TOTAL_AMOUNT']['pvalue_arr'][1];?>
</div></strong>
				</td>
			</tr>
		</tbody>
	</table>
	
	
	
	<div class="text-center">
		<a href="javascript: showPopupSMSCard();" class="btn btn-warning" style="width:142px;"><i class="fa fa-comment-o"></i> SMS рахунок</a>
		
		<a href="javascript: showPopupEmailCard();" class="btn btn-warning <?php if (!$_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['mail']) {?>disabled<?php }?>" style="width:142px;"><i class="fa fa-envelope-o"></i> Email рахунок</a>
	</div>
	
	<div class="text-center" style="padding-top:5px;">
		<a href="/admin/order/download_order_file/?order_id=<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['id'];?>
" class="btn btn-info" style="width:288px;"><i class="fa fa-file-text"></i> Скачати накладу</a>
	</div>
	
	<div class="text-center" style="padding-top:5px;">
		<a href="javascript: showPopupSMSDeclatarion();" class="btn btn-purple <?php if (!$_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['np_num']) {?>disabled<?php }?>" style="width:288px;"><i class="fa fa-truck"></i> Відправити номер накладної</a>
	</div>
	
	<div class="text-center <?php if ($_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['payment_type'] != 'bank') {?>hide<?php }?>" id="bankDocsBtn" style="padding-top:15px;">
		<a href="/admin/order/download_bank_file/?order_id=<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['id'];?>
" class="btn btn-inverse btnBankDocs <?php if (!$_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['bank_pay_header'] || !$_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['bank_pay_address'] || !$_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['bank_pay_account']) {?>disabled<?php }?>" style="width:142px;"><i class="fa fa-file"></i> Рахунок</a>
		
		<a href="/admin/order/download_expense_file/?order_id=<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['id'];?>
" class="btn btn-inverse btnBankDocs <?php if (!$_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['bank_pay_header'] || !$_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['bank_pay_address'] || !$_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['bank_pay_account']) {?>disabled<?php }?>" style="width:142px;"><i class="fa fa-inbox"></i> Видаткова</a>
	</div>
	<?php }?>
</div>
</div>


<hr>


<h4>Продукти</h4>

<div class="row">
	<table class="table table-bordered" style="max-width:1000px;">
        <thead>
            <tr>
                <th width="110"></th>
                <th>Назва продукту</th>
                <th width="120" class="text-center">Ціна</th>
                <th width="65" class="text-center">К-сть</th>
                <th width="120" class="text-center">Сума</th>
                <th width="50" class="text-center">Дії</th>
            </tr>
        </thead>
        <tbody id="tBodyProductList">
        	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['orderInfo']->value['product_list'], 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
            <?php echo smarty_function_counter(array('assign'=>"num",'start'=>1),$_smarty_tpl);?>

            <?php $_smarty_tpl->_subTemplateRender("file:admin/order/product.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
			<?php
}
if ($_smarty_tpl->tpl_vars['local']->do_else) {
?>
			<tr id="tBodyProductList_noProducts">
				<td class="text-center" colspan="6">Продукти в замолення не добавлені</td>
			</tr>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </tbody>
    </table>

	
</div>
<div><a href="javascript: productPopup()" class="btn btn-info btn-xs"><i class="fa fa-plus"></i> Добавити продукт</a></div>


<hr>



<h4>Доставка</h4>

<div class="row">
	
	<div class="col-xs-7">

		<div class="form-group" id="formMail">
			<label for="inputCity" class="col-xs-4 normal">Місто*</label>
			<div class="col-xs-8" id="scrollable-dropdown-menu">
				<input type="text" id="inputCity" name="city" class="form-control" placeholder="Введіть місто доставки" value="<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['city'];?>
">
			</div>
			<input type="hidden" id="inputCityNPRef" name="city_np_ref" value="<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['city_np_ref'];?>
">
		</div>
	    
	    <div class="form-group">
			<label for="inputNovaPoshtaWharehouse" class="col-xs-4 normal">Склад Нової Пошти*</label>
			<div class="col-xs-8">
				<select class="form-control chosen-select" onchange="changeWharehouse()" name="novaposhra_wharehouse_ref" id="inputNovaPoshtaWharehouse" data-placeholder="Спочатку оберіть місто"></select>
			</div>
			<input type="hidden" id="novaposhra_wharehouse" name="novaposhra_wharehouse" value="<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['novaposhra_wharehouse'];?>
">
		</div>

		<div class="form-group">
			<label for="inputAddress" class="col-xs-4 normal">Адресна доставка*</label>
			<div class="col-xs-8">
				<input type="text" id="inputAddress" name="address" class="form-control" placeholder="Введіть адресу клієнта" value="">
			</div>
		</div>

		
		
		<div class="form-group">
			<label for="inputDeliveryType" class="col-xs-4 normal">Тип доставки*</label>
			<div class="col-xs-8">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['deliveryTypeList']->value['UK'], 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
				<input type="radio" id="inputDeliveryType<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" name="delivery_type" value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['key']->value == $_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['delivery_type']) {?>checked<?php }?>> <label for="inputDeliveryType<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['local']->value;?>
</label><br>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputPaymentType" class="col-xs-4 normal">Тип оплати*</label>
			<div class="col-xs-8">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['paymentTypeList']->value['UK'], 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
				<input type="radio" id="inputPaymentType<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" name="payment_type" value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['key']->value == $_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['payment_type']) {?>checked<?php }?> onClick="changePayment('<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
')"> <label for="inputPaymentType<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['local']->value;?>
</label><br>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
				
				<div class="alert alert-info hide" id="paymentAlert" style="padding:5px 8px 5px 8px; line-height:18px; font-size:12px;">
					Даний спосіб оплати має додаткові витрати зі сторони отримувача (клієнта):<br>- зворотня доставка оплати: <?php echo $_smarty_tpl->tpl_vars['deliveryParam']->value['DELIVERY_EX_NP_AMOUNT']['pvalue'];?>
 грн + <?php echo $_smarty_tpl->tpl_vars['deliveryParam']->value['DELIVERY_EX_NP_PERCENT']['pvalue'];?>
%
				</div>
				
				<div id="bankAccountShowBtn" class="hide"><a href="javascript: showBankData()" class="btn btn-minier" style="width:160px;">Дані про підприємство</a></div>
			</div>
		</div>
		
		 <div class="form-group">
			<label for="inputNpNum" class="col-xs-4 normal">Номер декларації</label>
			<div class="col-xs-7">
				<input type="text" id="inputNpNum" name="np_num" class="form-control" placeholder="Введіть номер декларації" value="<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['np_num'];?>
">
			</div>
			<div class="col-xs-1">
				<?php if ($_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['np_num']) {?>
				<a href="https://novaposhta.ua/tracking/?cargo_number=<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['np_num'];?>
" target="_blank"><i class="fa fa-binoculars bigger-150" style="padding-top:5px;"></i></a>
				<?php }?>
			</div>
		</div>
	</div>
	
	
	<div class="col-xs-5">
		
		<div class="form-group">
			<div class="row">
				<div class="col-xs-12">
					<input type="hidden" name="sample_product_id" id="sample_product_id" value="<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['sample_product_id'];?>
">
					<input type="hidden" id="sampleProductPrice" value="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['sample_product_info']['price'])===null||$tmp==='' ? '0' : $tmp);?>
">
					<div class="col-xs-12">
						<div style="float:left; width:140px;" id="sampleProductImage">
							<?php if ($_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['sample_product_info']['image']) {?>
							<img src="/upl_files/product/size.150/<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['sample_product_info']['image'];?>
" width="120" height="120">
							<?php } else { ?>
							<img src="/admin/images/coffee-bag.jpg" width="120" height="120">
							<?php }?>
						</div>
						<div>
							<div style="font-size:14px; height:32px; font-weight:bold;" id="sampleProductHeader"><?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['sample_product_info']['brand_header'];?>
 <?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['sample_product_info']['header'];?>
</div>
							<div><a href="javascript: showSampleProductPopup()" class="btn btn-primary"><i class="fa fa-gift"></i> Вибрати пробнік</a></div>
							<div style="padding-top:15px;" id="sampleProductRemove" <?php if (!$_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['sample_product_info']['image']) {?>class="hide"<?php }?>><a href="javascript: deleteSampleProduct();" class="text-danger"><i class="fa fa-trash"></i>  Видалити пробнік</a></div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		
		<div class="form-group">
			<label for="inputPack" class="col-xs-3 normal">Упаковка</label>
			<div class="col-xs-8" id="packList">
				<div class="input-group">
					<select name="delivery[pack_id][]" class="form-control" onChange="calcPakingAmount();">
						<option data-price="0" value="0">--- Виберіть упаковку ---</option>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['packList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>           
						<option data-price="<?php echo $_smarty_tpl->tpl_vars['local']->value['price'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['pack_id'][0]['pack_id'] == $_smarty_tpl->tpl_vars['local']->value['id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
 - <?php echo $_smarty_tpl->tpl_vars['local']->value['in_stock'];?>
 шт</option>
						<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					</select>
					<span class="input-group-addon" style="border:0; background-color:#FFF; padding-bottom:0;"><a href="javascript: addPackForm()"><i class="fa fa-plus bigger-150"></i></a></span>
				</div>
				
				<?php if ($_smarty_tpl->tpl_vars['orderInfo']->value['delivery'] && count($_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['pack_id']) > 1) {?>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['pack_id'], 'local_p', false, 'key_p');
$_smarty_tpl->tpl_vars['local_p']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key_p']->value => $_smarty_tpl->tpl_vars['local_p']->value) {
$_smarty_tpl->tpl_vars['local_p']->do_else = false;
?>
					<?php if ($_smarty_tpl->tpl_vars['key_p']->value) {?>
					<div style="padding-top:5px;"></div>
					<select name="delivery[pack_id][]" class="form-control" onChange="calcPakingAmount();">
						<option data-price="0" value="0">--- Виберіть упаковку ---</option>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['packList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>           
						<option data-price="<?php echo $_smarty_tpl->tpl_vars['local']->value['price'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['local_p']->value['pack_id'] == $_smarty_tpl->tpl_vars['local']->value['id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
 - <?php echo $_smarty_tpl->tpl_vars['local']->value['in_stock'];?>
 шт</option>
						<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					</select>
					<?php }?>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
				<?php }?>
				
			</div>
		</div>
		
		
		<div class="form-group">
			<label for="inputPackingAmount" class="col-xs-3 normal">Конверт</label>
			<div class="col-xs-8">
				<a href="javascript: checkEnvelope('white');"><img src="/admin/images/env_white.jpg" width="130" height="66" style="margin-right:30px;"><img src="/admin/images/env-ok.png" id="envelope_white_check" width="41" height="41" <?php if ($_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['envelope'] != 'white') {?>class="hide"<?php }?> style="position:absolute; left:107px; top:30px;"></a>
				<a href="javascript: checkEnvelope('black');"><img src="/admin/images/env_black.jpg" width="130" height="66"><img src="/admin/images/env-ok.png" id="envelope_black_check" width="41" height="41" <?php if ($_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['envelope'] != 'black') {?>class="hide"<?php }?> style="position:absolute; left:272px; top:30px;"></a>
				<input type="hidden" name="envelope" id="envelope" value="<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['envelope'];?>
">
			</div>
		</div>
		
		
		<div class="form-group">
			<label for="inputPackingAmount" class="col-xs-4 normal">Вартість упакування</label>
			<div class="col-xs-3">
				<input type="text" id="inputPackingAmount" name="packing_amount" class="form-control" placeholder="0.00" value="<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['packing_amount'];?>
">
			</div>
		</div>
		
		<div class="form-group">
			<label for="inputDeliveryAmount" class="col-xs-4 normal">Вартість доставки</label>
			<div class="col-xs-3">
				<input type="text" id="inputDeliveryAmount" name="delivery_amount" class="form-control" placeholder="0.00" value="<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['delivery_amount'];?>
">
			</div>
		</div>
		
	</div>
	
</div>

<div style="clear:both;"></div>


<hr>

<h4>Коментарії</h4>

<div class="row">
<div class="col-xs-7">

	<div class="form-group" id="formMail">
		<label for="inputNote" class="col-xs-4 normal">Коментарій клієнта</label>
		<div class="col-xs-8">
			<textarea id="inputNote" name="note" class="form-control" rows="5" placeholder="Коментарій клієнта"><?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['note'];?>
</textarea>
		</div>
	</div>
    
    <div class="form-group">
		<label for="inputManageNote" class="col-xs-4 normal">Коментарій менеджера</label>
		<div class="col-xs-8">
			<textarea id="inputManageNote" name="note_manager" rows="5" class="form-control" placeholder="Коментарій менеджера"><?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['note_manager'];?>
</textarea>
		</div>
	</div>
</div>
<div class="col-xs-1"></div>
<div class="col-xs-4">
	<?php if ($_smarty_tpl->tpl_vars['orderInfo']->value['id']) {?>
	<table class="table table-bordered">
		<tbody>
			<tr>
				<td colspan="4" class="text-right" style="vertical-align:middle; font-size:14px;">ВАРТІСТЬ</td>
				<td class="text-center" style="background-color:#fcfcfc;">
					<strong><div style="font-size:18px; float:left;"><?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['total']['PRODUCT_AMOUNT']['pvalue_arr'][0];?>
</div><div style="float:left;">&nbsp;<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['total']['PRODUCT_AMOUNT']['pvalue_arr'][1];?>
</div></strong>
				</td>
			</tr>
			<tr <?php if (!$_smarty_tpl->tpl_vars['orderInfo']->value['total']['DISCOUNT_AMOUNT']['pvalue']) {?>class="hide"<?php }?>>
				<td colspan="4" class="text-right" style="vertical-align:middle; font-size:14px;">ЗНИЖКА</td>
				<td class="text-center" style="background-color:#fcfcfc;">
					<strong><div style="font-size:18px; float:left;">-<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['total']['DISCOUNT_AMOUNT']['pvalue_arr'][0];?>
</div><div style="float:left;">&nbsp;<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['total']['DISCOUNT_AMOUNT']['pvalue_arr'][1];?>
</div></strong>
				</td>
			</tr>
			<tr <?php if (!$_smarty_tpl->tpl_vars['orderInfo']->value['total']['COUPON_DISCOUNT_AMOUNT']['pvalue']) {?>class="hide"<?php }?>>
				<td colspan="4" class="text-right" style="vertical-align:middle; font-size:14px;">ЗНИЖКА КУПОНА</td>
				<td class="text-center" style="background-color:#fcfcfc;">
					<strong><div style="font-size:18px; float:left;">-<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['total']['COUPON_DISCOUNT_AMOUNT']['pvalue_arr'][0];?>
</div><div style="float:left;">&nbsp;<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['total']['COUPON_DISCOUNT_AMOUNT']['pvalue_arr'][1];?>
</div></strong>
				</td>
			</tr>
			<tr>
				<td colspan="4" class="text-right" style="vertical-align:middle; font-size:14px;">ДОСТАВКА</td>
				<td style="background-color:#fcfcfc;">
					<?php if ($_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['free_delivery'] == 'Y') {?>
					<div class="label label-danger label-xlg" style="text-transform:uppercase; padding-top:7px; height:30px;">Безкоштовна</div>
					<?php } else { ?>
					<div class="label label-xlg" style="text-transform:uppercase; padding-top:7px; height:30px;">Клієнт</div>
					<?php }?>
				</td>
			</tr>
			<tr style="background-color:#ebeaea;">
				<td colspan="4" class="text-right" style="vertical-align:middle; font-size:14px;"><strong>ДО ОПЛАТИ</strong></td>
				<td class="text-center">
					<strong><div style="font-size:20px; float:left;"><?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['total']['TOTAL_AMOUNT']['pvalue_arr'][0];?>
</div><div style="float:left;">&nbsp;<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['total']['TOTAL_AMOUNT']['pvalue_arr'][1];?>
</div></strong>
				</td>
			</tr>
		</tbody>
	</table>
	
	<?php }?>
</div>
</div>


<hr>
<div class="col-xs-12" style="padding-top:20px;">
	
	<div class="form-group">
		<div class="col-sm-12 text-center">
			<button type="button" class="btn btn-warning no-border" style="width:120px;" onClick="submit();">Зберегти</button>
		</div>
	</div>
	
</div>

</form>

<?php echo '<script'; ?>
 type="text/javascript" language="javascript">

initTypeahead();
changePayment('<?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['payment_type'];?>
');
loadUserComment();

<?php echo '</script'; ?>
>
<?php }
}
