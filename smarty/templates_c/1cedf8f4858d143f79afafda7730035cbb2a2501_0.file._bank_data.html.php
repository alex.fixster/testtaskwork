<?php
/* Smarty version 3.1.39, created on 2022-01-26 22:55:32
  from '/Users/alex/www/coffee-store/templates/admin/order/_bank_data.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61f1b5443234b0_66537673',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1cedf8f4858d143f79afafda7730035cbb2a2501' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/order/_bank_data.html',
      1 => 1643230529,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61f1b5443234b0_66537673 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/Users/alex/www/coffee-store/smarty/plugins/modifier.spec_char.php','function'=>'smarty_modifier_spec_char',),));
?>
<div class="modal fade" id="modalBankData" tabindex="-1" style="z-index:5000;" role="dialog" aria-labelledby="modalBankData" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">
                	Банківські дані підприємства
                </h4>
			</div>
			
			<div class="modal-body" style="height:280px;">
				
				<div>Назва підприємства</div>
				<div><input type="text" class="form-control" id="bankDataHeader" name="bank_pay_header" placeholder="Введіть назву підприємства" value="<?php echo smarty_modifier_spec_char($_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['bank_pay_header']);?>
"></div>
				
				<div style="padding-top:10px;">Юр. адреса</div>
				<div><input type="text" class="form-control" id="bankDataAddress" name="bank_pay_address" placeholder="Введіть юр. адресу" value="<?php echo smarty_modifier_spec_char($_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['bank_pay_address']);?>
"></div>
				
				<div style="padding-top:10px;">Розрахунковий рахунок</div>
				<div><textarea class="form-control" id="bankDataAccount" name="bank_pay_account" rows="5" placeholder="Введіть дані по р/р"><?php echo $_smarty_tpl->tpl_vars['orderInfo']->value['delivery']['bank_pay_account'];?>
</textarea></div>
			
			</div>
			
			<div class="modal-footer">
				<div style="float:right;">
					<button type="button" id="btnSendBankData" class="btn btn-success" onClick="saveBankData()">Зберегти</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal">Закрити</button>
				</div>
				
			</div>
			
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php }
}
