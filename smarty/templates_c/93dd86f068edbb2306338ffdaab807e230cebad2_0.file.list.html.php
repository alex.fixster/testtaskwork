<?php
/* Smarty version 3.1.39, created on 2022-01-17 17:54:20
  from '/Users/alex/www/coffee-store/templates/admin/essense/box/list.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e5912cc0d218_46076578',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '93dd86f068edbb2306338ffdaab807e230cebad2' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/essense/box/list.html',
      1 => 1641320457,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61e5912cc0d218_46076578 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="page-header">
<h1>Список коробок
<span style="float:right;"><a href="/admin/essense/box_add/" class="btn btn-success btn-sm no-border"><span class="fa fa-dropbox"></span>&nbsp;&nbsp;&nbsp;Добавити коробку</a></span>
</h1>
</div>


<div class="col-xs-8">
	
	<div class="table-responsive">
		<table class="table table-striped table-hover dataTable">
			<thead>
				<tr>
					<th>Заголовок</th>
                    <th width="130" class="text-center">Розмір</th>
                    <th width="80" class="text-center">Об'єм</th>
                    <th width="60" class="text-center">Ціна</th>
                    <th class="text-center" width="100">В наявності</th>
					<th class="text-center" width="70">Видима</th>
					<th width="130" class="text-center">Дії</th>
				</tr>
			</thead>
			<tbody>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['boxList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
				<tr <?php if ($_smarty_tpl->tpl_vars['local']->value['is_show'] == 'N') {?>class="text-muted"<?php }?>>
					<td><?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
</td>
					<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['local']->value['length'];?>
 x <?php echo $_smarty_tpl->tpl_vars['local']->value['width'];?>
 x <?php echo $_smarty_tpl->tpl_vars['local']->value['height'];?>
</td>
					<td class="text-center"><div class="label"><?php echo $_smarty_tpl->tpl_vars['local']->value['size_volume'];?>
</div></td>
                    <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['local']->value['price'];?>
</td>
					<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['local']->value['in_stock'];?>
</td>
                    <td class="text-center"><?php if ($_smarty_tpl->tpl_vars['local']->value['is_show'] == 'Y') {?><i class="fa fa-check text-success"></i><?php } else { ?><i class="fa fa-close text-danger"></i><?php }?></td>
					<td class="text-center">
						
						<div class="action-buttons">
							<a href="/admin/essense/box_edit/&id=<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
" class="green"><span class="glyphicon glyphicon-edit"></span> Ред.</a>
                            <a href="javascript: confirmdel('/admin/essense/box_delete/&id=<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
')" class="red"><span class="glyphicon glyphicon-trash"></span> Вид.</a>
						</div>
						
					</td>
				</tr>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</tbody>
		</table>
	</div>
	
</div><?php }
}
