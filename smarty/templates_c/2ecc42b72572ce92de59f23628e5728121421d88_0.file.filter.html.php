<?php
/* Smarty version 3.1.39, created on 2022-01-16 19:38:02
  from '/Users/alex/www/coffee-store/templates/admin/client/filter.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e457faa4b4c0_97250621',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2ecc42b72572ce92de59f23628e5728121421d88' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/client/filter.html',
      1 => 1641146388,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61e457faa4b4c0_97250621 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript" src="/library/typeahead/0.10.5/typeahead.bundle.js"><?php echo '</script'; ?>
>
<link href="/library/typeahead/0.10.5/typeahead.css" type="text/css" rel="stylesheet">

<form class="form-horizontal" role="form" action="/admin/client/save_filter/" method="post">
	<div class="row">
		<div class="col-xs-4">
			
			<div class="form-group">
				<label for="inputClient" class="col-sm-3 control-label normal">Ім'я</label>
				<div class="col-sm-9">
					<input class="form-control" name="client" id="inputClient" value="<?php echo $_smarty_tpl->tpl_vars['searchInfo']->value['client'];?>
" data-placeholder="Введіть ім'я клієнта">
				</div>
			</div>
            
         </div>
         <div class="col-xs-5">
            
            <div class="form-group">
				<label for="inputMail" class="col-sm-3 control-label normal">E-mail</label>
				<div class="col-sm-9">
					<input class="form-control" name="mail" id="inputMail" value="<?php echo $_smarty_tpl->tpl_vars['searchInfo']->value['mail'];?>
" data-placeholder="Введіть e-mail клієнта">
				</div>
			</div>
            
         </div>
         <div class="col-xs-3">
            
            <div class="form-group">
				<label for="inputPhone" class="col-sm-4 control-label normal">Телефон</label>
				<div class="col-sm-8">
					<input class="form-control" name="phone" id="inputPhone" value="<?php echo $_smarty_tpl->tpl_vars['searchInfo']->value['phone'];?>
" data-placeholder="Введіть телефон клієнта">
				</div>
			</div>
			
		</div>
		
		
		<div class="col-xs-12 text-center">
			<button type="submit" name="search" value="Search" class="btn btn-warning btn-sm no-border btn-mobile" style="width:100px;">
                Пошук
            </button> 
            <button type="button" name="clear" value="Clear" class="btn btn-default btn-sm no-border btn-mobile" onclick="clearFilter()" style="width:100px;">
                Очистити
            </button>
		</div>
	</div>
</form>

<hr>

<?php echo '<script'; ?>
 type="text/javascript" language="javascript">
initTypeahead();
<?php echo '</script'; ?>
><?php }
}
