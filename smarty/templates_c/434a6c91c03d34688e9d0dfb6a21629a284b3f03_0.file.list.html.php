<?php
/* Smarty version 3.1.39, created on 2022-01-20 22:09:03
  from '/Users/alex/www/coffee-store/templates/admin/coupon/list.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e9c15f2d6d16_95905939',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '434a6c91c03d34688e9d0dfb6a21629a284b3f03' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/coupon/list.html',
      1 => 1641298305,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:admin/coupon/_sms_coupon_popup.html' => 1,
    'file:admin/coupon/_coupon_percent_details_popup.html' => 1,
  ),
),false)) {
function content_61e9c15f2d6d16_95905939 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript" src="/library/bootstrap/3.1.1/js/popover.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" language="javascript" src="/admin/js/coupon_list.js"><?php echo '</script'; ?>
>

<?php $_smarty_tpl->_subTemplateRender("file:admin/coupon/_sms_coupon_popup.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender("file:admin/coupon/_coupon_percent_details_popup.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div class="page-header">
<h1>Список купонів знижок
<span style="float:right;"><a href="/admin/coupon/add/" class="btn btn-success btn-sm no-border"><span class="fa fa-percent"></span>&nbsp;&nbsp;&nbsp;Добавити купон знижки</a></span>
</h1>
</div>


<div class="col-xs-12">
	
	<div class="table-responsive">
		<table class="table table-striped table-hover dataTable">
			<thead>
				<tr>
					<th>Назва</th>
					<th width="150">Код</th>
					<th width="50" class="text-center">%</th>
					<th width="300">Період дії</th>
					<th>Персоналізація</th>
					<th class="text-enter" width="100">Замовлень</th>
					<th width="200" class="text-center">Дії</th>
				</tr>
			</thead>
			<tbody>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['couponList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
				<tr>
					<td><?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
</td>
					<td><?php echo $_smarty_tpl->tpl_vars['local']->value['coupon_code'];?>
</td>
					<td class="text-center"><a href="javascript: showPopupDiscountDetails(<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
)"><i class="fa fa-percent bigger-120"></i></a></td>
					<td><?php echo $_smarty_tpl->tpl_vars['local']->value['date_start_str'];?>
 р. - <?php if ($_smarty_tpl->tpl_vars['local']->value['date_end']) {
echo $_smarty_tpl->tpl_vars['local']->value['date_end_str'];?>
 р.<?php } else { ?><div class="label label-default">Безстрокова</div><?php }?></td>
					<td><a href="/admin/client/edit/&id=<?php echo $_smarty_tpl->tpl_vars['local']->value['user_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['local']->value['user_name'];?>
</a></td>
                	<td class="text-center"><?php if ($_smarty_tpl->tpl_vars['local']->value['order_qty']) {?><a href="/admin/order/list/?coupon_code=<?php echo $_smarty_tpl->tpl_vars['local']->value['coupon_code'];?>
"><span class="label label-warning"><?php echo $_smarty_tpl->tpl_vars['local']->value['order_qty'];?>
</span></a><?php }?></td>
				    <td class="text-center">
						
						<div class="action-buttons">
							<?php if ($_smarty_tpl->tpl_vars['local']->value['user_name']) {?>
							<a href="javascript: showPopupSendCoupon(<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
)" class="info" style="margin-right:15px;"><span class="fa fa-comment-o"></span> SMS</a>
							<?php } else { ?>
							<span style="margin-left:64px;"></span>
							<?php }?>
							<a href="/admin/coupon/edit/&id=<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
" class="green" style="margin-right:15px;"><span class="glyphicon glyphicon-edit"></span> Ред.</a>
							<?php if (!$_smarty_tpl->tpl_vars['local']->value['order_qty']) {?>
                            <a href="javascript: confirmdel('/admin/coupon/delete/&id=<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
')" class="red"><span class="glyphicon glyphicon-trash"></span> Вид.</a>
							<?php } else { ?>
							<span class="text-muted"><span class="glyphicon glyphicon-trash"></span> Вид.</span>
							<?php }?>
						</div>
						
					</td>
				</tr>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</tbody>
		</table>
	</div>
    
    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['pagination_admin'][0], array( array('count'=>$_smarty_tpl->tpl_vars['per_page']->value,'total_count'=>$_smarty_tpl->tpl_vars['total_count']->value,'link'=>$_smarty_tpl->tpl_vars['link']->value,'current_page'=>$_smarty_tpl->tpl_vars['current_page']->value,'max_pages'=>6),$_smarty_tpl ) );?>

	
</div>


<?php }
}
