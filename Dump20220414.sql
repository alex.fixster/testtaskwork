-- MySQL dump 10.13  Distrib 8.0.26, for macos11 (x86_64)
--
-- Host: 127.0.0.1    Database: testtask
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `header` varchar(255) NOT NULL,
  `product_qty` smallint unsigned NOT NULL DEFAULT '0',
  `subcategory_qty` smallint unsigned NOT NULL DEFAULT '0',
  `parent_id` smallint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `parent_idx` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Home & Kitchen',2,4,0),(2,'Electronics',1,4,0),(3,'Health, Household & Baby Care',3,0,0),(4,'Clothing, Shoes & Jewelry',0,0,0),(5,'Industrial & Scientific',2,0,0),(7,'Tools & Home Improvement',1,0,0),(8,'Beauty & Personal Care',2,0,0),(9,'Kids Home Store',1,0,1),(10,'Kitchen & Dining',1,0,1),(11,'Bedding',0,0,1),(12,'Bath',0,2,1),(13,'Bathroom Accessories',1,0,12),(14,'Towels',1,0,12),(15,'Accessories & Supplies',0,4,2),(16,'Camera & Photo',0,0,2),(17,'Car & Vehicle Electronics',1,0,2),(18,'Cell Phones & Accessories',0,0,2),(19,'Audio & Video Accessories',0,0,15),(20,'Camera & Photo Accessories',0,0,15),(21,'Cell Phone Accessories',0,0,15),(22,'Home Audio Accessories',0,0,15),(30,'Test',1,2,0),(31,'Test Subcategory 0.1',0,0,30),(33,'Test Subcategory 0.3',0,0,30);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `message` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (1,'Please enter category title'),(2,'Category successfully updated'),(3,'Category successfully added'),(4,'Category successfully deleted'),(5,'You have specified a non-existent category'),(6,'Category does not exist'),(7,'Product successfully updated'),(8,'Product successfully added'),(9,'Product successfully deleted'),(10,'You have specified a non-existent product'),(11,'Please enter product title'),(12,'Please enter product price'),(13,'Please select category(s)'),(14,'One or more categories no longer exist');
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `header` varchar(255) NOT NULL,
  `price` double(10,2) unsigned NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `price` (`price`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'Apple Pencil (2nd Generation)',35.89),(2,'Sceptre 24\" Professional Thin 75Hz 1080p LED Monitor 2x HDMI VGA Build-in Speakers, Machine Black (E248W-19203R Series)',489.90),(3,'HP DeskJet 4155e All-in-One Wireless Color Printer, with bonus 6 months free Instant Ink with HP+ (26Q90A)',278.05),(4,'CeraVe AM Facial Moisturizing Lotion SPF 30 | Oil-Free Face Moisturizer with Sunscreen | Non-Comedogenic | 3 Ounce',18.90),(5,'Nizoral Anti-Dandruff Shampoo, Basic, Fresh, 7 Fl Oz',64.99),(6,'Garmin vivosmart 4, Activity and Fitness Tracker w/Pulse Ox and Heart Rate Monitor',175.89),(7,'Withings Steel HR Hybrid Smartwatch - Activity, Sleep, Fitness and Heart Rate Tracker with Connected GPS',356.00);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_category`
--

DROP TABLE IF EXISTS `product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_category` (
  `product_id` int unsigned NOT NULL,
  `category_id` smallint unsigned NOT NULL,
  KEY `to_product_idx` (`product_id`),
  KEY `to_category_idx` (`category_id`),
  CONSTRAINT `to_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE,
  CONSTRAINT `to_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_category`
--

LOCK TABLES `product_category` WRITE;
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
INSERT INTO `product_category` VALUES (5,1),(5,14),(5,3),(5,5),(1,1),(3,9),(3,13),(3,8),(4,2),(4,8),(2,10),(6,3),(6,5),(6,7),(7,17),(7,3),(7,30);
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-14  1:21:13
