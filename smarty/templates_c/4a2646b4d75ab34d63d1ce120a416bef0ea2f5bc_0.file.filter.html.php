<?php
/* Smarty version 3.1.39, created on 2022-01-13 21:16:38
  from '/Users/alex/www/coffee-store/templates/admin/order/filter.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e07a96540aa5_53703991',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4a2646b4d75ab34d63d1ce120a416bef0ea2f5bc' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/order/filter.html',
      1 => 1510007048,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61e07a96540aa5_53703991 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript" src="/library/chosen/1.1.0/chosen.jquery.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" src="/library/datepicker/js/moment.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/library/datepicker/js/daterangepicker.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/library/datepicker/js/initialization.js" charset="UTF-8"><?php echo '</script'; ?>
>
<link href="/library/datepicker/css/daterangepicker.css" type="text/css" rel="stylesheet">


<form class="form-horizontal" role="form" action="/admin/order/save_filter/" method="post">
	<div class="row">
    	<div class="col-xs-2">
			
			<div class="form-group">
				<label for="inputOrderNum" class="col-xs-12"># замовлення</label>
				<div class="col-xs-12">
					<input type="text" class="form-control" name="order_num" id="inputOrderNum" value="<?php echo $_smarty_tpl->tpl_vars['searchInfo']->value['order_num'];?>
" placeholder="Введіть # замовлення">
				</div>
			</div>
            
        </div>
        
        <div class="col-xs-3">
			
			<div class="form-group">
				<label for="inputClient" class="col-xs-12">Клієнт (e-mail, ім'я, телефон)</label>
				<div class="col-xs-12">
					<input type="text" class="form-control" name="client" id="inputClient" value="<?php echo $_smarty_tpl->tpl_vars['searchInfo']->value['client'];?>
" placeholder="Введіть ключові слова">
				</div>
			</div>
            
        </div>
        <div class="col-xs-4">
        
        	<div class="form-group">
				<label for="inputStatus" class="col-xs-12">Статус</label>
				<div class="col-xs-12">
					<select class="form-control chosen-select" size="1" multiple name="status_id[]" id="inputStatus" data-placeholder="Виберіть статус">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['statusList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['searchInfo']->value['status_id'][$_smarty_tpl->tpl_vars['local']->value['id']]) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
</option>
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </select>
				</div>
			</div>
            
        </div>
		<div class="col-xs-3">
			
			 <div class="form-group">
				<label for="inputDatePeriod" class="col-xs-12">Період</label>
				<div class="col-xs-12">
                	<div class="input-group">
						<input name="period" id="inputDatePeriod" class="form-control" type="text" placeholder="Вкажіть період" value="<?php echo $_smarty_tpl->tpl_vars['searchInfo']->value['period'];?>
">
						 <span class="input-group-addon" style="cursor:pointer;"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>
			</div>
            
         </div>
         
         
		
		<div class="col-xs-12 text-center">
			<button type="submit" name="search" value="Search" class="btn btn-warning btn-sm no-border btn-mobile" style="width:100px;">
                Пошук
            </button> 
            <button type="button" name="clear" value="Clear" class="btn btn-default btn-sm no-border btn-mobile" onclick="clearFilter()" style="width:100px;">
                Очистити
            </button>
		</div>
	</div>
</form>

<hr>

<?php echo '<script'; ?>
 type="text/javascript" language="javascript">
initTypeahead();

daterangepicker_initialize({
	'inputDatePeriod' : 'double'
});
<?php echo '</script'; ?>
><?php }
}
