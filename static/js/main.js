function confirmdel(url, message) {
	if (message == null) message = 'Are you sure you want to delete this entry?';
	
	if (confirm(message)) {
		location.href = url;
	}
}


function showError(jMessage) {
	
	var objMessage = jQuery.parseJSON(jMessage);
	
	var messBody = "<ul>";
	for (i in objMessage) {
		messBody += "<li>"+objMessage[i]+"</li>";
	}
	messBody += "</ul>";
	
	$("#messageBody").html(messBody);
	$('#modalClientMessage').modal({show:true});
	//$('#modalClientMessage').on('hidden.bs.modal', function() {unblockLogin()});
}
