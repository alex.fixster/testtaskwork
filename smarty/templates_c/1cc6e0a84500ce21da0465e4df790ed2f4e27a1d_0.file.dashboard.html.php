<?php
/* Smarty version 3.1.39, created on 2022-01-13 10:53:28
  from '/Users/alex/www/coffee-store/templates/admin/dashboard/dashboard.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61dfe888b84920_65470160',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1cc6e0a84500ce21da0465e4df790ed2f4e27a1d' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/dashboard/dashboard.html',
      1 => 1482097366,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61dfe888b84920_65470160 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript" src="https://www.google.com/jsapi"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawVisualization);
	
	function drawVisualization() {
		// Some raw data (not necessarily accurate)
		
		var dataProfit = google.visualization.arrayToDataTable([
			['Місяць', <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['brandList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>'<?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
',<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?> 'Загальний'],
			
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['dashbaordList']->value['profit'], 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
			['<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
', <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['brandList']->value, 'local_b', false, 'key_b');
$_smarty_tpl->tpl_vars['local_b']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key_b']->value => $_smarty_tpl->tpl_vars['local_b']->value) {
$_smarty_tpl->tpl_vars['local_b']->do_else = false;
echo (($tmp = @$_smarty_tpl->tpl_vars['local']->value['brand'][$_smarty_tpl->tpl_vars['local_b']->value['id']])===null||$tmp==='' ? 0 : $tmp);?>
,<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?> <?php echo $_smarty_tpl->tpl_vars['local']->value['total'];?>
],
			<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			
		]);
		
		var optionsProfit = {
			title : 'Прибуток (без затрат на рекламу)',
			vAxis: {minValue:0},
			hAxis: {},
			seriesType: 'bars',
			series: {3: {type: 'line'}},
			chartArea:{height:200, top:50},
			colors: ['#3c4dc7', '#cd3a2b', '#000000', '#299114']
		};
		
		var chartProfit = new google.visualization.ColumnChart(document.getElementById('profitChart'));
		chartProfit.draw(dataProfit, optionsProfit);
		
		
		
		var dataTurnover = google.visualization.arrayToDataTable([
          	['Місяць', <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['brandList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>'<?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
',<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?> 'Загальний'],
          	
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['dashbaordList']->value['turnover'], 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
			['<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
', <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['brandList']->value, 'local_b', false, 'key_b');
$_smarty_tpl->tpl_vars['local_b']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key_b']->value => $_smarty_tpl->tpl_vars['local_b']->value) {
$_smarty_tpl->tpl_vars['local_b']->do_else = false;
echo (($tmp = @$_smarty_tpl->tpl_vars['local']->value['brand'][$_smarty_tpl->tpl_vars['local_b']->value['id']])===null||$tmp==='' ? 0 : $tmp);?>
,<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?> <?php echo $_smarty_tpl->tpl_vars['local']->value['total'];?>
],
			<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			
        ]);

        var optionsTurnover = {
            title: 'Обороти',
			chartArea:{height:300, top:50},
			seriesType: 'bars',
			series: {3: {type: 'line'}},
			colors: ['#3c4dc7', '#cd3a2b', '#000000', '#299114']
        };

        var chartTurnover = new google.visualization.ColumnChart(document.getElementById('turnoverChart'));
        chartTurnover.draw(dataTurnover, optionsTurnover);

		
 	}
<?php echo '</script'; ?>
>


<div class="page-header">
	<h1>
		Дашборд
	</h1>
</div>

<div style="height:10px;"></div>

<div id="profitChart" style="height:290px; border:solid 1px #CCC;"></div>

<div id="turnoverChart" style="height:390px; margin-top:20px; border:solid 1px #CCC;"></div><?php }
}
