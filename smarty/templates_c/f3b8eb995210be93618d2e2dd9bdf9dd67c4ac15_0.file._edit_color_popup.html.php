<?php
/* Smarty version 3.1.39, created on 2022-01-13 20:57:41
  from '/Users/alex/www/coffee-store/templates/admin/products/_edit_color_popup.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e0762573e558_59798834',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f3b8eb995210be93618d2e2dd9bdf9dd67c4ac15' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/products/_edit_color_popup.html',
      1 => 1494251506,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61e0762573e558_59798834 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="modal fade" id="modalProductColorTable" tabindex="-1" style="z-index:5000;" role="dialog" aria-labelledby="modalProductColorTable" aria-hidden="true">
	<div class="modal-dialog ">
		<div class="modal-content">
		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">
                	Виберіть колір продукту
                </h4>
			</div>
			
			<div class="modal-body" style="height:300px;">
				
            	<table class="product-color-table">
					<tr>
						<td class="color-table-0-0"></td>
						<td class="color-table-1-0"></td>
						<td class="color-table-2-0"></td>
						<td class="color-table-3-0"></td>
						<td class="color-table-4-0"></td>
					</tr>
					<tr>
						<td class="color-table-0-1"></td>
						<td class="color-table-1-1"></td>
						<td class="color-table-2-1"></td>
						<td class="color-table-3-1"></td>
						<td class="color-table-4-1"></td>
					</tr>
					<tr>
						<td class="color-table-0-2"></td>
						<td class="color-table-1-2"></td>
						<td class="color-table-2-2"></td>
						<td class="color-table-3-2"></td>
						<td class="color-table-4-2"></td>
					</tr>
					<tr>
						<td class="color-table-0-3"></td>
						<td class="color-table-1-3"></td>
						<td class="color-table-2-3"></td>
						<td class="color-table-3-3"></td>
						<td class="color-table-4-3"></td>
					</tr>
					<tr>
						<td class="color-table-0-4"></td>
						<td class="color-table-1-4"></td>
						<td class="color-table-2-4"></td>
						<td class="color-table-3-4"></td>
						<td class="color-table-4-4"></td>
					</tr>
				</table>
                
			</div>
			
			<div class="modal-footer">
				<input type="hidden" id="popup-color-num" value="">
				<input type="hidden" id="popup-color-selected" value="">
            	<button type="button" class="btn btn-success" onClick="selectColorProcess()">Вибрати</button>
				<button type="button" class="btn btn-primary" data-dismiss="modal">Закрити</button>
			</div>
			
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php }
}
