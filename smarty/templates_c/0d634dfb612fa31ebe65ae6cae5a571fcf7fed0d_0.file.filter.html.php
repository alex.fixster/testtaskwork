<?php
/* Smarty version 3.1.39, created on 2022-01-14 14:34:09
  from '/Users/alex/www/coffee-store/templates/admin/products/filter.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e16dc12b3766_64202501',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0d634dfb612fa31ebe65ae6cae5a571fcf7fed0d' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/products/filter.html',
      1 => 1642163648,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61e16dc12b3766_64202501 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript" src="/library/chosen/1.1.0/chosen.jquery.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" src="/library/typeahead/0.10.5/typeahead.bundle.js"><?php echo '</script'; ?>
>
<link href="/library/typeahead/0.10.5/typeahead.css" type="text/css" rel="stylesheet">

<form class="form-horizontal" role="form" action="/admin/products/save_filter/" method="post">
	<div class="row">
    	<div class="col-xs-4">
			
			<div class="form-group">
				<label for="inputBrand" class="col-xs-12">Бренди</label>
				<div class="col-xs-12">
					<select class="form-control chosen-select" multiple name="brand_id[]" id="inputFilterBrand" data-placeholder="Введіть бренд">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['brandList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['searchInfo']->value['brand_id'][$_smarty_tpl->tpl_vars['local']->value['id']]) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
</option>
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </select>
				</div>
			</div>
            
      </div>
        
      <div class="col-xs-4">
			
			<div class="form-group">
				<label for="inputCategory" class="col-xs-12">Категорія</label>
				<div class="col-xs-12">
					<select class="form-control chosen-select" multiple name="category_id[]" id="inputFilterCategory" data-placeholder="Введіть категорію">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categoryList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
					<optgroup label="<?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
">
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['local']->value['items'], 'local_i', false, 'key_i');
$_smarty_tpl->tpl_vars['local_i']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key_i']->value => $_smarty_tpl->tpl_vars['local_i']->value) {
$_smarty_tpl->tpl_vars['local_i']->do_else = false;
?>
                    	<option value="<?php echo $_smarty_tpl->tpl_vars['local_i']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['searchInfo']->value['category_id'][$_smarty_tpl->tpl_vars['local_i']->value['id']]) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['local_i']->value['header'];?>
</option>
						<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					</optgroup>
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </select>
				</div>
			</div>
            
      </div>
   </div>
   <div class="row">
		
		
		<div class="col-xs-4">
			
			<div class="form-group">
				<label for="inputHeader" class="col-xs-12">Назва</label>
				<div class="col-xs-12">
					<input type="text" class="form-control" name="header" id="inputFilterHeader" value="<?php echo $_smarty_tpl->tpl_vars['searchInfo']->value['header'];?>
" placeholder="Введіть назву товару">
				</div>
			</div>
            
         </div>
         <div class="col-xs-2">
            
            <div class="form-group">
				<label for="inputKeywords" class="col-xs-12">Опис, артикул</label>
				<div class="col-xs-12">
					<input type="text" class="form-control" name="keywords" id="inputFilterKeywords" value="<?php echo $_smarty_tpl->tpl_vars['searchInfo']->value['keywords'];?>
" placeholder="Введіть ключові слова">
				</div>
			</div>
            
         </div>
		 
		
		
		<div class="col-xs-4" style="height:60px;">
            
         <div class="form-group">
				<div class="col-xs-12" style="padding-top: 32px; padding-left:40px;">
					<input type="checkbox" name="is_show" id="inputShow" value="Y" <?php if ($_smarty_tpl->tpl_vars['searchInfo']->value['is_show']) {?>checked<?php }?>>
					<label for="inputShow">Показувати невидимі</label>
				</div>
			</div>
            
      </div>
         
		
		<div class="col-xs-8 text-center">
			<button type="submit" name="search" value="Search" class="btn btn-warning btn-sm no-border btn-mobile" style="width:100px;">
                Пошук
            </button> 
            <button type="button" name="clear" value="Clear" class="btn btn-default btn-sm no-border btn-mobile" onclick="clearFilter()" style="width:100px;">
                Очистити
            </button>
		</div>
	</div>
</form>

<hr>

<?php echo '<script'; ?>
 type="text/javascript" language="javascript">
initTypeahead();
<?php echo '</script'; ?>
><?php }
}
