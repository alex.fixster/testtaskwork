<?php
/* Smarty version 3.1.39, created on 2022-01-20 22:09:05
  from '/Users/alex/www/coffee-store/templates/admin/coupon/edit.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e9c161b55030_13105617',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '04c8d4ff9db39f0ef15e382dd52614c2ab2d2c22' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/coupon/edit.html',
      1 => 1498939415,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:admin/coupon/_edit_group_discount.html' => 3,
  ),
),false)) {
function content_61e9c161b55030_13105617 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/Users/alex/www/coffee-store/smarty/plugins/modifier.spec_char.php','function'=>'smarty_modifier_spec_char',),));
echo '<script'; ?>
 type="text/javascript" src="/library/datepicker/js/moment.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/library/datepicker/js/daterangepicker.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/library/datepicker/js/initialization.js" charset="UTF-8"><?php echo '</script'; ?>
>
<link href="/library/datepicker/css/daterangepicker.css" type="text/css" rel="stylesheet">


<?php echo '<script'; ?>
 type="text/javascript" src="/library/typeahead/0.10.5/typeahead.bundle.js"><?php echo '</script'; ?>
>
<link href="/library/typeahead/0.10.5/typeahead.css" type="text/css" rel="stylesheet">
<?php echo '<script'; ?>
 type="text/javascript" src="/library/handlebars/handlebars-v4.0.4.js"><?php echo '</script'; ?>
>


<?php echo '<script'; ?>
 type="text/javascript" language="javascript" src="/admin/js/coupon_edit.js"><?php echo '</script'; ?>
>

<div class="page-header">
	<h1>
		<?php if ($_smarty_tpl->tpl_vars['couponInfo']->value['id']) {?>Редагування<?php } else { ?>Створення<?php }?> купона знижки
        <span style="float:right;"><a href="/admin/coupon/add/" class="btn btn-success btn-sm no-border"><span class="fa fa-percent"></span>&nbsp;&nbsp;&nbsp;Створити купон знижки</a></span>
    </h1>
</div>

<div style="height:10px;"></div>

<form class="form-horizontal" role="form" action="/admin/coupon/update/" method="post">
<input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['couponInfo']->value['id'];?>
">

<div class="row">
<div class="col-xs-6">

<div class="row">
<div class="col-xs-12">

	<div class="form-group">
		<label for="inputHeader" class="col-xs-4 normal">Назва*</label>
		<div class="col-xs-8">
			<input type="text" id="inputHeader" name="header" class="form-control" placeholder="Введіть назву знижки" value="<?php echo smarty_modifier_spec_char($_smarty_tpl->tpl_vars['couponInfo']->value['header']);?>
">
		</div>
	</div>
	
	<div class="form-group">
		<label for="inputCode" class="col-xs-4 normal">Код*</label>
		<div class="col-xs-8">
			<input type="text" id="inputCode" name="coupon_code" class="form-control" placeholder="Введіть код знижки" value="<?php echo $_smarty_tpl->tpl_vars['couponInfo']->value['coupon_code'];?>
">
		</div>
	</div>
	
	<div class="form-group">
		<label for="inputDiscount" class="col-xs-4 normal">Знижка*</label>
		<div class="col-xs-8">
		
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['couponInfo']->value['discount_list'], 'local_d', false, 'key_d');
$_smarty_tpl->tpl_vars['local_d']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key_d']->value => $_smarty_tpl->tpl_vars['local_d']->value) {
$_smarty_tpl->tpl_vars['local_d']->do_else = false;
?>
				<?php $_smarty_tpl->_subTemplateRender("file:admin/coupon/_edit_group_discount.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
			<?php
}
if ($_smarty_tpl->tpl_vars['local_d']->do_else) {
?>
				<?php $_smarty_tpl->_subTemplateRender("file:admin/coupon/_edit_group_discount.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
				<?php $_smarty_tpl->_subTemplateRender("file:admin/coupon/_edit_group_discount.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
			<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		
			<div class="row" style="padding-top:2px;" id="inputDiscountBtn">
				<div class="col-xs-12">
					<a href="javascript: addGroupDiscount();" class="btn btn-minier btn-success"><i class="fa fa-plus"></i> Добавити групу продуктів</a>
				</div>
			</div>
		
		</div>
		
	</div>
    
   <div class="form-group">
    	<label for="inputContent" class="col-xs-4 normal">Нотатки</label>
		<div class="col-xs-8">
			<textarea name="content" id="inputContent" class="form-control" rows="6" placeholder="Введіть нотатки"><?php echo $_smarty_tpl->tpl_vars['couponInfo']->value['content'];?>
</textarea>
		</div>
	</div>
	
	<div class="form-group">
		<label for="inputDateStart" class="col-xs-4 normal">Дата початку*</label>
		<div class="col-xs-5">
			<div class="input-group">
				<input name="date_start" id="inputDateStart" class="form-control" type="text" placeholder="Вкажіть дату" value="<?php echo $_smarty_tpl->tpl_vars['couponInfo']->value['date_start'];?>
">
				<span class="input-group-addon" style="cursor:pointer;"><i class="glyphicon glyphicon-calendar"></i></span>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<label for="inputDateEnd" class="col-xs-4 normal">Дата завершення</label>
		<div class="col-xs-5">
			<div class="input-group">
				<input name="date_end" id="inputDateEnd" class="form-control" type="text" placeholder="Вкажіть дату" value="<?php echo $_smarty_tpl->tpl_vars['couponInfo']->value['date_end'];?>
">
				<span class="input-group-addon" style="cursor:pointer;"><i class="glyphicon glyphicon-calendar"></i></span>
			</div>
		</div>
	</div>
    
    <div class="form-group">
		<label class="col-xs-4 normal">Персональна*</label>
		<div class="col-xs-8">
			<input id="inputPersonalY" name="personal" type="radio" value="Y" <?php if ($_smarty_tpl->tpl_vars['couponInfo']->value['personal'] == 'Y') {?>checked<?php }?> onClick="showHideClientFld()"> <label for="inputPersonalY">Так</label> &nbsp;&nbsp;&nbsp;
            <input id="inputPersonalN" name="personal" type="radio" value="N" <?php if ($_smarty_tpl->tpl_vars['couponInfo']->value['personal'] == 'N') {?>checked<?php }?> onClick="showHideClientFld()"> <label for="inputPersonalN">Ні</label>
        </div>
	</div>
	
	<div class="form-group" id="formClient">
		<label for="inputClientName" class="col-xs-4 normal">Ім'я клієнта*</label>
		<div class="col-xs-8">
			<input type="hidden" id="inputClientID" name="user_id" value="<?php echo $_smarty_tpl->tpl_vars['couponInfo']->value['user_id'];?>
">
			<input type="text" id="inputClientName" name="client_name" class="form-control" placeholder="Імя, телефон, e-mail" value="<?php echo $_smarty_tpl->tpl_vars['couponInfo']->value['client_name'];?>
">
		</div>
	</div>

</div>

<div class="col-xs-12">
	
	<div class="form-group">
		<div class="col-sm-12 text-center">
			<button type="submit" class="btn btn-warning no-border" style="width:120px;">Зберегти</button>
		</div>
	</div>
	

</div>

</div>
</form>


<?php echo '<script'; ?>
 type="text/javascript" language="javascript">
daterangepicker_initialize({
	'inputDateStart' : 'single',
	'inputDateEnd' : 'single'
});

initTypeahead();
showHideClientFld();
<?php echo '</script'; ?>
><?php }
}
