<?php
/* Smarty version 3.1.39, created on 2022-01-13 21:16:38
  from '/Users/alex/www/coffee-store/templates/admin/order/list.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e07a96538c46_37396620',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c340c37945f81a2924c52d61601f053b6f3cb949' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/order/list.html',
      1 => 1511082550,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:admin/order/filter.html' => 1,
  ),
),false)) {
function content_61e07a96538c46_37396620 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript" language="javascript" src="/admin/js/order_list.js"><?php echo '</script'; ?>
>

<div class="page-header">
<h1>Список замовлень
<span style="float:right;"><a href="/admin/order/add/" class="btn btn-success btn-sm no-border"><span class="fa fa-user"></span>&nbsp;&nbsp;&nbsp;Добавити замовлення</a></span>
</h1>
</div>

<div id="orderFilter">
	<?php $_smarty_tpl->_subTemplateRender("file:admin/order/filter.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
	<div style="height:10px;"></div>
</div>

<div class="col-xs-12">
	
	<div class="table-responsive">
		<table class="table table-hover table-bordered dataTable">
			<thead>
				<tr>
					<th width="60" class="<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['oid']['icon'];?>
" onclick="location.href='<?php echo $_smarty_tpl->tpl_vars['linkSort']->value;?>
&sort=<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['oid']['sort'];?>
'">ID</th>
                    <th width="90" class="<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['onum']['icon'];?>
" onclick="location.href='<?php echo $_smarty_tpl->tpl_vars['linkSort']->value;?>
&sort=<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['onum']['sort'];?>
'">Номер</th>
                    <th width="110" class="<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['sheader']['icon'];?>
" onclick="location.href='<?php echo $_smarty_tpl->tpl_vars['linkSort']->value;?>
&sort=<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['sheader']['sort'];?>
'">Статус</th>
                    <th class="<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['dname']['icon'];?>
" onclick="location.href='<?php echo $_smarty_tpl->tpl_vars['linkSort']->value;?>
&sort=<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['dname']['sort'];?>
'">Клієнт</th>
					<th width="120" class="text-center">Замовлення</th>
                    <th width="145" class="<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['total']['icon'];?>
" onclick="location.href='<?php echo $_smarty_tpl->tpl_vars['linkSort']->value;?>
&sort=<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['total']['sort'];?>
'">Сума</th>
                    <th width="175" class="<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['odate']['icon'];?>
" onclick="location.href='<?php echo $_smarty_tpl->tpl_vars['linkSort']->value;?>
&sort=<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['odate']['sort'];?>
'">Дата</th>
                    <th width="130" class="text-center">Дії</th>
				</tr>
			</thead>
			<tbody>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['orderList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
				<tr class="<?php echo $_smarty_tpl->tpl_vars['local']->value['color'];?>
">
					<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['local']->value['order_num'];?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['local']->value['status'];?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['local']->value['name'];?>
<div class="text-muted" style="font-size:11px;"><?php echo $_smarty_tpl->tpl_vars['local']->value['phone_str'];?>
</div></td>
					<td class="text-center"><?php if ($_smarty_tpl->tpl_vars['local']->value['order_qty'] == 1) {?><span class="badge badge-danger">Новий</span><?php } else { ?><a href="/admin/order/list/?user_id=<?php echo $_smarty_tpl->tpl_vars['local']->value['user_id'];?>
"><span class="badge badge-grey" style="font-size:18px; padding-top: 3px; padding-bottom:5px;"><?php echo $_smarty_tpl->tpl_vars['local']->value['order_qty'];?>
</span></a><?php }?></td>
                    <td><?php echo $_smarty_tpl->tpl_vars['local']->value['amount_str'];
if ($_smarty_tpl->tpl_vars['local']->value['profit']) {?><div class="small"><span class="label"><?php echo $_smarty_tpl->tpl_vars['local']->value['profit'];?>
</span><?php if ($_smarty_tpl->tpl_vars['local']->value['profit_100']) {?> <span class="label label-warning"><?php echo $_smarty_tpl->tpl_vars['local']->value['profit_100'];?>
</span><?php }?></div><?php }?></td>
					<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['local']->value['cdatetime_str'];?>
</td>
                    <td class="text-center">
						
						<div class="action-buttons">
							<a href="/admin/order/edit/&id=<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
" class="green"><span class="glyphicon glyphicon-edit bigger-150"></span></a>
							<a href="/admin/order/edit/&id=<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
" class="blue" style="padding-left:10px;"><span class="fa fa-copy bigger-150"></span></a>
                            <a href="javascript: confirmdel('/admin/order/delete/&id=<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
')" style="padding-left:10px;" class="red"><span class="glyphicon glyphicon-trash bigger-150"></span></a>
						</div>
						
					</td>
				</tr>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</tbody>
		</table>
	</div>
    
    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['pagination_admin'][0], array( array('count'=>$_smarty_tpl->tpl_vars['per_page']->value,'total_count'=>$_smarty_tpl->tpl_vars['total_count']->value,'link'=>$_smarty_tpl->tpl_vars['link']->value,'current_page'=>$_smarty_tpl->tpl_vars['current_page']->value,'max_pages'=>6),$_smarty_tpl ) );?>

	
</div>
<?php }
}
