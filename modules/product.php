<?php


switch ($urlValues[1]) {
	
	default:

		$productList = $product->getAllProduct($rqData['parent_id']);


		$smarty->assign("productList", $productList);

		$include_tpl = "product/list.html";

	break;
	
	case "add":
	case "edit":

		$categoryTreeList = $category->getCategoryTree();

		if ((int)$rqData['id']) $productInfo = $product->getProductInfo($rqData['id']);

		if ($_SESSION['form_data']['product']) $productInfo = $_SESSION['form_data']['product'];

		// dump($productInfo);

		$smarty->assign("productInfo", $productInfo);

		$smarty->assign("categoryTreeList", $categoryTreeList);

		$include_tpl = "product/edit.html";

	break;
	
	case "update":

		if ((int)$rqData['id']) $res = $product->updateProduct($rqData);
		else $res = $product->addProduct($rqData);

		if ($res) {

			if ($rqData['id']) save_message(7);
			else save_message(8);

			header("location: ".BASE_URL."/product/");

		}

		else header("location: ".$_SERVER['HTTP_REFERER']);

		exit;

	break;
	
	case "delete":

		$res = $product->deleteProduct($rqData['id']);

		if ($res) save_message(9);

		header("location: ".$_SERVER['HTTP_REFERER']);

		exit;

	break;
	
}


?>