<?php

/*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type:     function
 * Name:     pagination
 * Purpose:  prints pagination
 * -------------------------------------------------------------
 */


function smarty_function_category_tree ($params) {

	extract($params);

	empty($category_id)	? $category_id = "0" : $category_id = (int)$params['category_id'];
	empty($parent_id)	? $parent_id = "0" : $parent_id = (int)$params['parent_id'];
	empty($category_tree_arr) ? $category_tree_arr = array() : $category_tree_arr = (array)$params['category_tree_arr'];
	empty($level)	? $level = "0" : $level = (int)$params['level'];

	$nbsp_str = "";
	for ($i = 0; $i < $level; $i++) $nbsp_str .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

	// dump($category_tree_arr);

	foreach ($category_tree_arr as $key => $val) {

		if ($val['id'] != $category_id) {

			
			if ($val['id'] == $parent_id) $selected = 'selected';
			else $selected = "";

			$singleOption = '<option value="'.$val['id'].'" '.$selected.'>'.$nbsp_str.$val['header'].'</option>'."\n";
			echo $singleOption;

			if ($val['parent_list']) {

				$locParam['category_id'] = $category_id;
				$locParam['parent_id'] = $parent_id;
				$locParam['category_tree_arr'] = $val['parent_list'];
				$locParam['level'] = $level+1;

				smarty_function_category_tree($locParam);

			}

		}

	}	

}



function smarty_function_product_category_tree ($params) {

	extract($params);

	empty($category_id)	? $category_id = array() : $category_id = (array)$params['category_id'];
	empty($category_tree_arr) ? $category_tree_arr = array() : $category_tree_arr = (array)$params['category_tree_arr'];
	empty($level)	? $level = "0" : $level = (int)$params['level'];

	$nbsp_str = "";
	for ($i = 0; $i < $level; $i++) $nbsp_str .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

	// dump($category_tree_arr);

	foreach ($category_tree_arr as $key => $val) {

		// if ($val['id'] != $category_id) {
			
			if ($category_id[$val['id']]) $selected = 'selected';
			else $selected = "";

			$singleOption = '<option value="'.$val['id'].'" '.$selected.'>'.$nbsp_str.$val['header'].'</option>'."\n";
			echo $singleOption;

			if ($val['parent_list']) {

				$locParam['category_id'] = $category_id;
				$locParam['category_tree_arr'] = $val['parent_list'];
				$locParam['level'] = $level+1;

				smarty_function_product_category_tree($locParam);

			}

		// }

	}	

}




function smarty_function_pagination_admin($params)
{
	extract($params);
	
	empty($start_name)	? $start_name = "start" : $start_name = $params['start_name'];
	empty($max_pages)	? $max_pages = 6 : $max_pages = $params['max_pages'];
	
	$current_page = $params['current_page'];
	$link = $params['link'];

	$start_amp = strpos($link, "&");
	if ($start_amp) {
		$amp_link = substr($link, $start_amp);
		$link = substr($link, 0, $start_amp);
	}

	if (!isset($params['count']) || !isset($params['total_count']) || !isset($params['current_page'])) {
		dump($params);
		return;
	}


	
	if (!empty($count)) {

		if (div_ost($total_count, $count) > 0) $page_number = div_cil($total_count, $count) + 1;
		else $page_number = div_cil($total_count, $count);

		
		$last_page_number = $page_number;
		$end_number = ($page_number-1)*$count;



		// don't show anything if only one page
		if ($total_count > 0) {

			// dump(1);
	
			
			
			if ($current_page <= ceil($max_pages/2) and ($current_page + $max_pages) < $page_number) {
				$page_number = $max_pages;
				$pagination_start = 0;
			}

			if ($current_page <= ceil($max_pages/2) and ($current_page + ceil($max_pages/2)) >= $page_number) {
				$pagination_start = 0;
			}
			elseif($current_page > ceil($max_pages/2) and ($current_page + ceil($max_pages/2)) < $page_number) {
				$page_number = $current_page + floor($max_pages/2);
				$pagination_start = $current_page - ceil($max_pages/2);
			}
			elseif (($current_page + ceil($max_pages/2)) >= $page_number) {
				$pagination_start = $page_number - $max_pages;
				if ($pagination_start < 0) $pagination_start = 0;
			}
			
			if ($amp_link) $amp_link = "?".substr($amp_link, 1);
			
			
			for ($i = $pagination_start; $i < $page_number; $i++) {
				
				if (($i+1) * $count <= $total_count) $last_number = ($i+1) * $count;
				
				else $last_number = $total_count;
				
				if ($current_page == $i+1) {
					$arr[$i] = "<li class='active'><a>".($current_page)."</a></li>";
				} else  {
					$arr[$i] = "<li><a href='".$link.($i+1)."/".$amp_link."'>".($i+1)."</a></li>";
				}
			}
			// dump($arr);
			$str .= join("", $arr);

			
			// dump($amp_link);
			
			if ($current_page > 1 and !$withoutPrevNext) {
				$str = "<li><a href='".$link."1/".$amp_link."'>First</a></li><li><a href='".$link.($current_page-1)."/".$amp_link."'>&laquo;</a></li>".$str;
				
			}
			
			
			
			if ($current_page < $page_number and !$withoutPrevNext) {
				$str = $str."<li><a href='".$link.($current_page + 1)."/".$amp_link."'>&raquo;</a></li><li><a href='".$link.$last_page_number."/".$amp_link."'>Last</a></li>";
			}

			if ($page_number > 1) $str = "<div class='pagination-centered'><ul class='pagination'><li class='disabled'><a>Total: ".$total_count."</a></li></ul> <ul class='pagination'>".$str."</ul></div>";
			else $str = "";

			echo $str;
		}
	}
	
}




function smarty_function_pagination_client($params) {

	global $globalStatText;

	// dump($params);

	extract($params);
	
	empty($start_name)	? $start_name = "start" : $start_name = $params['start_name'];
	empty($max_pages)	? $max_pages = 6 : $max_pages = $params['max_pages'];
	
	$current_page = $params['current_page'];
	$link = $params['link'];

	// dump($link);

	$start_amp = strpos($link, "?");
	if ($start_amp) {
		$amp_link = substr($link, $start_amp);
		$link = substr($link, 0, $start_amp);
	}

	if (!isset($params['count']) || !isset($params['total_count']) || !isset($params['current_page'])) {
		dump($params);
		return;
	}

	
	
	if (!empty($count)) {

		if (div_ost($total_count, $count) > 0) $page_number = div_cil($total_count, $count) + 1;
		else $page_number = div_cil($total_count, $count);

		
		$last_page_number = $page_number;
		$end_number = ($page_number-1)*$count;

		// don't show anything if only one page
		if ($total_count > 0) {
			
			
			if ($current_page <= ceil($max_pages/2) and ($current_page + $max_pages) < $page_number) {
				$page_number = $max_pages;
				$pagination_start = 0;
			}

			if ($current_page <= ceil($max_pages/2) and ($current_page + ceil($max_pages/2)) >= $page_number) {
				$pagination_start = 0;
			}
			elseif($current_page > ceil($max_pages/2) and ($current_page + ceil($max_pages/2)) < $page_number) {
				$page_number = $current_page + floor($max_pages/2);
				$pagination_start = $current_page - ceil($max_pages/2);
			}
			elseif (($current_page + ceil($max_pages/2)) >= $page_number) {
				$pagination_start = $page_number - $max_pages;
				if ($pagination_start < 0) $pagination_start = 0;
			}
			
			
			
			for ($i = $pagination_start; $i < $page_number; $i++) {
				
				if (($i+1) * $count <= $total_count) $last_number = ($i+1) * $count;
				
				else $last_number = $total_count;
				
				if ($current_page == $i+1) {
					$arr[$i] = "<li class='active'>".($current_page)."</li>";
				} else  {
					$arr[$i] = "<li><a href='".$link.($i+1)."/".$amp_link."'>".($i+1)."</a></li>";
				}
			}
			
			$str .= join($arr, "");
			
			if ($current_page > 1 and !$withoutPrevNext) {
				$str = "<li><a href='".$link."1/".$amp_link."'><img src='/images/pagination-left-arrow.png' width='11' height='11' alt='' title=''></a></li>".$str;
				// $str = "<li><a href='".$link.($current_page-1)."/".$amp_link."'></a></li>".$str;
				
			}


			
			if ($current_page < $page_number and !$withoutPrevNext) {
				// $str = $str."<li><a href='".$link.($current_page + 1)."/".$amp_link."'></a></li>";
				$str = $str."<li><a href='".$link.$last_page_number."/".$amp_link."'><img src='/images/pagination-right-arrow.png' width='11' height='11' alt='' title=''></a></li>";
			}
			//  "<div class='pagination-centered'><ul class='pagination'><li class='disabled'><a>Total: ".$total_count."</a></li></ul> <ul class='pagination'>".$str."</ul></div>";
			if ($page_number > 1) $str = '<div class="pagination"><ul>'.$str.'</ul></div>';
			else $str = "";

			echo $str;
		}
	}

}




// цілочисельне ділення
function div_cil($var, $n) {
	return floor($var / $n);
}

// цілочисельне ділення
function div_ost($var, $n) {
	return ($var - (floor($var / $n) *$n));
}



?>