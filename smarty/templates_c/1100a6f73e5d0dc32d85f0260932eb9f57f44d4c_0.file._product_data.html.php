<?php
/* Smarty version 3.1.39, created on 2022-01-13 11:25:28
  from '/Users/alex/www/coffee-store/templates/admin/income/_product_data.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61dff008ec1978_48335740',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1100a6f73e5d0dc32d85f0260932eb9f57f44d4c' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/income/_product_data.html',
      1 => 1510772648,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61dff008ec1978_48335740 (Smarty_Internal_Template $_smarty_tpl) {
?>	<?php if ($_smarty_tpl->tpl_vars['productInfo']->value) {?>
    <div class="row">
    	<div class="col-xs-5"><img src="/upl_files/product/size.200/<?php echo $_smarty_tpl->tpl_vars['productInfo']->value['image'];?>
" width="200" height="200" class="img-thumbnail"></div>
    	<div class="col-xs-7">
        	<div style="font-size:14px;"><em><?php echo $_smarty_tpl->tpl_vars['productInfo']->value['brand'];?>
</em></div>
            <div style="padding-bottom:10px; font-size:16px;"><strong><?php echo $_smarty_tpl->tpl_vars['productInfo']->value['header'];?>
</strong></div>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['productInfo']->value['category_list'], 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
            <div>&bull; <?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
</div>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            <div style="padding-top:10px; padding-bottom:10px;">Вага: <?php echo $_smarty_tpl->tpl_vars['productInfo']->value['weight'];?>
</div>
            <div>Упаковка: <?php echo $_smarty_tpl->tpl_vars['productInfo']->value['pack'];?>
</div>
        </div>
    </div>
    <?php }
}
}
