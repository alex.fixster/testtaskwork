<?php

class category {

	

	function getAllCategory($parent_id = "0") {

		global $db, $zend_db;

		$parent_id = (int)$parent_id;

		$fldList[] = "id";
		$fldList[] = "header";
		$fldList[] = "product_qty";
		$fldList[] = "subcategory_qty";
		$fldList[] = "parent_id";
		

		$select = $zend_db->select()
						->from("category", $fldList)
						->where("parent_id = ?", $parent_id)
						->order("id");

		$list = $db->_changeKey($zend_db->query($select)->fetchAll(), "id");

		return $list;

	}



	function getCategoryTree($parent_id = "0", $likeList = false, $productOpt = false) {

		global $db, $zend_db, $categoryTreeList, $product;

		$list = $this->getAllCategory($parent_id);

		if ($list) {

			foreach ($list as $key => $val) {

				if ($productOpt == "Y") {

					$list[$key]['product_list'] = $product->getAllProduct(array("category_id" => array($val['id'])));

				}

				$categoryList = $this->getCategoryTree($val['id'], $likeList, $productOpt);

				if ($categoryList) {

					if ($likeList) $list = $list + $categoryList;
					else $list[$key]['parent_list'] = $categoryList;

				}

			}

		}

		$categoryTreeList = $list;

		return $categoryTreeList;

	}



	function getBreadcrumb($id) {

		global $db, $zend_db;

		$categoryInfo = $this->getCategoryInfo($id);

		if ($categoryInfo['parent_id']) {

			$breadcrumbList = $this->getBreadcrumb($categoryInfo['parent_id']);

		}

		$breadcrumbList[] = $categoryInfo;

		return $breadcrumbList;

	}




	function getCategoryInfo($id) {

		global $db, $zend_db;

		$id = (int)$id;

		$select = $zend_db->select()
						->from("category")
						->where("id = ?", $id);

		$categoryInfo = $db->getOne($select->__toString());

		// $this->_updateSubCategoryQty($id, 0);

		return $categoryInfo;

	}




	function updateCategory($post) {

		global $db, $zend_db;

		if ($this->_checkFormCategory($post)) {

			$post['parent_id'] = (int)$post['parent_id'];
			$post['id'] = (int)$post['id'];

			$info = $this->getCategoryInfo($post['id']);

			$postArr['header'] = trim(strip_tags($post['header']));
			$postArr['parent_id'] = $post['parent_id'];
			
			$idArr['id'] = $post['id'];

			$db->update("category", $postArr, $idArr);

			$this->_updateSubCategoryQty($info['parent_id'], $post['parent_id']);

			return true;

		}

		else {

			$this->_saveFormToSession($post);

			return false;

		}

	}



	function addCategory($post) {

		global $db, $zend_db;

		if ($this->_checkFormCategory($post)) {

			$post['parent_id'] = (int)$post['parent_id'];

			$postArr['header'] = trim(strip_tags($post['header']));
			$postArr['parent_id'] = $post['parent_id'];
			
			$db->add("category", $postArr);

			if ($post['parent_id']) $this->_updateSubCategoryQty(0, $post['parent_id']);

			return true;

		}

		else {

			$this->_saveFormToSession($post);

			return false;

		}

	}



	private function _updateSubCategoryQty($oldParentId, $newParentId) {

		global $db, $zend_db;

		if ($oldParentId) {

			$select = $zend_db->select()
						->from("category", array("category_qty" => "COUNT(*)"))
						->where("parent_id = ?", $oldParentId)
						->order("id");

			$categoryQty = $db->getRow($select->__toString());

			$db->update("category", array("subcategory_qty" => $categoryQty), array("id" => $oldParentId));

		}

		if ($newParentId) {

			$select = $zend_db->select()
						->from("category", array("category_qty" => "COUNT(*)"))
						->where("parent_id = ?", $newParentId)
						->order("id");

			$categoryQty = $db->getRow($select->__toString());

			$db->update("category", array("subcategory_qty" => $categoryQty), array("id" => $newParentId));
			
		}

	}



	private function _checkFormCategory($post) {

		$error = false;

		if ((int)$post['id']) {

			$categoryInfo = $this->getCategoryInfo((int)$post['id']);

			if (!$categoryInfo) $error = save_message(6);

		}

		if (!trim(strip_tags($post['header']))) $error = save_message(1);

		if ((int)$post['parent_id']) {

			$subCategoryInfo = $this->getCategoryInfo((int)$post['parent_id']);

			if (!$subCategoryInfo) $error = save_message(5);

		}
		
		return !$error;
		
	}




	private function _saveFormToSession($post) {

		$_SESSION['form_data']['category'] = $post;

	}




	function deleteCategory($id) {

		global $db, $zend_db;

		$id = (int)$id;

		if ($id) {

			$info = $this->getCategoryInfo($id);
			$parentList = $this->getCategoryTree($id, true);

			$db->delete("category", array("id" => $id));

			if ($parentList) {

				foreach ($parentList as $key => $val) $db->delete("category", array("id" => $val['id']));

			}

			$this->_updateSubCategoryQty($info['parent_id'], 0);

		}

		return true;

	}



	function _updateProductQty($category_id) {

		global $db, $zend_db;

		$category_id = (int)$category_id;

		if ($category_id) {

			$sql = "UPDATE category as c SET c.product_qty = (SELECT COUNT(*) FROM product_category as pc WHERE pc.category_id=c.id) WHERE c.id=".$category_id;
			$db->query($sql);

			return true;

		}

		else return false;

	}


}