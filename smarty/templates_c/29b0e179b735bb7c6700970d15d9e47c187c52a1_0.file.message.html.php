<?php
/* Smarty version 3.1.39, created on 2022-01-13 20:50:07
  from '/Users/alex/www/coffee-store/templates/common/message.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e0745f5e1792_29579530',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '29b0e179b735bb7c6700970d15d9e47c187c52a1' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/common/message.html',
      1 => 1448647816,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61e0745f5e1792_29579530 (Smarty_Internal_Template $_smarty_tpl) {
?><div id="dialog-message" style="display:none;" title="<?php echo $_smarty_tpl->tpl_vars['globalStatText']->value['GLOBAL']['POPUP_MESSAGE_HEADER'];?>
">
	<p id="messageBody">
		<?php if ($_smarty_tpl->tpl_vars['messageArr']->value) {?>
		<ul class="systemMessages">
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['messageArr']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
		<li><?php echo $_smarty_tpl->tpl_vars['local']->value;?>
</li>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		</ul>
		<?php }?>
	</p>
</div>
<?php }
}
