<?php
/* Smarty version 3.1.39, created on 2022-01-17 17:05:22
  from '/Users/alex/www/coffee-store/templates/admin/order/_product_popup.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e585b2738293_84280081',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ae90b9174f31366297ba9ef9e060ee3d75394b41' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/order/_product_popup.html',
      1 => 1511111927,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61e585b2738293_84280081 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="modal fade" id="modalProduct" tabindex="-1" style="z-index:5000;" role="dialog" aria-labelledby="modalProduct" aria-hidden="true">
	<div class="modal-dialog modal-lg" style="width:1000px;">
		<div class="modal-content">
		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">
                	Добавити продукти
                	<div class="hide" style="float:right; padding-right:20px;" id="filterBtnHeader"><a href="javascript: showFilterForm();" class="btn btn-success btn-xs"><i class="fa fa-filter"></i> Фільтр</a></div>
                </h4>
			</div>
			
			<div class="modal-body" style="height:560px;">
				
                <form class="form-horizontal" role="form" id="filterForm">
                    <div class="row">
                        <div class="col-xs-4">
                            
                            <div class="form-group">
                                <label for="inputFilterBrand" class="col-xs-12">Бренди</label>
                                <div class="col-xs-12">
                                    <select class="form-control chosen-select" multiple name="brand_id[]" id="inputFilterBrand" data-placeholder="Введіть бренд">
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['brandList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['searchInfo']->value['brand_id'][$_smarty_tpl->tpl_vars['local']->value['id']]) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
</option>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    </select>
                                </div>
                            </div>
                            
                        </div>
                        
                        <div class="col-xs-4">
                            
                            <div class="form-group">
                                <label for="inputFilterCategory" class="col-xs-12">Категорія</label>
                                <div class="col-xs-12">
									<select class="form-control chosen-select" multiple name="category_id[]" id="inputFilterCategory" data-placeholder="Введіть категорію">
									<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categoryList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
									<optgroup label="<?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
">
										<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['local']->value['items'], 'local_i', false, 'key_i');
$_smarty_tpl->tpl_vars['local_i']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key_i']->value => $_smarty_tpl->tpl_vars['local_i']->value) {
$_smarty_tpl->tpl_vars['local_i']->do_else = false;
?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['local_i']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['searchInfo']->value['category_id'][$_smarty_tpl->tpl_vars['local']->value['id']]) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['local_i']->value['header'];?>
</option>
										<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
									</optgroup>
									<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
									</select>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-xs-4">
                            
                            <div class="form-group">
                                <label for="inputFilterHeader" class="col-xs-12">Назва, артикул</label>
                                <div class="col-xs-12">
                                    <input type="text" class="form-control" name="header" id="inputFilterHeader" value="<?php echo $_smarty_tpl->tpl_vars['searchInfo']->value['header'];?>
" placeholder="Введіть назву товару">
                                </div>
                            </div>
                            
                         </div>
                         
                        
                        <div class="col-xs-12 text-center">
                            <button type="button" name="search" value="Search" class="btn btn-warning btn-sm no-border btn-mobile" style="width:100px;" onClick="searchProduct()">
                                Пошук
                            </button> 
                        </div>
                    </div>
                </form>
                
                <div id="divSearchProduct" style="overflow:auto; height:550px;">
                	
                </div>
                
			</div>
			
			<div class="modal-footer">
            	<button type="button" class="btn btn-success" onClick="addProductProcessAsQty()">Добавити</button>
				<button type="button" class="btn btn-primary" data-dismiss="modal">Закрити</button>
			</div>
			
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php }
}
