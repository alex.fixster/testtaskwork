<?php
/* Smarty version 3.1.39, created on 2022-01-13 20:57:41
  from '/Users/alex/www/coffee-store/templates/admin/products/_edit_image.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e07625750fa0_26103537',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b68fcfbeb2406ababdddc33bb1b7190beb657f94' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/products/_edit_image.html',
      1 => 1488820808,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61e07625750fa0_26103537 (Smarty_Internal_Template $_smarty_tpl) {
?>	<div class="form-group">
    	<input type="hidden" name="image[file][<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['local']->value['image'];?>
">
        <input type="hidden" name="image[id][<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
">
		<label for="inputImage" class="col-xs-2 normal">Картинка #<span><?php echo $_smarty_tpl->tpl_vars['num']->value;?>
</span></label>
		<div class="col-xs-1"><i class="fa fa-trash text-danger bigger-150" style="padding-top:3px; cursor:pointer;"></i></div>
        <div class="col-xs-1" style="margin-top:4px;"><input type="radio" name="image[main]" value="<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['local']->value['is_main'] == 'Y' || $_smarty_tpl->tpl_vars['isMain']->value) {?>checked<?php }?>></div>
        <div class="col-xs-1"><input type="text" name="image[sorter][<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
]" class="form-control text-center" value="<?php echo $_smarty_tpl->tpl_vars['local']->value['sorter'];?>
"></div>
        <div class="col-xs-7" style="margin-top:6px;">
        	<?php if ($_smarty_tpl->tpl_vars['local']->value['image']) {?>
			<a href="javascript: void(0);" class="productImage" data-container="body" data-toggle="popover" data-placement="right" data-content='<img src="/upl_files/product/size.150/<?php echo $_smarty_tpl->tpl_vars['local']->value['image'];?>
">'><?php echo $_smarty_tpl->tpl_vars['local']->value['image'];?>
</a>
            <?php } else { ?>
			<input type="file" name="image_file[<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
]">
			<?php }?>
		</div>
	</div><?php }
}
