<?php
/* Smarty version 3.1.39, created on 2022-01-16 19:38:02
  from '/Users/alex/www/coffee-store/templates/admin/client/list.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e457faa446d9_66215397',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1904383da4ffad93ac4ef7f26d3713f44ebb5244' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/client/list.html',
      1 => 1641227641,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:admin/client/filter.html' => 1,
  ),
),false)) {
function content_61e457faa446d9_66215397 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript" src="/library/bootstrap/3.1.1/js/popover.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" language="javascript" src="/admin/js/users.js"><?php echo '</script'; ?>
>

<div class="page-header">
<h1>Список клієнтів
<span style="float:right;"><a href="/admin/client/add/" class="btn btn-success btn-sm no-border"><span class="fa fa-user"></span>&nbsp;&nbsp;&nbsp;Добавити клієнта</a></span>
</h1>
</div>

<div id="orderFilter">
	<?php $_smarty_tpl->_subTemplateRender("file:admin/client/filter.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
	<div style="height:10px;"></div>
</div>

<div class="col-xs-12">
	
	<div class="table-responsive">
		<table class="table table-striped table-hover dataTable">
			<thead>
				<tr>
					<th width="70" class="<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['pid']['icon'];?>
" onclick="location.href='<?php echo $_smarty_tpl->tpl_vars['linkSort']->value;?>
&sort=<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['pid']['sort'];?>
'">ID</th>
                    <th width="300" class="<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['pname']['icon'];?>
" onclick="location.href='<?php echo $_smarty_tpl->tpl_vars['linkSort']->value;?>
&sort=<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['pname']['sort'];?>
'">Ім'я</th>
                    <th>Нотатки</th>
                    <th width="200">E-mail</th>
                    <th width="115">Телефон</th>
                    <th class="text-center" width="100">Замовлень</th>
					<th width="120" class="text-center">Дії</th>
				</tr>
			</thead>
			<tbody>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['clientList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
				<tr>
					<td class="text-center"><?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['local']->value['name'];?>
</td>
                    <td <?php if ($_smarty_tpl->tpl_vars['local']->value['note']) {?>class="danger red"<?php }?>><?php echo $_smarty_tpl->tpl_vars['local']->value['note'];?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['local']->value['mail'];?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['local']->value['phone_str'];?>
</td>
					<td class="text-center"><?php if ($_smarty_tpl->tpl_vars['local']->value['order_count']) {?><a href="/admin/order/list/?client=<?php echo $_smarty_tpl->tpl_vars['local']->value['phone'];?>
"><span class="label label-warning"><?php echo $_smarty_tpl->tpl_vars['local']->value['order_count'];?>
</span></a><?php }?></td>
					<td class="text-left">
						
						<div class="action-buttons">
							<a href="/admin/client/edit/&id=<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
" class="green"><span class="glyphicon glyphicon-edit"></span> Ред.</a>
                            <?php if (!$_smarty_tpl->tpl_vars['local']->value['order_count']) {?>
                            <a href="javascript: confirmdel('/admin/client/delete/&id=<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
')" class="red"><span class="glyphicon glyphicon-trash"></span> Вид.</a>
                            <?php }?>
						</div>
						
					</td>
				</tr>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</tbody>
		</table>
	</div>
    
    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['pagination_admin'][0], array( array('count'=>$_smarty_tpl->tpl_vars['per_page']->value,'total_count'=>$_smarty_tpl->tpl_vars['total_count']->value,'link'=>$_smarty_tpl->tpl_vars['link']->value,'current_page'=>$_smarty_tpl->tpl_vars['current_page']->value,'max_pages'=>6),$_smarty_tpl ) );?>

	
</div>


<?php }
}
