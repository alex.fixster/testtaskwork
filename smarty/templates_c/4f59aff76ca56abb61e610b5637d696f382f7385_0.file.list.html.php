<?php
/* Smarty version 3.1.39, created on 2022-01-14 14:04:19
  from '/Users/alex/www/coffee-store/templates/admin/products/list.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e166c3e6b564_76175673',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4f59aff76ca56abb61e610b5637d696f382f7385' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/products/list.html',
      1 => 1642161859,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:admin/products/filter.html' => 1,
  ),
),false)) {
function content_61e166c3e6b564_76175673 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript" src="/library/bootstrap/3.1.1/js/popover.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" language="javascript" src="/admin/js/products.js"><?php echo '</script'; ?>
>

<div class="page-header">
<h1>Список продуктів
<span style="float:right;"><a href="/admin/products/add/" class="btn btn-success btn-sm no-border"><span class="fa fa-user"></span>&nbsp;&nbsp;&nbsp;Добавити продукт</a></span>
</h1>
</div>

<div id="orderFilter">
	<?php $_smarty_tpl->_subTemplateRender("file:admin/products/filter.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
	<div style="height:10px;"></div>
</div>

<div class="col-xs-12">
	
	<div class="table-responsive">
		<table class="table table-striped table-hover table-bordered dataTable">
			<thead>
				<tr>
					<th width="50" class="text-center">ID</th>
                    <th width="70" class="<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['partno']['icon'];?>
" onclick="location.href='<?php echo $_smarty_tpl->tpl_vars['linkSort']->value;?>
&sort=<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['partno']['sort'];?>
'">Арт.</th>
                    <th class="<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['pheader']['icon'];?>
" onclick="location.href='<?php echo $_smarty_tpl->tpl_vars['linkSort']->value;?>
&sort=<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['pheader']['sort'];?>
'">Назва</th>
                    <th width="150" class="<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['pstock']['icon'];?>
" onclick="location.href='<?php echo $_smarty_tpl->tpl_vars['linkSort']->value;?>
&sort=<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['pstock']['sort'];?>
'">К-сть</th>
                    <th width="100" class="<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['pprice']['icon'];?>
" onclick="location.href='<?php echo $_smarty_tpl->tpl_vars['linkSort']->value;?>
&sort=<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['pprice']['sort'];?>
'">Ціна</th>
                     <th class="text-center" width="100">Показувати</th>
					<th width="120" class="text-center">Дії</th>
				</tr>
			</thead>
			<tbody>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['productList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
				<tr <?php if ($_smarty_tpl->tpl_vars['local']->value['is_show'] == 'N') {?>class="text-muted"<?php }?>>
					<td class="text-center" style="vertical-align:middle;"><?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
</td>
                    <td class="text-center" style="vertical-align:middle;"><?php echo $_smarty_tpl->tpl_vars['local']->value['art_no'];?>
</td>
                    <td>
                    	<div style="float:left; padding-right:10px;"><img src="/upl_files/product/size.150/<?php echo $_smarty_tpl->tpl_vars['local']->value['image'];?>
" width="50" height="50"></div>
						<div style="font-size:14px; padding-bottom: 5px;"><?php echo $_smarty_tpl->tpl_vars['local']->value['brand_header'];?>
 <?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
, <?php echo $_smarty_tpl->tpl_vars['local']->value['weight'];?>
</div>
						<div class="text-muted"><?php echo $_smarty_tpl->tpl_vars['local']->value['type'];?>
</div>
					</td>
					<td class="text-center" style="vertical-align:middle;">
						<div style="padding-bottom: 5px;"><label class="badge badge-info" style="font-size:16px;"><?php echo $_smarty_tpl->tpl_vars['local']->value['stock_qty'];?>
</label></div>
						<div><label class="label label-light"><img src="/admin/images/PL-flag.jpg" width="21" height="14"> <?php echo $_smarty_tpl->tpl_vars['local']->value['pl_stock_qty'];?>
</label>
							 <label class="label label-light"><img src="/admin/images/UA-flag.jpg" width="21" height="14"> <?php echo $_smarty_tpl->tpl_vars['local']->value['ua_stock_qty'];?>
</label>
						</div>
					</td>
                    <td class="text-center" style="vertical-align:middle;"><?php echo $_smarty_tpl->tpl_vars['local']->value['price'];?>
</td>
                    <td class="text-center" style="vertical-align:middle;"><?php if ($_smarty_tpl->tpl_vars['local']->value['is_show'] == 'Y') {?><i class="fa fa-check"></i><?php } else { ?><i class="fa fa-close"></i><?php }?></td>
					<td class="text-center" style="vertical-align:middle;">
						
						<div class="action-buttons">
							<a href="/admin/products/edit/&id=<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
" class="green"><span class="glyphicon glyphicon-edit"></span> Ред.</a>
                            <a href="javascript: confirmdel('/admin/products/delete/&id=<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
')" class="red"><span class="glyphicon glyphicon-trash"></span> Вид.</a>
						</div>
						
					</td>
				</tr>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</tbody>
		</table>
	</div>
    
    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['pagination_admin'][0], array( array('count'=>$_smarty_tpl->tpl_vars['per_page']->value,'total_count'=>$_smarty_tpl->tpl_vars['total_count']->value,'link'=>$_smarty_tpl->tpl_vars['link']->value,'current_page'=>$_smarty_tpl->tpl_vars['current_page']->value,'max_pages'=>6),$_smarty_tpl ) );?>

	
</div>
<?php }
}
