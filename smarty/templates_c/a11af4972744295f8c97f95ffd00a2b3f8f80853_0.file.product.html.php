<?php
/* Smarty version 3.1.39, created on 2022-01-17 17:05:22
  from '/Users/alex/www/coffee-store/templates/admin/order/product.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e585b2751de2_57471626',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a11af4972744295f8c97f95ffd00a2b3f8f80853' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/order/product.html',
      1 => 1511190336,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61e585b2751de2_57471626 (Smarty_Internal_Template $_smarty_tpl) {
?>									<tr id="product_<?php echo $_smarty_tpl->tpl_vars['local']->value['product_id'];?>
">
										<td>
											<a href="#">
												<img width="100" height="100" src="/upl_files/product/size.150/<?php echo $_smarty_tpl->tpl_vars['local']->value['image'];?>
">
											</a>
										</td>
										<td>
											<?php if ($_smarty_tpl->tpl_vars['local']->value['reduction']) {?>
											<div class="label label-danger"><?php echo $_smarty_tpl->tpl_vars['local']->value['reduction'];?>
</div>
											<?php }?>
                                            <div style="font-size:16px;"><a href="#" style="color:#000000;"><em><?php echo $_smarty_tpl->tpl_vars['local']->value['brand_header'];?>
</em> <?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
</a></div>
                                            <div class="text-muted" style="margin-top:8px;">Вага: <?php echo $_smarty_tpl->tpl_vars['local']->value['weight'];?>
</div>
                                            <div class="text-muted" style="margin-top:2px;">Тип: <?php echo $_smarty_tpl->tpl_vars['local']->value['type'];?>
</div>
										</td>
										<td class="text-center" style="vertical-align:middle;" onDblClick="showPriceFld(<?php echo $_smarty_tpl->tpl_vars['local']->value['product_id'];?>
);" id="productPriceTD<?php echo $_smarty_tpl->tpl_vars['local']->value['product_id'];?>
">
											<input type="hidden" name="cart[<?php echo $_smarty_tpl->tpl_vars['local']->value['product_id'];?>
][price]" value="<?php echo $_smarty_tpl->tpl_vars['local']->value['price_arr'][0];?>
.<?php echo $_smarty_tpl->tpl_vars['local']->value['price_arr'][1];?>
">
											<span style="font-size:26px;"><?php echo $_smarty_tpl->tpl_vars['local']->value['price_arr'][0];?>
</span><span>,<?php echo $_smarty_tpl->tpl_vars['local']->value['price_arr'][1];?>
</span><br>грн
										</td>
										<td style="vertical-align:middle;">
											<input class="form-control text-center" title="Qty" id="productID_<?php echo $_smarty_tpl->tpl_vars['local']->value['product_id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['local']->value['qty'];?>
" name="cart[<?php echo $_smarty_tpl->tpl_vars['local']->value['product_id'];?>
][qty]">
										</td>
										<td class="text-center" style="vertical-align:middle;">
                                            <span style="font-size:26px;"><?php echo $_smarty_tpl->tpl_vars['local']->value['amount_arr'][0];?>
</span><span>,<?php echo $_smarty_tpl->tpl_vars['local']->value['amount_arr'][1];?>
</span><br>грн
										</td>
										<td class="text-center" style="vertical-align:middle;">
											<a href="javascript: deleteProduct(<?php echo $_smarty_tpl->tpl_vars['local']->value['product_id'];?>
);" class="text-danger"><i class="fa fa-trash bigger-200"></i></a>
										</td>
									</tr><?php }
}
