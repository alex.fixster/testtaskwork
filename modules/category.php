<?php


switch ($urlValues[1]) {
	
	default:

		$categoryList = $category->getAllCategory($rqData['parent_id']);

		$breadcrumbList = $category->getBreadcrumb($rqData['parent_id']);

		// dump($breadcrumbList);

		$smarty->assign("breadcrumbList", $breadcrumbList);

		$smarty->assign("categoryList", $categoryList);

		$include_tpl = "category/list.html";

	break;

	case "add":
	case "edit":

		$categoryTreeList = $category->getCategoryTree();

		if ((int)$rqData['id']) $categoryInfo = $category->getCategoryInfo($rqData['id']);

		if ($_SESSION['form_data']['category']) {

			$categoryInfo = $_SESSION['form_data']['category'];

		}

		$smarty->assign("categoryInfo", $categoryInfo);

		$smarty->assign("categoryTreeList", $categoryTreeList);

		$include_tpl = "category/edit.html";

	break;

	case "update":

		// $rqData['id'] = 10000;

		if ((int)$rqData['id']) $res = $category->updateCategory($rqData);
		else $res = $category->addCategory($rqData);

		if ($res) {

			if ($rqData['id']) save_message(2);
			else save_message(3);

			header("location: ".BASE_URL."/category/".((int)$rqData['parent_id'] ? "?parent_id=".(int)$rqData['parent_id'] : ""));

		}

		else header("location: ".$_SERVER['HTTP_REFERER']);

		exit;

	break;

	case "delete":

		$res = $category->deleteCategory($rqData['id']);

		if ($res) save_message(4);

		header("location: ".$_SERVER['HTTP_REFERER']);

		exit;

	break;
	
}


?>