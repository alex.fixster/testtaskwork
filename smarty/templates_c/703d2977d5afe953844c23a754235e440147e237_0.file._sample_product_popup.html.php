<?php
/* Smarty version 3.1.39, created on 2022-01-17 17:05:22
  from '/Users/alex/www/coffee-store/templates/admin/order/_sample_product_popup.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e585b273f030_83611170',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '703d2977d5afe953844c23a754235e440147e237' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/order/_sample_product_popup.html',
      1 => 1511201905,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61e585b273f030_83611170 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="modal fade" id="modalSampleProduct" tabindex="-1" style="z-index:5000;" role="dialog" aria-labelledby="modalSampleProduct" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">
                	Пробніки
                </h4>
			</div>
			
			<div class="modal-body" style="height:600px;">
				
				<div id="divSampleProductList" style="overflow:auto; height:580px;">
					
					<div class="row" style="margin-left:0; margin-right:0;">
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['sampleProductList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
						<div class="col-xs-3 text-center" style="padding-bottom:30px; cursor:pointer;" id="sampleProductID_<?php echo $_smarty_tpl->tpl_vars['local']->value['product_id'];?>
" onClick="changeSelectedSampleProduct(<?php echo $_smarty_tpl->tpl_vars['local']->value['product_id'];?>
)">
							<div class="sampleProductData" data-image="<?php echo $_smarty_tpl->tpl_vars['local']->value['image'];?>
" data-header="<?php echo $_smarty_tpl->tpl_vars['local']->value['brand_header'];?>
 <?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
" data-price="<?php echo $_smarty_tpl->tpl_vars['local']->value['price'];?>
"></div>
							<div><img src="/upl_files/product/size.150/<?php echo $_smarty_tpl->tpl_vars['local']->value['image'];?>
" width="180" height="180" class="<?php if ($_smarty_tpl->tpl_vars['local']->value['used']) {?>bw_image<?php }?>"></div>
							<div style="line-height:16px;"><strong><?php echo $_smarty_tpl->tpl_vars['local']->value['brand_header'];?>
</strong><br><span style="font-size:14px;"><?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
</span></div>
						
						</div>
						<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					</div>
					
				</div>
                
			</div>
			
			<div class="modal-footer">
            	<button type="button" class="btn btn-success" onClick="changeSampleProduct()">Добавити</button>
				<button type="button" class="btn btn-primary" data-dismiss="modal">Закрити</button>
			</div>
			
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php }
}
