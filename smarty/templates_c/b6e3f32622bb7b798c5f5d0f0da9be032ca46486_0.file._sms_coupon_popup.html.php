<?php
/* Smarty version 3.1.39, created on 2022-01-20 22:09:03
  from '/Users/alex/www/coffee-store/templates/admin/coupon/_sms_coupon_popup.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e9c15f2f21c7_21320243',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b6e3f32622bb7b798c5f5d0f0da9be032ca46486' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/coupon/_sms_coupon_popup.html',
      1 => 1468685836,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61e9c15f2f21c7_21320243 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="modal fade" id="modalCouponSMS" tabindex="-1" style="z-index:5000;" role="dialog" aria-labelledby="modalCouponSMS" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">
                	Відправка купона знижки по SMS
                </h4>
			</div>
			
			<div class="modal-body" style="height:260px;">
				
                <form class="form-horizontal" role="form">
                    <div class="row" id="divSendSMSCoupon">
					
						
                         
                    </div>
                </form>
                
			</div>
			
			<div class="modal-footer">
				<div style="float:right;">
					<button type="button" id="btnSendSMSCoupon" class="btn btn-success hide" onClick="sendCouponSMS()">Відправити</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal">Закрити</button>
				</div>
				
				<div style="float:right;" class="hide" id="divSendProcess">
					<div class="progress progress-striped pos-rel active" style="height:42px; width:130px;">
						<div class="progress-bar progress-bar-success" style="width: 100%;"></div>
					</div>
				</div>
				
			</div>
			
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php }
}
