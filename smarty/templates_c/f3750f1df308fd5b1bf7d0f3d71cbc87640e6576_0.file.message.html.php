<?php
/* Smarty version 3.1.39, created on 2022-01-13 10:38:13
  from '/Users/alex/www/coffee-store/templates/admin/common/message.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61dfe4f5229b95_58895089',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f3750f1df308fd5b1bf7d0f3d71cbc87640e6576' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/common/message.html',
      1 => 1450735616,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61dfe4f5229b95_58895089 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="modal fade" id="modalClientMessage" tabindex="-1" style="z-index:10000;" role="dialog" aria-labelledby="modalClientMessage" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">System Message</h4>
			</div>
			
			<div class="modal-body">
				<p id="messageBody">
					<?php if ($_smarty_tpl->tpl_vars['adminMessageArr']->value) {?>
					<ul>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['adminMessageArr']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
					<li><?php echo $_smarty_tpl->tpl_vars['local']->value;?>
</li>
					<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					</ul>
					<?php }?>
				</p>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			</div>
			
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php }
}
