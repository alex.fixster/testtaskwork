<?php

class product {

	

	function getAllProduct($filterData = array()) {

		global $db, $zend_db;

		$fldList[] = "p.id";
		$fldList[] = "p.header";
		$fldList[] = "p.price";
		$fldList['price_1'] = "FLOOR(p.price)";
		$fldList['price_2'] = "SUBSTR(p.price, -2, 2)";


			

		$select = $zend_db->select()
						->from(array("p" => "product"), $fldList)
						->order("p.id");

		if ($filterData['category_id'] and array($filterData['category_id'])) {

			$select->join(array("pc" => "product_category"), "pc.product_id = p.id");
			$select->where("pc.category_id IN (".implode(",", $filterData['category_id']).")");

		}

		$list = $zend_db->query($select)->fetchAll();

		// dump($list);

		return $list;
		

	}


	function getProductInfo($id) {

		global $db, $zend_db;

		$id = (int)$id;

		$select = $zend_db->select()
						->from("product")
						->where("id = ?", $id);

		$productInfo = $db->getOne($select->__toString());

		if ($productInfo) {

			$select = $zend_db->select()
						->from("product_category", array("category_id"))
						->where("product_id = ?", $id);

			$productInfo['category_list'] = $db->_changeKey($zend_db->query($select)->fetchAll(), "category_id");

		}

		return $productInfo;

	}



	
	function updateProduct($post) {

		global $db, $zend_db;

		if ($this->_checkFormProduct($post)) {

			$post['id'] = (int)$post['id'];

			$info = $this->getProductInfo($post['id']);

			$postArr['header'] = trim(strip_tags($post['header']));
			$postArr['price'] = (double)$post['price'];
			
			$idArr['id'] = $post['id'];

			$db->update("product", $postArr, $idArr);

			$this->_updateProductCategory($post['id'], $post['category_id'], $info['category_list']);

			return true;

		}

		else {

			$this->_saveFormToSession($post);

			return false;

		}

	}



	private function _updateProductCategory($product_id, $categoryList, $oldCatgeoryList = array()) {

		global $db, $zend_db, $category;

		$db->delete("product_category", array("product_id" => (int)$product_id));

		if (is_array($categoryList)) {

			foreach ($categoryList as $key => $val) {

				$db->add("product_category", array("product_id" => (int)$product_id, "category_id" => (int)$val));

			}

		}


		if (is_array($categoryList)) {

			foreach ($categoryList as $key => $val) if ((int)$val) $category->_updateProductQty((int)$val);

		}

		if (is_array($oldCatgeoryList)) {

			foreach ($oldCatgeoryList as $key => $val) if ((int)$key) $category->_updateProductQty((int)$key);

		}

	}



	function addProduct($post) {

		global $db, $zend_db;

		if ($this->_checkFormProduct($post)) {

			$postArr['header'] = trim(strip_tags($post['header']));
			$postArr['price'] = (double)$post['price'];

			$id = $db->add("product", $postArr);

			$this->_updateProductCategory($id, $post['category_id']);

			return $id;

		}

		else {

			$this->_saveFormToSession($post);

			return false;

		}

	}



	private function _checkFormProduct($post) {

		global $category;

		$error = false;

		if ((int)$post['id']) {

			$productInfo = $this->getProductInfo((int)$post['id']);

			if (!$productInfo) $error = save_message(10);

		}

		if (!trim(strip_tags($post['header']))) $error = save_message(11);

		if (!(double)$post['price']) $error = save_message(12);

		if (!$post['category_id'] or !is_array($post['category_id']) or !count($post['category_id'])) $error = save_message(13);
		else {

			$category_error = false;

			foreach ($post['category_id'] as $key => $val) {

				$categoryInfo = $category->getCategoryInfo($val);

				if (!$categoryInfo) $category_error = true;

			}

			if ($category_error) $error = save_message(14);

		}
		
		return !$error;
		
	}
	



	private function _saveFormToSession($post) {

		$_SESSION['form_data']['product'] = $post;
		unset($_SESSION['form_data']['product']['category_id']);

		// dump($post['category_id']);exit;


		if ($post['category_id'] and is_array($post['category_id']) and count($post['category_id'])) {

			foreach ($post['category_id'] as $key => $val) {

				$_SESSION['form_data']['product']['category_list'][$val]['category_id'] = $val;

			}

		}

	}



	
	function deleteProduct($id) {

		global $db, $zend_db;

		$id = (int)$id;

		if ($id) {

			$info = $this->getProductInfo($id);
			
			$db->delete("product", array("id" => $id));

			$this->_updateProductCategory($id, array(), $info['category_list']);

		}

		return true;

	}




	// API

	function getApiAllProduct($category_id = array()) {

		global $db, $zend_db, $category;

		// dump($category_id);exit;

		// $subCategoryList = $category->getCategoryTree($category_id, true, true);

		// dump($subCategoryList);exit;

		$list = $this->getAllProduct(array("category_id" => $category_id));

		if ($list) {

			foreach ($list as $key => $val) {

				$returnData[$val['id']]['id'] = $val['id'];
				$returnData[$val['id']]['title'] = $val['header'];
				$returnData[$val['id']]['price'] = $val['price'];

				$select = $zend_db->select()
									->from("product_category", array("category_id"))
									->where("product_id = ?", $val['id']);

				$categoryIDList = $db->_changeKey($zend_db->query($select)->fetchAll(), "category_id");


				if ($categoryIDList) {

					$select = $zend_db->select()
								->from("category")
								->where("id IN (".implode(",", array_keys($categoryIDList)).")");


					$categoryList = $zend_db->query($select)->fetchAll();

					if ($categoryList) {

						foreach ($categoryList as $k => $v) {

							$returnData[$val['id']]['category_list'][$v['id']]['id'] = $v['id'];
							$returnData[$val['id']]['category_list'][$v['id']]['title'] = $v['header'];

						}

					}

				}

			}

		}

		return $returnData;

	}



	function getApiProductInfo($id) {

		global $db, $zend_db;

		$productInfo = $this->getProductInfo($id);

		$returnData['id'] = $productInfo['id'];
		$returnData['title'] = $productInfo['header'];
		$returnData['price'] = $productInfo['price'];

		if ($productInfo['category_list']) {

			$select = $zend_db->select()
								->from("category")
								->where("id IN (".implode(",", array_keys($productInfo['category_list'])).")");


			$categoryList = $zend_db->query($select)->fetchAll();

			foreach ($categoryList as $k => $v) {

				$returnData['category_list'][$v['id']]['id'] = $v['id'];
				$returnData['category_list'][$v['id']]['title'] = $v['header'];

			}

		}

		return $returnData;

	}
	

}