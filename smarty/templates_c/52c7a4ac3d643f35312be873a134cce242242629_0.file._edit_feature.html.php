<?php
/* Smarty version 3.1.39, created on 2022-01-13 20:57:41
  from '/Users/alex/www/coffee-store/templates/admin/products/_edit_feature.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e07625749d17_18039743',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '52c7a4ac3d643f35312be873a134cce242242629' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/products/_edit_feature.html',
      1 => 1494604592,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61e07625749d17_18039743 (Smarty_Internal_Template $_smarty_tpl) {
?>		<tr>
        	<td rowspan="2" class="action-buttons text-center" style="padding-top:35px;"><a href="javascript: void(0);" class="text-danger"><i class="fa fa-trash bigger-200"></i></a></td>
        	<td>
            	<select class="form-control" name="feature[type][]">
                	<option value="">-- Тип --</option>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['paramList']->value['UK'], 'local_p', false, 'key_p');
$_smarty_tpl->tpl_vars['local_p']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key_p']->value => $_smarty_tpl->tpl_vars['local_p']->value) {
$_smarty_tpl->tpl_vars['local_p']->do_else = false;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['local_p']->value['pcode'];?>
" data-value-ua="<?php echo $_smarty_tpl->tpl_vars['paramList']->value['UK'][$_smarty_tpl->tpl_vars['key_p']->value]['pvalue'];?>
" data-value-ru="<?php echo $_smarty_tpl->tpl_vars['paramList']->value['RU'][$_smarty_tpl->tpl_vars['key_p']->value]['pvalue'];?>
" <?php if ($_smarty_tpl->tpl_vars['local_p']->value['pcode'] == $_smarty_tpl->tpl_vars['key']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['local_p']->value['pvalue'];?>
 (<?php echo $_smarty_tpl->tpl_vars['local_p']->value['pcode'];?>
)</option>
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </select>
            </td>
            <td><span class="label arrowed-right" style="float:right; margin-top:7px;"><strong>UK</strong></span></td>
            <td>
				<input type="text" class="form-control" name="feature[pname][UK][]" value="<?php echo $_smarty_tpl->tpl_vars['local']->value['UK']['pname'];?>
" <?php if (!$_smarty_tpl->tpl_vars['local']->value) {?>disabled<?php }?>>
            </td>
            <td>
				<?php if ($_smarty_tpl->tpl_vars['key']->value == 'FEATURES') {?>
				<textarea class="form-control" name="feature[pvalue][UK][]" rows="4" <?php if (!$_smarty_tpl->tpl_vars['local']->value) {?>disabled<?php }?>><?php echo $_smarty_tpl->tpl_vars['local']->value['UK']['pvalue'];?>
</textarea>
				<?php } else { ?>
            	<input type="text" class="form-control" name="feature[pvalue][UK][]" value="<?php echo $_smarty_tpl->tpl_vars['local']->value['UK']['pvalue'];?>
" <?php if (!$_smarty_tpl->tpl_vars['local']->value) {?>disabled<?php }?>>
           		<?php }?>
		    </td>
        </tr>
        <tr>
        	<td class="text-right">Позиція <input type="text" name="feature[sorter][]" class="form-control" style="width:30px; display:inline; margin-left:5px; text-align:center;" value="<?php echo $_smarty_tpl->tpl_vars['local']->value['UK']['sorter'];?>
" <?php if (!$_smarty_tpl->tpl_vars['local']->value) {?>disabled<?php }?>></td>
        	<td><span class="label arrowed-right" style="float:right; margin-top:7px;"><strong>RU</strong></span></td>
            <td>
            	<input type="text" class="form-control" name="feature[pname][RU][]" value="<?php echo $_smarty_tpl->tpl_vars['local']->value['RU']['pname'];?>
" <?php if (!$_smarty_tpl->tpl_vars['local']->value) {?>disabled<?php }?>>
		    </td>
            <td>
				<?php if ($_smarty_tpl->tpl_vars['key']->value == 'FEATURES') {?>
				<textarea class="form-control" name="feature[pvalue][RU][]" rows="4" <?php if (!$_smarty_tpl->tpl_vars['local']->value) {?>disabled<?php }?>><?php echo $_smarty_tpl->tpl_vars['local']->value['RU']['pvalue'];?>
</textarea>
				<?php } else { ?>
            	<input type="text" class="form-control" name="feature[pvalue][RU][]" value="<?php echo $_smarty_tpl->tpl_vars['local']->value['RU']['pvalue'];?>
" <?php if (!$_smarty_tpl->tpl_vars['local']->value) {?>disabled<?php }?>>
           		<?php }?>
		    </td>
        </tr><?php }
}
