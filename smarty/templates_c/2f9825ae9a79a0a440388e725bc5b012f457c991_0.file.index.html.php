<?php
/* Smarty version 3.1.39, created on 2022-04-10 23:27:16
  from '/Users/alex/www/testtask/templates/index.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_62533da46372c2_78767402',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2f9825ae9a79a0a440388e725bc5b012f457c991' => 
    array (
      0 => '/Users/alex/www/testtask/templates/index.html',
      1 => 1649622430,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:common/message.html' => 1,
  ),
),false)) {
function content_62533da46372c2_78767402 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html>
<head>
	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>PHP Technical Task</title>
	<meta name="description" content="It’s needed to create an online store with products and categories. Categories must be organized in a tree structure with including levels up to 4"/>
	<meta name="keywords" content=""/>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-3.6.0.min.js" type="text/javascript" language="javascript"><?php echo '</script'; ?>
>

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<?php echo '<script'; ?>
 src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"><?php echo '</script'; ?>
>


	
	<link rel="stylesheet" type="text/css" href="/css/main.css" media="all"/>
	
	<?php echo '<script'; ?>
 src="/js/main.js" type="text/javascript" language="javascript"><?php echo '</script'; ?>
>
	
	<?php echo '<script'; ?>
 type="text/javascript" language="javascript">
	var BASE_URL = '<?php echo (defined('BASE_URL') ? constant('BASE_URL') : null);?>
';
	<?php echo '</script'; ?>
>
	
	<!--<?php echo '<script'; ?>
 type="text/javascript" language="javascript" src="/js/main.js"><?php echo '</script'; ?>
>-->
</head>

<body>

<header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
	<a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="#">Products Shop Task</a>
</header>

<div class="container-fluid">
	<div class="row">
		<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block ">
			<div class="position-sticky pt-3">
				<ul class="nav flex-column">
					
					<li class="nav-item">
						<a class="nav-link active" aria-current="page" href="/">API Documentations</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="/category/">Categories</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="/product/">Products</a>
					</li>
				</ul>
			</div>
		</nav>

		<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
			<?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['include_tpl']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
		</main>
	</div>
</div>


<?php $_smarty_tpl->_subTemplateRender("file:common/message.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php if ($_smarty_tpl->tpl_vars['errorMessageArr']->value) {
echo '<script'; ?>
 type="text/javascript" language="javascript">

var myModal = new bootstrap.Modal($('#modalClientMessage'));
myModal.show();
/*
$('#modalClientMessage').on('hidden.bs.modal', function (e) {
	
	<?php echo $_smarty_tpl->tpl_vars['messageCloseFunct']->value;?>

	
});
*/

<?php echo '</script'; ?>
>
<?php }?>

</body>
</html><?php }
}
