<?php
/* Smarty version 3.1.39, created on 2022-01-17 17:34:59
  from '/Users/alex/www/coffee-store/templates/admin/essense/sample/edit.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e58ca36d9c42_63262550',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '45cc738c3957424d180d916244c8bb5040a33d95' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/essense/sample/edit.html',
      1 => 1641805450,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61e58ca36d9c42_63262550 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript" src="/library/chosen/1.1.0/chosen.jquery.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" language="javascript" src="/admin/js/essense/sample.js"><?php echo '</script'; ?>
>

<div class="page-header">
	<h1>
		<?php if ($_smarty_tpl->tpl_vars['sampleInfo']->value['id']) {?>Редагування<?php } else { ?>Створення<?php }?> пробніків
        <span style="float:right;"><a href="/admin/essense/sample_add/" class="btn btn-success btn-sm no-border"><span class="fa fa-bookmark"></span>&nbsp;&nbsp;&nbsp;Добавити пробнік</a></span>
    </h1>
</div>

<div style="height:10px;"></div>

<form class="form-horizontal" role="form" action="/admin/essense/sample_update/" method="post">
<input type="hidden" id="inputSampleID" name="id" value="<?php echo $_smarty_tpl->tpl_vars['sampleInfo']->value['id'];?>
">

<div class="row">
<div class="col-xs-5">


	<div class="form-group">
		<label for="inputSampleCategory" class="col-xs-3 normal">Категорія</label>
		<div class="col-xs-8">
			<select id="inputSampleCategory" name="sampleCategory" class="form-control chosen-select" onChange="changeCategory()">
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['sampleCategory']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
			<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['local']->value;?>
</option>
			<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="inputProductID" class="col-xs-3 normal">Продукт*</label>
		<div class="col-xs-8">
			<select id="inputProductID" name="product_id" class="form-control chosen-select" onChange="changeImagePreview()">
			<option value="0" data-image="" data-price="0">--- Виберіть продукт ---</option>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['productList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>           
			<option data-image="<?php echo $_smarty_tpl->tpl_vars['local']->value['image'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['sampleInfo']->value['product_id'] == $_smarty_tpl->tpl_vars['local']->value['id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['local']->value['brand_header'];?>
 <?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
, <?php echo $_smarty_tpl->tpl_vars['local']->value['weight'];?>
</option>
			<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</select>
		</div>
	</div>
	
	<div class="form-group">
		<label for="inputPrice" class="col-xs-3 normal">Ціна*</label>
		<div class="col-xs-3">
			<input type="text" id="inputPrice" name="price" class="form-control input-small" style="display:inline;" placeholder="Введіть ціну" value="<?php echo $_smarty_tpl->tpl_vars['sampleInfo']->value['price'];?>
">
			<span style="padding-left: 10px;"><a href="javascript: updateSamplePrice();"><i class="glyphicon glyphicon-refresh blue" style="font-size: 20px; font-weight:bold; top:5px;"></i></a></span>
		</div>
	</div>
	
	<div class="form-group">
		<label for="inputShow" class="col-xs-3 normal">Не показувати</label>
		<div class="col-xs-8">
			<input type="checkbox" id="inputShow" name="is_show" value="N" <?php if ($_smarty_tpl->tpl_vars['sampleInfo']->value['is_show'] == 'N') {?>checked<?php }?>>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-12 text-center">
			<button type="submit" class="btn btn-warning no-border" style="width:120px;">Зберегти</button>
		</div>
	</div>
	
</div>
<div class=" col-xs-4" id="divImagePreview">
	
</div>
</div>



</form>


<?php echo '<script'; ?>
 type="text/javascript" language="javascript">
$(".chosen-select").chosen({
	width:"100%",
	no_results_text: 'Нічого не знайдено'
}); 
$('.chosen-select').data('chosen').allow_single_deselect = true;

$("#inputProductID_chosen input").css("width", "100%");

changeImagePreview();
<?php echo '</script'; ?>
><?php }
}
