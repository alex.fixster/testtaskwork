<?php
/* Smarty version 3.1.39, created on 2022-01-17 17:05:22
  from '/Users/alex/www/coffee-store/templates/admin/order/_user_comment.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e585b2743233_16320094',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ccdc9f19f6d30e849b939e99f6d1cda40cb65543' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/order/_user_comment.html',
      1 => 1511721974,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61e585b2743233_16320094 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="modal fade" id="modalUserComment" tabindex="-1" style="z-index:5000;" role="dialog" aria-labelledby="modalUserComment" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">
                	Нотатки про клієнта
                </h4>
			</div>
			
			<div class="modal-body" style="height:230px;">
				
				<div class="progress progress-striped pos-rel active" id="userCommentProgress" style="margin-top:85px; height:30px; margin-left:30px; margin-right:30px;">
					<div class="progress-bar progress-bar-success" style="width: 100%;"></div>
				</div>
				
               <textarea class="form-control hide" id="userComment" rows="8" placeholder="Введіть нотатки про клієнта"></textarea>
			   
			   <div id="no-user" style="margin-top:85px; margin-left:200px; font-size:16px;" class="hide">Клієнта не знайдено</div>
               
			</div>
			
			<div class="modal-footer">
				<div style="float:right;">
					<button type="button" id="btnSendUserComment" class="btn btn-success" onClick="saveUserComment()">Зберегти</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal">Закрити</button>
				</div>
				
				<div style="float:right;" class="hide" id="divSendSMSProcess">
					<div class="progress progress-striped pos-rel active" style="height:42px; width:130px;">
						<div class="progress-bar progress-bar-success" style="width: 100%;"></div>
					</div>
				</div>
				
			</div>
			
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php }
}
