<?php
/* Smarty version 3.1.39, created on 2022-01-20 22:09:05
  from '/Users/alex/www/coffee-store/templates/admin/coupon/_edit_group_discount.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e9c161b5c1e2_33928341',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5767b44eb062e7daa99a7a01e19f008766b30cce' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/coupon/_edit_group_discount.html',
      1 => 1544815966,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61e9c161b5c1e2_33928341 (Smarty_Internal_Template $_smarty_tpl) {
?>			<div class="row" style="padding-bottom:5px;">
				<div class="col-xs-8">
					<select name="discount_group[]" class="form-control">
					<option value="">-- Група продуктів --</option>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categoryGroupList']->value['UK'], 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['local_d']->value['category_group'] == $_smarty_tpl->tpl_vars['key']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['local']->value;?>
</option>
					<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					</select>
				</div>
				<div class="col-xs-4">
					<input type="text" name="discount_percent[]" class="form-control" placeholder="Процент" value="<?php echo $_smarty_tpl->tpl_vars['local_d']->value['discount'];?>
">
				</div>
			</div><?php }
}
