<?php
/* Smarty version 3.1.39, created on 2022-01-17 15:29:36
  from '/Users/alex/www/coffee-store/templates/admin/products/edit.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e56f403043d6_00658385',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '06d61323daf18342f9aba0173864ae0635e76aa1' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/products/edit.html',
      1 => 1642426173,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:admin/products/_edit_related_popup.html' => 1,
    'file:admin/products/_edit_feature.html' => 1,
    'file:admin/products/_edit_image.html' => 2,
    'file:admin/products/_edit_related.html' => 1,
  ),
),false)) {
function content_61e56f403043d6_00658385 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/Users/alex/www/coffee-store/smarty/plugins/modifier.spec_char.php','function'=>'smarty_modifier_spec_char',),1=>array('file'=>'/Users/alex/www/coffee-store/smarty/plugins/function.counter.php','function'=>'smarty_function_counter',),));
echo '<script'; ?>
 type="text/javascript" src="/library/chosen/1.1.0/chosen.jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="/library/bootstrap/3.1.1/js/popover.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" language="javascript" src="/admin/js/products.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" src="/library/typeahead/0.10.5/typeahead.bundle.js"><?php echo '</script'; ?>
>
<link href="/library/typeahead/0.10.5/typeahead.css" type="text/css" rel="stylesheet">

<?php $_smarty_tpl->_subTemplateRender("file:admin/products/_edit_related_popup.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<div class="page-header">
	<h1>
		<?php if ($_smarty_tpl->tpl_vars['productInfo']->value['id']) {?>Редагування<?php } else { ?>Створення<?php }?> продукта
        <span style="float:right;"><a href="/admin/products/add/" class="btn btn-success btn-sm no-border"><span class="fa fa-ticket"></span>&nbsp;&nbsp;&nbsp;Добавити продукт</a></span>
    </h1>
</div>

<div style="height:10px;"></div>

<form class="form-horizontal" role="form" action="/admin/products/update/" method="post" enctype="multipart/form-data">
<input type="hidden" name="id" id="inputID" value="<?php echo $_smarty_tpl->tpl_vars['productInfo']->value['id'];?>
">


<div class="row">
<div class="col-xs-6">

	<div class="form-group">
		<label for="inputBrand" class="col-xs-4 normal">Бренд</label>
		<div class="col-xs-8">
			<select id="inputBrand" name="brand_id" class="form-control chosen-select" onChange="changeBrand()">
            <option data-brand="" value="">-- Виберіть бренд --</option>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['brandList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>           
            <option data-brand="<?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['productInfo']->value['brand_id'] == $_smarty_tpl->tpl_vars['local']->value['id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
 (Атикул: <?php echo $_smarty_tpl->tpl_vars['local']->value['art_no'];?>
)</option>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </select>
		</div>
	</div>
    
   	<div class="form-group" id="formMail">
		<label for="inputArtno" class="col-xs-4 normal">Артикул</label>
		<div class="col-xs-8">
			<input type="text" id="inputArtno" name="art_no" class="form-control" placeholder="Введіть артикул" value="<?php echo $_smarty_tpl->tpl_vars['productInfo']->value['art_no'];?>
" onKeyUp="changeArtNo()">
		</div>
	</div>

</div>
<div class="col-xs-6 text-right">
	<?php if ($_smarty_tpl->tpl_vars['productInfo']->value['id']) {?>
	<div><a href="/coffee/<?php echo $_smarty_tpl->tpl_vars['productInfo']->value['alias'];?>
/" target="_blank" class="btn btn-warning btn-sm" style="width:130px;"><i class="fa fa-eye"></i> Переглянути</a></div>
    <?php }?>
</div>
</div>

<div class="row">
<div class="col-xs-12">
   <div class="form-group">
		<label for="inputTitle" class="col-xs-2 normal" style="margin-top:25px;">Назва</label>
		<div class="col-xs-4">
			<span class="label arrowed-in-left arrowed" style="float:right; margin-bottom:4px;"><strong>UK</strong></span>
			<input type="text" id="inputTitleUK" name="header_UK" class="form-control" placeholder="Введіть заголовок" value="<?php echo smarty_modifier_spec_char($_smarty_tpl->tpl_vars['productInfo']->value['header_UK']);?>
" onKeyUp="changeHeader()">
		</div>
		<div class="col-xs-4">
			<span class="label arrowed-right" style="margin-bottom:4px;"><strong>RU</strong></span>
			<input type="text" id="inputTitleRU" name="header_RU" class="form-control" placeholder="Введіть заголовок" value="<?php echo smarty_modifier_spec_char($_smarty_tpl->tpl_vars['productInfo']->value['header_RU']);?>
">
		</div>
	</div>
    
</div>
<div class="col-xs-6">
   
	<div class="form-group">
		<label for="inputDescriptionUK" class="col-xs-12 normal">Короткий опис <span class="label arrowed-in-left arrowed" style="float:right; margin-top:2px;"><strong>UK</strong></span></label>
		<div class="col-xs-12">
			<textarea id="inputDescriptionUK" name="description_UK" class="form-control" rows="4" placeholder="Введіть текст"><?php echo $_smarty_tpl->tpl_vars['productInfo']->value['description_UK'];?>
</textarea>
		</div>
	</div>
	
	<div class="form-group">
		<label for="inputContentShortUK" class="col-xs-12 normal">Опис ч.1 <span class="label arrowed-in-left arrowed" style="float:right; margin-top:2px;"><strong>UK</strong></span></label>
		<div class="col-xs-12">
			<textarea id="inputContentShortUK" name="content_short_UK" class="form-control" rows="4" placeholder="Введіть текст"><?php echo $_smarty_tpl->tpl_vars['productInfo']->value['content_short_UK'];?>
</textarea>
		</div>
	</div>
	
	<div class="form-group">
		<label for="inputContentUK" class="col-xs-12 normal">Опис ч.2 <span class="label arrowed-in-left arrowed" style="float:right; margin-top:2px;"><strong>UK</strong></span></label>
		<div class="col-xs-12">
			<textarea id="inputContentUK" name="content_UK" class="form-control" rows="10" placeholder="Введіть текст"><?php echo $_smarty_tpl->tpl_vars['productInfo']->value['content_UK'];?>
</textarea>
		</div>
	</div>
    
</div>
<div class="col-xs-6">

	<div class="form-group">
		<label for="inputDescriptionRU" class="col-xs-12 normal"><span class="label arrowed-right"><strong>RU</strong></span></label>
		<div class="col-xs-12">
			<textarea id="inputDescriptionRU" name="description_RU" class="form-control" rows="4" placeholder="Введіть текст"><?php echo $_smarty_tpl->tpl_vars['productInfo']->value['description_RU'];?>
</textarea>
		</div>
	</div>
	
	<div class="form-group">
    	<label for="inputContentShortRU" class="col-xs-12 normal"><span class="label arrowed-right"><strong>RU</strong></span></label>
		<div class="col-xs-12">
			<textarea name="content_short_RU" id="inputContentShortRU" class="form-control" rows="4" placeholder="Введіть текст"><?php echo $_smarty_tpl->tpl_vars['productInfo']->value['content_short_RU'];?>
</textarea>
		</div>
	</div>

	<div class="form-group">
    	<label for="inputContentRU" class="col-xs-12 normal"><span class="label arrowed-right"><strong>RU</strong></span></label>
		<div class="col-xs-12">
			<textarea name="content_RU" id="inputContentRU" class="form-control" rows="10" placeholder="Введіть текст"><?php echo $_smarty_tpl->tpl_vars['productInfo']->value['content_RU'];?>
</textarea>
		</div>
	</div>
</div>
</div>

<div class="row">
<div class="col-xs-12">

	<div class="form-group" id="formMail">
		<label for="inputAlias" class="col-xs-2 normal">Аліас</label>
		<div class="col-xs-6">
			<input type="text" id="inputAlias" name="alias" class="form-control" placeholder="Введіть аліас" value="<?php echo $_smarty_tpl->tpl_vars['productInfo']->value['alias'];?>
">
		</div>
	</div>

	<div class="form-group">
		<label for="inputCategory" class="col-xs-2 normal">Категорії</label>
		<div class="col-xs-6">
			<select id="inputCategory" name="category_id[]" multiple class="form-control chosen-select" data-placeholder="Виберіть категорії">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categoryList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
			<optgroup label="<?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['local']->value['items'], 'local_i', false, 'key_i');
$_smarty_tpl->tpl_vars['local_i']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key_i']->value => $_smarty_tpl->tpl_vars['local_i']->value) {
$_smarty_tpl->tpl_vars['local_i']->do_else = false;
?>
				<option value="<?php echo $_smarty_tpl->tpl_vars['local_i']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['productInfo']->value['category_id'][$_smarty_tpl->tpl_vars['local_i']->value['id']]) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['local_i']->value['header'];?>
</option>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</optgroup>
			<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </select>
		</div>
	</div>
    
    <div class="form-group">
		<label for="inputShow" class="col-xs-2 normal">Не показувати</label>
		<div class="col-xs-9" style="padding-top:4px;">
			<input id="inputShow" name="is_show" type="checkbox" value="N" <?php if ($_smarty_tpl->tpl_vars['productInfo']->value['is_show'] == 'N') {?>checked<?php }?>>
        </div>
	</div>
	
	<div class="form-group">
		<label for="inputSale" class="col-xs-2 normal">Розпродаж</label>
		<div class="col-xs-9" style="padding-top:4px;">
			<input id="inputSale" name="is_sale" type="checkbox" value="Y" <?php if ($_smarty_tpl->tpl_vars['productInfo']->value['is_sale'] == 'Y') {?>checked<?php }?>>
        </div>
	</div>
    
    <div class="form-group">
		<label for="inputSorter" class="col-xs-2 normal">Позиція</label>
		<div class="col-xs-2">
			<input type="text" id="inputSorter" name="sorter" class="form-control" placeholder="Введіть позицію" value="<?php echo $_smarty_tpl->tpl_vars['productInfo']->value['sorter'];?>
">
		</div>
	</div>
	
	<div class="form-group">
		<label for="inputPriceOld" class="col-xs-2 normal">Стара ціна</label>
		<div class="col-xs-2">
			<input type="text" id="inputPriceOld" name="price_old" class="form-control" placeholder="Введіть стару ціну" value="<?php echo $_smarty_tpl->tpl_vars['productInfo']->value['price_old'];?>
">
		</div>
	</div>
    
    <div class="form-group">
		<label for="inputPrice" class="col-xs-2 normal">Ціна</label>
		<div class="col-xs-2">
			<input type="text" id="inputPrice" name="price" class="form-control" placeholder="Введіть ціну" value="<?php echo $_smarty_tpl->tpl_vars['productInfo']->value['price'];?>
">
		</div>
	</div>

</div>
</div>
<hr>
<h4>Параметри</h4>

<div class="row">
<div class="col-xs-10">
	<table class="table table-striped" style="margin-bottom:0;" id="tableFeature">
    	<thead>
        	<tr>
            	<th width="50"></th>
            	<th width="210">Тип</th>
                <th width="60"></th>
            	<th width="170">Назва</th>
            	<th>Значення</th>
            </tr>
        </thead>
        <tbody>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['productInfo']->value['features_list'], 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
    	<?php $_smarty_tpl->_subTemplateRender("file:admin/products/_edit_feature.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </tbody>
    </table>
    
    <div style="padding-top:5px;"><a href="javascript: addFeatureFields()" class="btn btn-info btn-xs"><i class="fa fa-plus"></i> Добавити параметр</a></div>
</div>
</div>
<hr>

<!--
-->

<h4>Картинки</h4>

<div class="row">
<div class="col-xs-9">
	<div id="tableImage">
	<?php echo smarty_function_counter(array('assign'=>"num",'start'=>0),$_smarty_tpl);?>

	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['productInfo']->value['image_list'], 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
    <?php echo smarty_function_counter(array('assign'=>"num"),$_smarty_tpl);?>

    <?php $_smarty_tpl->_subTemplateRender("file:admin/products/_edit_image.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
    <?php
}
if ($_smarty_tpl->tpl_vars['local']->do_else) {
?>
    <?php $_smarty_tpl->_assignInScope('num', "1");?>
    <?php $_smarty_tpl->_assignInScope('isMain', "1");?>
    <?php $_smarty_tpl->_subTemplateRender("file:admin/products/_edit_image.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </div>
    <div><a href="javascript: addImageFields()" class="btn btn-info btn-xs"><i class="fa fa-plus"></i> Добавити картинку</a></div>
</div>
</div>
<hr>
<h4>Зв'язані продукти</h4>

<div class="row" id="divRelatedProduct">
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['productInfo']->value['related_list'], 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
    <?php echo smarty_function_counter(array('assign'=>"num",'start'=>1),$_smarty_tpl);?>

    <?php $_smarty_tpl->_subTemplateRender("file:admin/products/_edit_related.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</div>
<div><a href="javascript: relatedPopup()" class="btn btn-info btn-xs"><i class="fa fa-plus"></i> Зв'язати продукт</a></div>
<hr>
<div class="col-xs-12" style="padding-top:20px;">
	
	<div class="form-group">
		<div class="col-sm-12 text-center">
			<button type="submit" class="btn btn-warning no-border" style="width:120px;">Зберегти</button>
		</div>
	</div>
	
</div>

</form>

<?php echo '<script'; ?>
 type="text/javascript" language="javascript">
$(".chosen-select").chosen({
	width:"100%",
	no_results_text: 'Нічого не знайдено'
}); 
$('.chosen-select').data('chosen').allow_single_deselect = true;
$("#inputFilterBrand_chosen input").css("width", "100%");
$("#inputFilterCategory_chosen input").css("width", "100%");

initEditProduct();
initTypeahead();


$( ".product-color-table td" ).click(function() {
	clearColorTable();
	var className = this.className;
	var classNameData = className.split('-');
	
	$("."+className).css("background-position", "-"+(104*classNameData[2])+"px -"+(52*classNameData[3])+"px");
	
	$("#popup-color-selected").val(((104*classNameData[2]+52)/2)+";"+(52*classNameData[3]/2));
	
});
<?php echo '</script'; ?>
><?php }
}
