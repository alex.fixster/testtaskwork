<?php
/* Smarty version 3.1.39, created on 2022-01-13 21:16:50
  from '/Users/alex/www/coffee-store/templates/admin/income/list.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e07aa29b6608_96667009',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '64aa7e6ea43faa825b5b67befc8968145379d951' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/income/list.html',
      1 => 1642099995,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:admin/income/filter.html' => 1,
  ),
),false)) {
function content_61e07aa29b6608_96667009 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript" language="javascript" src="/admin/js/income.js"><?php echo '</script'; ?>
>

<div class="page-header">
<h1>Прихід продуктів
<span style="float:right;"><a href="/admin/income/add/" class="btn btn-success btn-sm no-border"><span class="fa fa-user"></span>&nbsp;&nbsp;&nbsp;Добавити прихід продуктів</a></span>
</h1>
</div>

<div id="orderFilter">
	<?php $_smarty_tpl->_subTemplateRender("file:admin/income/filter.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
	<div style="height:10px;"></div>
</div>

<div class="col-xs-12">
	
	<div class="table-responsive">
		<table class="table table-striped table-hover table-bordered dataTable">
			<thead>
				<tr>
					<th width="140" class="<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['icdate']['icon'];?>
" onclick="location.href='<?php echo $_smarty_tpl->tpl_vars['linkSort']->value;?>
&sort=<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['icdate']['sort'];?>
'">Дата</th>
                    <th class="<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['pheader']['icon'];?>
" onclick="location.href='<?php echo $_smarty_tpl->tpl_vars['linkSort']->value;?>
&sort=<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['pheader']['sort'];?>
'">Продукт</th>
                    <th width="70" class="text-center">Країна</th>
					<th width="75" class="<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['iqty']['icon'];?>
" onclick="location.href='<?php echo $_smarty_tpl->tpl_vars['linkSort']->value;?>
&sort=<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['iqty']['sort'];?>
'">К-сть</th>
                    <th width="75" class="<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['iprice']['icon'];?>
" onclick="location.href='<?php echo $_smarty_tpl->tpl_vars['linkSort']->value;?>
&sort=<?php echo $_smarty_tpl->tpl_vars['sorterList']->value['iprice']['sort'];?>
'">Ціна</th>
                    <th width="120" class="text-center">Дії</th>
				</tr>
			</thead>
			<tbody>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['incomeList']->value, 'local', false, 'key');
$_smarty_tpl->tpl_vars['local']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['local']->value) {
$_smarty_tpl->tpl_vars['local']->do_else = false;
?>
				<tr>
					<td style="vertical-align: middle;"><?php echo $_smarty_tpl->tpl_vars['local']->value['cdate_str'];?>
</td>
                    <td>

                    	<div class="form-group">
							<div style="float:left; margin-right: 10px;"><img src="/upl_files/product/size.150/<?php echo $_smarty_tpl->tpl_vars['local']->value['image'];?>
" width="75" height="75"></div>
							<div>
					        	<div><?php echo $_smarty_tpl->tpl_vars['local']->value['brand_header'];?>
 <?php echo $_smarty_tpl->tpl_vars['local']->value['header'];?>
, <?php echo $_smarty_tpl->tpl_vars['local']->value['weight'];?>
</div>
		                    	<div class="text-muted"><?php echo $_smarty_tpl->tpl_vars['local']->value['pack'];?>
</div>
		                        
		                        <div class="text-muted" style="padding-top: 10px;"><?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['local']->value['category_list'], 'local_c', false, 'key_c');
$_smarty_tpl->tpl_vars['local_c']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['key_c']->value => $_smarty_tpl->tpl_vars['local_c']->value) {
$_smarty_tpl->tpl_vars['local_c']->do_else = false;
if ($_smarty_tpl->tpl_vars['key_c']->value) {?>, <?php }
echo $_smarty_tpl->tpl_vars['local_c']->value['header'];
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?></div>
							</div>
						</div>

                    	
                    </td>
                    <td class="text-center" style="vertical-align: middle;"><img src="/admin/images/<?php echo $_smarty_tpl->tpl_vars['local']->value['income_country'];?>
-flag.jpg" width="40" height="25" style="border: 1px solid #cccccc;"></td>
					<td class="text-center" style="vertical-align: middle;"><?php echo $_smarty_tpl->tpl_vars['local']->value['qty'];?>
</td>
                    <td class="text-center" style="vertical-align: middle;"><?php echo $_smarty_tpl->tpl_vars['local']->value['price'];?>
</td>
                    <td class="text-center" style="vertical-align: middle;">
						
						<div class="action-buttons">
							<a href="/admin/income/edit/&id=<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
" class="green"><span class="glyphicon glyphicon-edit"></span> Ред.</a>
                            <a href="javascript: confirmdel('/admin/income/delete/&id=<?php echo $_smarty_tpl->tpl_vars['local']->value['id'];?>
')" class="red"><span class="glyphicon glyphicon-trash"></span> Вид.</a>
						</div>
						
					</td>
				</tr>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</tbody>
		</table>
	</div>
    
    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['pagination_admin'][0], array( array('count'=>$_smarty_tpl->tpl_vars['per_page']->value,'total_count'=>$_smarty_tpl->tpl_vars['total_count']->value,'link'=>$_smarty_tpl->tpl_vars['link']->value,'current_page'=>$_smarty_tpl->tpl_vars['current_page']->value,'max_pages'=>6),$_smarty_tpl ) );?>

	
</div>
<?php }
}
