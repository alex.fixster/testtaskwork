<?

class Zend_Db_Adapter_db {
	
	
	
	function getAll($sql, $key_fld = false) {

		global $zend_db;
		
		try {

			$result = $zend_db->fetchAll($sql);

			$result = $this->_changeKey($result, $key_fld);


			if (count($result)) return $result;

			else return null;

		}

		catch (Exception $ex) {

			$this->_printError(debug_backtrace());

		}

		return $returnArr;
		
	}



	function _changeKey($list, $key_fld) {

		if ($list and $key_fld) {

			$key_fldInt = (int)$key_fld;
			$key_fldIntStr = (string)$key_fldInt;

			if ($key_fldIntStr == $key_fld) {

				$aKeys = array_keys($list[0]);

				$k = $aKeys[$key_fld - 1];

				foreach ($list as $key => $val) {
					
					$retArr[$val[$k]] = $val;

				}

				return $retArr;

			}

			else {

				foreach ($list as $key => $val) {
					
					$retArr[$val[$key_fld]] = $val;

				}

				return $retArr;

			}

		}

		else return $list;

	}



	function getRow($sql) {

		global $zend_db;

		try {
		
			$result = $zend_db->fetchOne($sql);
			
			return $result;

		}

		catch (Exception $ex) {

			$this->_printError(debug_backtrace());

		}
		
	}




	function getOne($sql) {

		global $zend_db;

		try {
		
			$result = $zend_db->fetchRow($sql);

			return $result;

		}

		catch (Exception $ex) {

			$this->_printError(debug_backtrace());

		}
		
	}




	function add($table, $fieldArr) {

		global $zend_db;
        

		if (!is_array($fieldArr)) return false;

		else {

			try {

				$fieldArr = $this->_checkFieldsInTable($table, $fieldArr);

				$res = $zend_db->insert($table, $fieldArr);

				$lastId = $zend_db->lastInsertId($table);

				return $lastId;

			}

			catch (Exception $ex) {

				$this->_printError(debug_backtrace());

			}

		}
		
	}




	function update($table, $fieldArr, $idArr) {

		global $zend_db;

		try {

			$fieldArr = $this->_checkFieldsInTable($table, $fieldArr);
			$idArr = $this->_checkFieldsInTable($table, $idArr);

			if (!count($fieldArr) or !count($idArr)) $this->_printError(debug_backtrace());

			foreach ($idArr as $key => $val) $where[] = $key." = ".$zend_db->quote($val);

			$result = $zend_db->update($table, $fieldArr, $where);

			if ($result) return $result;
			else return true;

		}

		catch (Exception $ex) {


			$this->_printError(debug_backtrace());

		}

	}




	function delete($table, $idArr) {

		global $zend_db;

		try {

			$idArr = $this->_checkFieldsInTable($table, $idArr);

			if (!is_array($idArr) or !count($idArr)) $this->_printError(debug_backtrace());


			foreach ($idArr as $key => $val) {
				if ($val == null) $where[] = $key." IS NULL";
				else $where[] = $key." = ".$zend_db->quote($val);
			}


			$result = $zend_db->delete($table, $where);

			if ($result) return $result;
			else return true;

		}

		catch (Exception $ex) {

			$this->_printError(debug_backtrace());

		}

	}




	function query($sql) {

		global $zend_db;

		try {
		
			$result = $zend_db->query($sql);

			return true;

		}

		catch (Exception $ex) {

			$this->_printError(debug_backtrace());

		}
		
	}



	function transactShell($key = null) {

		global $zend_db;

		switch ($key) {

			case 'START':
				$zend_db->beginTransaction();
			break;

			case 'COMMIT':
				$zend_db->commit();
			break;

			case 'ROLL':
				$zend_db->rollBack();
			break;			
			
			default:
				return false;
			break;
		}								

		return true;

	}
	
	
	
	function endTransactShell($dbError) {
		
		global $zend_db;

		if (is_array($dbError) and count($dbError)) {
			
			$error = false;
			
			foreach ($dbError as $key => $val) if ($val) $error = true;
				
			if ($error) {
				
				$this->transactShell('ROLL');
				
				return false;
				
			}
			
			else return $this->transactShell('COMMIT');
			
		}
		
		else {
			
			return $this->transactShell('COMMIT');
			
		}
		
	}




	function close() {

		global $zend_db;
		
		$zend_db->closeConnection();
		
	}



	protected function _checkFieldsInTable($table, $fieldArr) {

		global $zend_db;

		$columnArr = $this->getAll("SHOW COLUMNS FROM ".$table, 1);

		foreach ($fieldArr as $key => $val) {

			if (!is_array($columnArr[$key])) unset($fieldArr[$key]);
			elseif (strtolower($val) == "null") $fieldArr[$key] = NULL;

		}

		return $fieldArr;

	} 




	// START ERRORS

	function _printError($errorArr) {

		global $zend_db;

		// $zend_db->getProfiler()->setEnabled(true);
		dump($zend_db->getProfiler()->getLastQueryProfile());

		// dump($zend_db->select()->__toString());


		$sql = str_replace("\r", "", $errorArr[0]['args'][0]);
		$sqlArr = explode("\n", $sql);

		if (is_array($sqlArr)) {

			foreach ($sqlArr as $key => $val) {
				
				$lineArr = explode("\t", $val);
				if (count($lineArr) - 1 > 1) $tabArr[] = count($lineArr) - 1;

			}

			if (is_array($tabArr)) {

				sort($tabArr);
				
				for ($i = 0; $i < $tabArr[0]; $i++) $tabs .= "\t";

				foreach ($sqlArr as $key => $val) {

					$sqlArr[$key] = $this->_str_replace_once($tabs, "", $val);

				}

			}

			$sql = implode("\n", $sqlArr);

		}

		echo "<h2 style='font-family: Arial;'>Mysql Query Error:</h2>";

		echo "<div style='font-family:arial; font-size:13px; border:solid 1px #333333; padding:0px 10px 0px 10px; background-color:#eaeaea;'>";
		echo "<pre>".$sql."</pre>";
		echo "</div>";
		
		dump($errorArr);
		die();

	}



	function _str_replace_once($needle , $replace , $haystack){
		
		$pos = strpos($haystack, $needle);
		
		if ($pos === false) return $haystack;
		
		return substr_replace($haystack, $replace, $pos, strlen($needle));

	}

	// END START ERRORS
	
	
		
}
?>