<?php
/* Smarty version 3.1.39, created on 2022-01-13 10:38:13
  from '/Users/alex/www/coffee-store/templates/admin/login.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61dfe4f521e295_70295198',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '42c4f0aa6d379a6c247f0bd181499e81b4caa687' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/login.html',
      1 => 1488563008,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:admin/common/message.html' => 1,
  ),
),false)) {
function content_61dfe4f521e295_70295198 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>Coffee-Store Адмін</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	
	
	
	<!-- bootstrap & fontawesome -->
	<link rel="stylesheet" href="/library/ace/1.3/css/bootstrap.min.css" />
	<link rel="stylesheet" href="/library/ace/1.3/css/font-awesome.min.css" />


	<!-- page specific plugin styles -->

	<!-- text fonts -->
	<link rel="stylesheet" href="/library/ace/1.3/css/ace-fonts.css" />

	<!-- ace styles -->
	<link rel="stylesheet" href="/library/ace/1.3/css/ace.min.css" />

	
	<link rel="stylesheet" href="/library/ace/1.3/css/ace-skins.min.css" />
	<!--<link rel="stylesheet" href="/library/ace/1.3/ace-rtl.min.css" />-->
	
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic,700italic&subset=latin,cyrillic' rel='stylesheet' type='text/css' />
	
	<!-- ace settings handler -->
	<?php echo '<script'; ?>
 src="/library/ace/1.3/js/ace-extra.min.js"><?php echo '</script'; ?>
>
	
	<link href="/admin/css/login.css" type="text/css" rel="stylesheet">
	<?php echo '<script'; ?>
 type="text/javascript" src="/admin/js/login.js"><?php echo '</script'; ?>
>
	
	
	<?php echo '<script'; ?>
 type="text/javascript" language="javascript">
	var BASE_URL = '<?php echo (defined('BASE_URL') ? constant('BASE_URL') : null);?>
';
	<?php echo '</script'; ?>
>
	
</head>

<body style="padding:0;">

	<div id="navbar" class="navbar navbar-default hide">
		
	</div>

	<!-- /section:basics/navbar.layout -->
	<div class="main-container" id="main-container">
	
	<div class="page-content">
		
		<?php $_smarty_tpl->_subTemplateRender("file:admin/common/message.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		
		
		<div class="text-right hidden-xs"><div style="height:40px;"></div></div>

		<form class="form-signin" action="#" onSubmit="return validLogin()">
			<h2 class="form-signin-heading">Авторизація</h2>
			
			<div style="padding-bottom:5px;"><input type="text" id="mail" class="form-control" placeholder="Email address" autofocus></div>
			<div style="padding-bottom:15px;"><input type="password" id="pass" class="form-control" placeholder="Password"></div>
			
			<div id="login_btn"><button class="btn btn-lg btn-primary btn-block" type="submit">Ввійти</button></div>
			<div class="text-center" id="loading" style="display:none;"><img src="/admin/images/loading.gif" width="40" height="40" alt="Loading..." title="Loading..."></div>
			
			
		</form>

		<div class="hidden-xs"><div style="height:50px;"></div></div>
		<div class="visible-xs"><div style="height:20px;"></div></div>

		
		
	</div>
	</div>
	

		
	<?php echo '<script'; ?>
 src="/library/jquery/jquery-2.1.1.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="/library/ace/1.3/js/bootstrap.min.js"><?php echo '</script'; ?>
>
	
	<?php echo '<script'; ?>
 type="text/javascript" src="/library/bootstrap/3.1.1/js/modal.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 type="text/javascript" src="/library/bootstrap/3.1.1/js/tooltip.js"><?php echo '</script'; ?>
>

	
	<!-- ace scripts -->
	<?php echo '<script'; ?>
 src="/library/ace/1.3/js/ace-elements.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="/library/ace/1.3/js/ace.min.js"><?php echo '</script'; ?>
>


<?php if ($_smarty_tpl->tpl_vars['messageArr']->value) {
echo '<script'; ?>
 type="text/javascript" language="javascript">

$('#modalClientMessage').modal({show:true});
$('#modalClientMessage').on('hidden.bs.modal', function (e) {
	
	<?php echo $_smarty_tpl->tpl_vars['messageCloseFunct']->value;?>

	
});


<?php echo '</script'; ?>
>
<?php }?>

</body>
</html><?php }
}
