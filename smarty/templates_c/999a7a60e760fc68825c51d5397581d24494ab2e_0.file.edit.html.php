<?php
/* Smarty version 3.1.39, created on 2022-01-17 18:22:14
  from '/Users/alex/www/coffee-store/templates/admin/essense/box/edit.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61e597b6251902_32920090',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '999a7a60e760fc68825c51d5397581d24494ab2e' => 
    array (
      0 => '/Users/alex/www/coffee-store/templates/admin/essense/box/edit.html',
      1 => 1641374188,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61e597b6251902_32920090 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/Users/alex/www/coffee-store/smarty/plugins/modifier.spec_char.php','function'=>'smarty_modifier_spec_char',),));
?>
<div class="page-header">
	<h1>
		<?php if ($_smarty_tpl->tpl_vars['boxInfo']->value['id']) {?>Редагування<?php } else { ?>Створення<?php }?> коробки
        <span style="float:right;"><a href="/admin/essense/box_add/" class="btn btn-success btn-sm no-border"><span class="fa fa-dropbox"></span>&nbsp;&nbsp;&nbsp;Добавити коробку</a></span>
    </h1>
</div>

<div style="height:10px;"></div>

<form class="form-horizontal" role="form" action="/admin/essense/box_update/" method="post">
<input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['boxInfo']->value['id'];?>
">

<div class="row">
<div class="col-xs-7">


	<div class="form-group">
		<label for="inputTitle" class="col-xs-3 normal">Заголовок</label>
		<div class="col-xs-5">
			<input type="text" id="inputTitle" name="header" class="form-control" placeholder="Введіть заголовок" value="<?php echo smarty_modifier_spec_char($_smarty_tpl->tpl_vars['boxInfo']->value['header']);?>
">
		</div>
	</div>

	<div class="form-group">
		<label for="inputTitle" class="col-xs-3 normal">Розмір</label>
		<div class="col-xs-9">
			<div class="input-group" style="float:left; width: 160px;">
				<span class="input-group-addon">
					Довжина
				</span>
				<input class="form-control text-center" style="width:60px; padding-right: 10px;" type="text" name="length" value="<?php echo $_smarty_tpl->tpl_vars['boxInfo']->value['length'];?>
">
			</div>
			<div class="input-group" style="float:left; width: 156px;">
				<span class="input-group-addon">
					Ширина
				</span>
				<input class="form-control text-center" style="width:60px; padding-right: 10px;" type="text" name="width" value="<?php echo $_smarty_tpl->tpl_vars['boxInfo']->value['width'];?>
">
			</div>
			<div class="input-group" style="float:left; width: 160px;">
				<span class="input-group-addon">
					Висота
				</span>
				<input class="form-control text-center" style="width:60px;" type="text" name="height" value="<?php echo $_smarty_tpl->tpl_vars['boxInfo']->value['height'];?>
">
			</div>
			
		</div>
	</div>
	
	<div class="form-group">
		<label for="inputPrice" class="col-xs-3 normal">Ціна</label>
		<div class="col-xs-2">
			<input type="text" id="inputPrice" name="price" class="form-control" placeholder="Введіть ціну" value="<?php echo $_smarty_tpl->tpl_vars['boxInfo']->value['price'];?>
">
		</div>
	</div>
	
	<div class="form-group">
		<label for="inputInStock" class="col-xs-3 normal">На складі</label>
		<div class="col-xs-2">
			<input type="text" id="inputInStock" name="in_stock" class="form-control" placeholder="К-сть" value="<?php echo $_smarty_tpl->tpl_vars['boxInfo']->value['in_stock'];?>
">
		</div>
	</div>
	
	<div class="form-group">
		<label for="inputShow" class="col-xs-3 normal">Не показувати</label>
		<div class="col-xs-8">
			<input type="checkbox" id="inputShow" name="is_show" value="N" <?php if ($_smarty_tpl->tpl_vars['boxInfo']->value['is_show'] == 'N') {?>checked<?php }?>>
		</div>
	</div>
	
</div>





<div class="col-xs-7">
	
	<div class="form-group">
		<div class="col-sm-12 text-center">
			<button type="submit" class="btn btn-warning no-border" style="width:120px;">Зберегти</button>
		</div>
	</div>
	

</div>

</div>
</form><?php }
}
